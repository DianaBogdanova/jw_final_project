package by.training.hospitality_network.service;

import by.training.hospitality_network.entity.Image;
import by.training.hospitality_network.service.exception.ServiceException;

import java.io.InputStream;

public interface GalleryService {
    boolean create(InputStream fileContent, int id) throws ServiceException;
    boolean create(Image image) throws ServiceException;
    Image read(int ownerId) throws ServiceException;
}
