package by.training.hospitality_network.service;

import by.training.hospitality_network.dao.util.EntitiesOnPage;
import by.training.hospitality_network.entity.User;
import by.training.hospitality_network.service.exception.ServiceException;

import java.util.List;

public interface UserService {
    User authorisation(String login, String password) throws ServiceException;
    boolean registration(String name, String surname,
                         String login, String password)
            throws ServiceException;
    List<User> readAll() throws ServiceException;
    List<User> filterUsers(String country, boolean isHost) throws ServiceException;

    EntitiesOnPage<User> readFromStartPosition(
            int startPosition, int numberOnPage) throws ServiceException;
    User readById(int id) throws ServiceException;
    int calcRating(int userId) throws ServiceException;
    boolean deleteUser(int userId) throws ServiceException;
    boolean lock(int userId) throws ServiceException;
    boolean unlock(int userId) throws ServiceException;

}
