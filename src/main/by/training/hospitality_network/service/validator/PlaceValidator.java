package by.training.hospitality_network.service.validator;

import by.training.hospitality_network.dao.CatalogDAO;
import by.training.hospitality_network.dao.DAOProvider;
import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.service.exception.ServiceException;

public class PlaceValidator {
    private DAOProvider provider = DAOProvider.getInstance();
    CatalogDAO catalogDAO = provider.getCatalogDAO();
    public boolean cityMatchCountry(final String city, final String country) throws ServiceException {
        try {
            return catalogDAO.cityMatchesCountry(city, country);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
