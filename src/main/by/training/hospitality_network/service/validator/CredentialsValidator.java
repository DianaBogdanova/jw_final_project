package by.training.hospitality_network.service.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CredentialsValidator {
    private static final String EMAIL_PATTERN
            = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[A-Z0-9.-]+\\.[A-Z]{2,}$";
    private static final String PASSWORD_PATTERN
            = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d@$!%*#?&]{8,}$";
    public static boolean isCorrect(final String email,
                                               final String password) {
        return isEmailCorrect(email) && isPasswordCorrect(password);
    }

    private static boolean isEmailCorrect(final String email) {
        Pattern emailPattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = emailPattern.matcher(email);
        return matcher.find();
    }
    private static boolean isPasswordCorrect(final String password) {
        Pattern emailPattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = emailPattern.matcher(password);
        return matcher.find();
    }
}
