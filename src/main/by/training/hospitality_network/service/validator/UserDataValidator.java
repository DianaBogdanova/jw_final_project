package by.training.hospitality_network.service.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserDataValidator {
    private static String PASSWORD_REGEX
            = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d@$!%*#?&]{8,}$";
    private static String EMAIL_REGEX
            = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-z0-9.-]+\\.[a-z]{2,}$";
    private static String NAME_REGEX
            = "^[A-Za-z ,.'-]+$";
    private static String SURNAME_REGEX
            = "^[A-Za-z ,.'-]+$";
    private static String PHONE_REGEX
            = "[0-9]{1,15}";

    public boolean isRegistrationDataCorrect(final String name,
                                             final String surname,
                                             final String email,
                                             final String password) {
        return isNameCorrect(name) && isSurnameCorrect(surname)
                && isEmailCorrect(email) && isPasswordCorrect(password);
    }
    public boolean isPhoneCorrect(final String phone) {
        Pattern emailPattern = Pattern.compile(PHONE_REGEX);
        Matcher matcher = emailPattern.matcher(phone);
        return matcher.find();
    }
    private boolean isEmailCorrect(final String email) {
        Pattern emailPattern = Pattern.compile(EMAIL_REGEX);
        Matcher matcher = emailPattern.matcher(email);
        return matcher.find();
    }
    private boolean isPasswordCorrect(final String password) {
        Pattern emailPattern = Pattern.compile(PASSWORD_REGEX);
        Matcher matcher = emailPattern.matcher(password);
        return matcher.find();
    }
    private boolean isNameCorrect(final String name) {
        Pattern emailPattern = Pattern.compile(NAME_REGEX);
        Matcher matcher = emailPattern.matcher(name);
        return matcher.find();
    }
    private boolean isSurnameCorrect(final String surname) {
        Pattern emailPattern = Pattern.compile(SURNAME_REGEX);
        Matcher matcher = emailPattern.matcher(surname);
        return matcher.find();
    }
}
