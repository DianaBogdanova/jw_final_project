package by.training.hospitality_network.service;

import by.training.hospitality_network.entity.User;
import by.training.hospitality_network.service.exception.ServiceException;

import java.util.List;

public interface UserAccountService {
    String editAccount(String phone, boolean isHost, String profile,
                        final User user) throws ServiceException;
}
