package by.training.hospitality_network.service;

import by.training.hospitality_network.service.exception.ServiceException;

import java.util.List;

public interface CatalogService {
    List<String> readCountries() throws ServiceException;
    List<String> readCities() throws ServiceException;
}
