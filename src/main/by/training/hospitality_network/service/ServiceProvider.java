package by.training.hospitality_network.service;

import by.training.hospitality_network.service.impl.*;

public final class ServiceProvider {
    private static final ServiceProvider INSTANCE
            = new ServiceProvider();

    private UserService userService
            = new UserServiceImpl();
    private QueryService queryService
            = new QueryServiceImpl();
    private UserAccountService profileService
            = new UserAccountServiceImpl();
    private GalleryService galleryService
            = new GalleryServiceImpl();

    private ReviewService reviewService
            = new ReviewServiceImpl();
    private CatalogService catalogService
            = new CatalogServiceIml();


    private ServiceProvider(){}

    public static ServiceProvider getInstance() {
        return INSTANCE;
    }

    public UserService getUserService() {
        return userService;
    }
    public QueryService getQueryService() {
        return queryService;
    }
    public UserAccountService getAccountService() {
        return profileService;
    }
    public GalleryService getGalleryService() {
        return galleryService;
    }

    public ReviewService getReviewService() {
        return reviewService;
    }

    public CatalogService getCatalogService() {
        return catalogService;
    }
}
