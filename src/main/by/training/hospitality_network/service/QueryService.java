package by.training.hospitality_network.service;

import by.training.hospitality_network.entity.Query;
import by.training.hospitality_network.service.exception.ServiceException;

import java.util.List;

public interface QueryService {
    boolean apply(Query query) throws ServiceException;
    List<Query> readUsersInQueries(int userId) throws ServiceException;
    List<Query> readUsersOutQueries(int userId) throws ServiceException;


    boolean reject(int id) throws ServiceException;
    boolean accept(int id) throws ServiceException;
    boolean cancel(int id) throws ServiceException;

}
