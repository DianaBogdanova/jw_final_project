package by.training.hospitality_network.service;

import by.training.hospitality_network.dao.util.EntitiesOnPage;
import by.training.hospitality_network.entity.Review;
import by.training.hospitality_network.entity.User;
import by.training.hospitality_network.service.exception.ServiceException;

import java.util.List;

public interface ReviewService {
    List<Review> readUsersReviews(int userId) throws ServiceException;
    EntitiesOnPage<Review> readFromStartPosition(
            int startPosition, int numberOnPage, int userId) throws ServiceException;
    boolean addReview(int userId, int authorId, String text, int rating) throws ServiceException;
    boolean deleteReview(int id) throws ServiceException;

}
