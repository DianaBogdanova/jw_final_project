package by.training.hospitality_network.service.impl;

import by.training.hospitality_network.dao.DAOProvider;
import by.training.hospitality_network.dao.UserDAO;
import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.dao.util.EntitiesOnPage;
import by.training.hospitality_network.entity.User;
import by.training.hospitality_network.service.UserService;
import by.training.hospitality_network.service.exception.ServiceException;
import by.training.hospitality_network.service.validator.UserDataValidator;

import java.util.List;

public class UserServiceImpl implements UserService {
    private UserDataValidator validator = new UserDataValidator();
    private DAOProvider daoProvider = DAOProvider.getInstance();
    private UserDAO userDAO = daoProvider.getUserDaoImpl();



    @Override
    public User authorisation(final String login, final String password) throws ServiceException {

        User user;
        try {
            user = userDAO.authentication(login, password);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        if(user != null && user.getIsActive()) {
            return user;
        }
        return null;
    }

    @Override
    public boolean registration(final String name,
                                final String surname, final String email,
                                final String password)
            throws ServiceException {
        if (!validator.isRegistrationDataCorrect(name, surname,
                email, password)) {
            return false;
        }
        boolean isPerformed;
        try {
            isPerformed = daoProvider.getUserDaoImpl().registration(
                    name, surname, email, password);
        } catch (DAOException e) {
            throw new ServiceException("Registration is failed", e);
        }
        return isPerformed;
    }
    @Override
    public List<User> readAll() throws ServiceException {
        try {
            return userDAO.readAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<User> filterUsers(String country, boolean isHost) throws ServiceException {
        try {
            return userDAO.filterUsers(country, isHost);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public EntitiesOnPage<User> readFromStartPosition(final int startPosition,
                                                      final int numberOnPage) throws ServiceException {
        try {
            return userDAO.readFromStartPosition(startPosition, numberOnPage);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public User readById(final int id) throws ServiceException {
        User user;
        try {
            user = userDAO.read(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return user;
    }

    @Override
    public int calcRating(final int userId) throws ServiceException {
        int rating;
        try {
            rating = userDAO.calcRating(userId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return  rating;
    }

    @Override
    public boolean deleteUser(int userId) throws ServiceException {
        try {
            return userDAO.delete(userId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean lock(int userId) throws ServiceException {
        try {
            return userDAO.lock(userId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean unlock(int userId) throws ServiceException {
        try {
            return userDAO.unlock(userId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

}
