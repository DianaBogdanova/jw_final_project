package by.training.hospitality_network.service.impl;


import by.training.hospitality_network.dao.DAOProvider;
import by.training.hospitality_network.dao.ReviewDAO;
import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.dao.util.EntitiesOnPage;
import by.training.hospitality_network.entity.Review;
import by.training.hospitality_network.service.ReviewService;
import by.training.hospitality_network.service.exception.ServiceException;

import java.util.List;

public class ReviewServiceImpl implements ReviewService {

    private DAOProvider provider = DAOProvider.getInstance();
    private ReviewDAO reviewDAO = provider.getReviewDAO();

    @Override
    public List<Review> readUsersReviews(final int userId) throws ServiceException {
        List<Review> reviews;
        try {
            reviews = reviewDAO.readUsersReviews(userId);
        } catch (DAOException e) {
            throw new ServiceException("Selecting reviews error", e);
        }
        return reviews;
    }

    @Override
    public EntitiesOnPage<Review> readFromStartPosition(final int startPosition,
                                                      final int numberOnPage, int userId)
            throws ServiceException {
        try {
            return reviewDAO.readFromStartPosition(
                    startPosition, numberOnPage, userId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
    @Override
    public boolean addReview(int userId, int authorId, String text, int rating) throws ServiceException {
        Review review = new Review();
        review.setUserId(userId);
        review.setAuthorId(authorId);
        review.setText(text);
        review.setRating(rating);

        try {
            return reviewDAO.create(review);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean deleteReview(final int id) throws ServiceException {
        try {
            return reviewDAO.delete(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
