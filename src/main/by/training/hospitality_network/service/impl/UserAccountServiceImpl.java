package by.training.hospitality_network.service.impl;

import by.training.hospitality_network.dao.DAOProvider;
import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.entity.User;
import by.training.hospitality_network.service.UserAccountService;
import by.training.hospitality_network.service.exception.ServiceException;
import by.training.hospitality_network.service.validator.UserDataValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class UserAccountServiceImpl implements UserAccountService {
    private static Logger LOGGER
            = LogManager.getLogger(UserAccountServiceImpl.class);
    private DAOProvider daoProvider = DAOProvider.getInstance();
    private UserDataValidator validator = new UserDataValidator();



    @Override
    public String editAccount(final String phone, final boolean isHost,
                               final String profile,
                               final User user) throws ServiceException {
        if(!validator.isPhoneCorrect(phone)) {
            LOGGER.error("Phone is not correct!");
            return "Incorrect phone number";
        }
        boolean isDone;
        try {
            isDone = daoProvider.getUserAccountDAO().editAccount(
                    phone, isHost, profile, user);
        } catch (DAOException e) {
           throw new ServiceException("Account editing is failed");
        }
        if(isDone) {
            return "Changes are saved";
        } else {
            return "Data caving error";
        }
    }
}
