package by.training.hospitality_network.service.impl;

import by.training.hospitality_network.dao.CatalogDAO;
import by.training.hospitality_network.dao.DAOProvider;
import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.service.CatalogService;
import by.training.hospitality_network.service.exception.ServiceException;
import by.training.hospitality_network.service.validator.UserDataValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class CatalogServiceIml implements CatalogService {
    private static final Logger LOGGER
            = LogManager.getLogger(UserAccountServiceImpl.class);
    private DAOProvider daoProvider = DAOProvider.getInstance();
    private CatalogDAO catalogDAO = daoProvider.getCatalogDAO();
    @Override
    public List<String> readCountries() throws ServiceException {
        List<String> countries;
        try {
            countries
                    = catalogDAO.readCountries();
        } catch (DAOException e) {
            LOGGER.error("Country change error");
            throw new ServiceException("Country change is failed", e);
        }
        return countries;
    }

    @Override
    public List<String> readCities() throws ServiceException {
        List<String> cities;
        try {
            cities
                    = catalogDAO.readCities();
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
        return cities;
    }
}
