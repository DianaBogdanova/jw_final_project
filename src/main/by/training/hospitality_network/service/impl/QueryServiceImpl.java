package by.training.hospitality_network.service.impl;

import by.training.hospitality_network.dao.DAOProvider;
import by.training.hospitality_network.dao.QueryDAO;
import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.entity.Query;
import by.training.hospitality_network.service.QueryService;
import by.training.hospitality_network.service.exception.ServiceException;

import java.util.List;

public class QueryServiceImpl implements QueryService {
    private DAOProvider daoProvider = DAOProvider.getInstance();
    private QueryDAO queryDAO = daoProvider.getQueryDAOImpl();


    @Override
    public boolean apply(final Query query) throws ServiceException {
        boolean isPerformed;
        try {
            isPerformed = queryDAO.create(query);
        } catch (DAOException e) {
            throw new ServiceException("Applying is failed", e);
        }
        return isPerformed;
    }

    @Override
    public List<Query> readUsersInQueries(int userId) throws ServiceException {
        List<Query> queries;
        try {
            queries = queryDAO.readUsersInQueries(userId);
        } catch (DAOException e) {
            throw new ServiceException("Selecting queries error", e);
        }
        return queries;
    }
    @Override
    public List<Query> readUsersOutQueries(int userId) throws ServiceException {
        List<Query> queries;
        try {
            queries = queryDAO.readUsersOutQueries(userId);
        } catch (DAOException e) {
            throw new ServiceException("Selecting queries error", e);
        }
        return queries;
    }

    @Override
    public boolean reject(final int id) throws ServiceException {
        boolean isPerformed;
        try {
            isPerformed = queryDAO.reject(id);
        } catch (DAOException e) {
            throw new ServiceException("Can't reject the query", e);
        }
        return isPerformed;
    }

    @Override
    public boolean accept(final int id) throws ServiceException {
        boolean isPerformed;
        try {
            isPerformed = queryDAO.accept(id);
        } catch (DAOException e) {
            throw new ServiceException("Can't accept the query", e);
        }
        return isPerformed;
    }

    @Override
    public boolean cancel(final int id) throws ServiceException {
        boolean isPerformed;
        try {
            isPerformed = queryDAO.cancel(id);
        } catch (DAOException e) {
            throw new ServiceException("Can't cancel the query", e);
        }
        return isPerformed;
    }
}
