package by.training.hospitality_network.service.impl;

import by.training.hospitality_network.dao.DAOProvider;
import by.training.hospitality_network.dao.GalleryDAO;
import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.entity.Image;
import by.training.hospitality_network.service.GalleryService;
import by.training.hospitality_network.service.exception.ServiceException;

import java.io.InputStream;

public class GalleryServiceImpl implements GalleryService {
    private DAOProvider provider = DAOProvider.getInstance();
    private GalleryDAO galleryDAO = provider.getGalleryDAO();
    @Override
    public boolean create(final InputStream fileContent, final int id) throws ServiceException {
        boolean isDone;
        try {
            isDone = galleryDAO.create(fileContent, id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return isDone;
    }

    @Override
    public boolean create(final Image image) throws ServiceException {
        boolean isDone;
        try {
            isDone = galleryDAO.create(image);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return isDone;
    }

    @Override
    public Image read(final int ownerId) throws ServiceException {
        Image image;
        try {
            image = galleryDAO.read(ownerId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return image;
    }
}
