package by.training.hospitality_network.controller.custom;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

public class GreetingTag extends TagSupport {
    private String userName;
    public void setUserName(final String name) {
        userName = name;
    }
    @Override
    public int doStartTag() throws JspException {
        try {
            ResourceBundle bundle
                    = ResourceBundle.getBundle(
                            "resources/locale", new Locale((String) pageContext.
                            getSession().getAttribute("language")));
            String greeting = bundle.getString("greeting") + userName;
            pageContext.getOut().write("<h4><strong>" + greeting + "</strong><h4>");
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }
}
