package by.training.hospitality_network.controller.util;

import by.training.hospitality_network.entity.Image;
import by.training.hospitality_network.service.GalleryService;
import by.training.hospitality_network.service.ServiceProvider;
import by.training.hospitality_network.service.exception.ServiceException;


public class ImageReader {
    private ServiceProvider provider = ServiceProvider.getInstance();
    private GalleryService service = provider.getGalleryService();


    public Image prepareImage(
                              final int ownerId) throws ServiceException {
        return service.read(ownerId);
    }
}
