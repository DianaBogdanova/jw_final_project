package by.training.hospitality_network.controller.util;

import by.training.hospitality_network.controller.action.Action;
import by.training.hospitality_network.dao.util.EntitiesOnPage;
import by.training.hospitality_network.entity.Entity;

import javax.servlet.http.HttpServletRequest;
import java.net.http.HttpRequest;

public class Pagination {
    private static final String CURRENT_PAGE_PARAM = "currentPage";
    private static final String PAGES_NUMBER_PARAM = "pagesNumber";

    public static int calculatePagesNumber(final EntitiesOnPage entities,
                                           final int entitiesOnPageNumber) {
        int entitiesNumber = entities.getEntitiesNumber();
        int fullPages = entitiesNumber / entitiesOnPageNumber;
        int notFullPages = entitiesNumber % entitiesOnPageNumber;
        int result = 1;
        if (fullPages >= 1) {
            result = fullPages;
            if (notFullPages != 0) {
                result += 1;
            }
        }
        return result;
    }

    public static int calculatePageBegin(final int currentPage,
                                         final int entitiesOnPageNumber) {
        return (currentPage - 1) * entitiesOnPageNumber;
    }

    public static void setPaginationParams(Action.Forward forward, int currentPage,
                                           EntitiesOnPage entities, String listName,
                                           final int entitiesOnPageNumber) {
        forward.getAttributes().put(CURRENT_PAGE_PARAM, currentPage);
        forward.getAttributes().put(PAGES_NUMBER_PARAM,
                calculatePagesNumber(entities, entitiesOnPageNumber));
        forward.getAttributes().put(listName, entities);
    }
    public static void setPaginationParams(HttpServletRequest request, int currentPage,
                                           EntitiesOnPage entities, String listName,
                                           final int entitiesOnPageNumber) {
        request.setAttribute(CURRENT_PAGE_PARAM, currentPage);
        request.setAttribute(PAGES_NUMBER_PARAM,
                calculatePagesNumber(entities, entitiesOnPageNumber));
        request.setAttribute(listName, entities);
    }

}
