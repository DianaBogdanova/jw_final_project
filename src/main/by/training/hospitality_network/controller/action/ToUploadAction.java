package by.training.hospitality_network.controller.action;

import by.training.hospitality_network.dao.exception.DAOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ToUploadAction extends AuthorizedUserAction {
    @Override
    public Forward execute(HttpServletRequest request, HttpServletResponse response) throws DAOException, IOException, ServletException {
        return new Forward("/upload.jsp", false);
    }
}
