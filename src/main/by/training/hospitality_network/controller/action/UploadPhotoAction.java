package by.training.hospitality_network.controller.action;

import by.training.hospitality_network.dao.exception.ConnectionPoolException;
import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.dao.impl.DAOImpl;
import by.training.hospitality_network.entity.Image;
import by.training.hospitality_network.entity.User;
import by.training.hospitality_network.service.GalleryService;
import by.training.hospitality_network.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

public class UploadPhotoAction extends AuthorizedUserAction {
/**
 * Logger for creation notes to some appender.
 */
private static final Logger LOGGER
        = LogManager.getLogger(UploadPhotoAction.class);

    private static final String NAME_OF_HIDDEN_FOLDER
            = "\\out\\artifact";

    private static final String PATH_TO_IMAGE_FOLDER
            =  "\\web\\img\\avatar\\";

    private static final String PATH_TO_IMAGE_FOLDER_FROM_TABLE
            = "../../img/avatar/";
    @Override
    public Forward execute(HttpServletRequest req, HttpServletResponse response)
            throws DAOException, IOException, ServletException {
        Part filePart = req.getPart("photo");

        User user = (User) req.getSession().getAttribute("currentUser");
        int userId = user.getId();

        Part part = req.getPart("photo");
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();

        String uploadPath = req.getServletContext().getRealPath("");
        Integer uploadPathIndex = uploadPath.indexOf(NAME_OF_HIDDEN_FOLDER);
        String pathToProject = uploadPath.substring(0, uploadPathIndex);
        String newPhotoPath
                = pathToProject + PATH_TO_IMAGE_FOLDER + fileName;

        File fileSaveDir = new File(newPhotoPath);
        part.write(fileSaveDir + File.separator);

        Image image = new Image();
        image.setUserId(userId);
        image.setPath(PATH_TO_IMAGE_FOLDER_FROM_TABLE + fileName);
        Forward forward = new Forward("/upload.jsp", false);
        boolean isDone = false;
        try {
            GalleryService service = provider.getGalleryService();
            isDone = service.create(image);
            user.setAvatarPath(PATH_TO_IMAGE_FOLDER_FROM_TABLE + fileName);
        } catch (ServiceException e) {
            req.setAttribute("error", "Photo upload error");
            LOGGER.error("Photo upload is failed", e);
        }
        if(isDone) {
            req.setAttribute(
                    "message", "The photo is uploaded successfully");
        } else {
            req.setAttribute("error", "The photo isn't uploaded");
        }

        return forward;
    }
}
