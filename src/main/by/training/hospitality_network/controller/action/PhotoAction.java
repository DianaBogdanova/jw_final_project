package by.training.hospitality_network.controller.action;

import by.training.hospitality_network.dao.exception.ConnectionPoolException;
import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.dao.impl.DAOImpl;
import by.training.hospitality_network.entity.User;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PhotoAction extends Action {
    private static final String NAME_OF_HIDDEN_FOLDER
            = "\\out\\artifact";

    private static final String PATH_TO_IMAGE_FOLDER
            =  "\\web\\img\\avatar\\";

    private static final String PATH_TO_IMAGE_FOLDER_FROM_TABLE
            = "../../img/avatar/";
    @Override
    public Forward execute(HttpServletRequest req, HttpServletResponse response) throws DAOException, IOException, ServletException {
        Part filePart = req.getPart("photo");

        User user = (User) req.getSession().getAttribute("currentUser");
        int userId = user.getId();

        Part part = req.getPart("photo");
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();

        //String savePath = req.getServletContext().getRealPath("") + File.separator + fileName;
        String uploadPath = req.getServletContext().getRealPath("");
        Integer uploadPathIndex = uploadPath.indexOf(NAME_OF_HIDDEN_FOLDER);
        String pathToProject = uploadPath.substring(0, uploadPathIndex);
        String newPhotoPath
                = pathToProject + PATH_TO_IMAGE_FOLDER + fileName;

        File fileSaveDir = new File(newPhotoPath);

        part.write(fileSaveDir + File.separator);

        try {
            Connection con = DAOImpl.getPool().takeConnection();
            PreparedStatement ps = con.prepareStatement("Insert INTO avatars(`user_id`, `path`) VALUES (?, ?)");
            ps.setInt(1, userId);
            ps.setString(2, PATH_TO_IMAGE_FOLDER_FROM_TABLE + fileName);
            ps.executeUpdate();


        } catch (SQLException | ConnectionPoolException e) {
            e.printStackTrace();
        }
        return null;
    }


}
