package by.training.hospitality_network.controller.action.administrator;

import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.entity.User;
import by.training.hospitality_network.service.ReviewService;
import by.training.hospitality_network.service.UserService;
import by.training.hospitality_network.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteReviewAction extends AdminAction {
    private static final Logger LOGGER
            = LogManager.getLogger(DeleteReviewAction.class);
    private static final String REVIEW_ID_PARAM = "rId";
    private static final String ACCOUNT_OWNER = "uId";
    private static final String ERROR_PAGE = "/error.jsp";



    @Override
    public Forward execute(HttpServletRequest req, HttpServletResponse resp) throws DAOException, IOException, ServletException {
        int id = Integer.parseInt(req.getParameter(REVIEW_ID_PARAM));
        int userId = Integer.parseInt(req.getParameter(ACCOUNT_OWNER));
        ReviewService reviewService = provider.getReviewService();
        Forward forward = new Forward("/user/account.html");

        boolean isDone;
        try {
            isDone = reviewService.deleteReview(id);
        } catch (ServiceException e) {
            req.setAttribute("error", "Can not delete the review");
            LOGGER.error(e);
            return new Forward(ERROR_PAGE, false);
        }
        if (isDone) {
            forward.getAttributes().put("message", "Review is successfully deleted");
        } else {
            forward.getAttributes().put("error", "The review can't be deleted");
        }
        forward.getAttributes().put("otherUser", userId);
        return forward;
    }
}
