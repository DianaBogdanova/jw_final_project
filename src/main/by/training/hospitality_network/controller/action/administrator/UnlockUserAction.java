package by.training.hospitality_network.controller.action.administrator;

import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.entity.User;
import by.training.hospitality_network.service.UserService;
import by.training.hospitality_network.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UnlockUserAction extends AdminAction {
    private static final Logger LOGGER
            = LogManager.getLogger(UnlockUserAction.class);
    private static final String USER_ID_PARAM = "userId";
    private static final String CURRENT_PAGE_PARAM = "currentPage";
    private static final String ACCOUNT_OWNER = "otherUser";
    private static final String ERROR_PAGE = "/error.jsp";



    @Override
    public Forward execute(HttpServletRequest req, HttpServletResponse resp) throws DAOException, IOException, ServletException {
        int id = Integer.parseInt(req.getParameter(USER_ID_PARAM));
        String currentPage = req.getParameter(CURRENT_PAGE_PARAM);

        UserService userService = provider.getUserService();
        Forward forward = null;
        if("list".equals(currentPage)) {
            forward = new Forward("/search.html");
        } else if("account".equals(currentPage)){
            int otherUserId = Integer.parseInt(req.getParameter(USER_ID_PARAM));
            User user;
            try {
                user = userService.readById(otherUserId);
            } catch (ServiceException e) {
                LOGGER.error(e);
                return new Forward(ERROR_PAGE, false);
            }
            forward = new Forward("/user/account.html");
            forward.getAttributes().put(ACCOUNT_OWNER, user.getId());
        }
        boolean isDone;
        try {
            isDone = userService.unlock(id);
        } catch (ServiceException e) {
            req.setAttribute("error", "Can not lock the user");
            LOGGER.error(e);
            return new Forward("/error.html");
        }
        if (isDone) {
            forward.getAttributes().put("message", "User is successfully unlocked");
        } else {
            forward.getAttributes().put("error", "You can't unlock this user");
        }
        return forward;
    }
}
