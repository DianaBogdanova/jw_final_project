package by.training.hospitality_network.controller.action.administrator;

import by.training.hospitality_network.controller.action.Action;
import by.training.hospitality_network.entity.UserRole;

public abstract class AdminAction extends Action {
    public AdminAction() {
        getAllowRoles().add(UserRole.ADMINISTRATOR);
    }

}
