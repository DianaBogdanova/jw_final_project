package by.training.hospitality_network.controller.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Locale;

public class MainAction extends Action {
    private static final String MAIN_PAGE = "/main.jsp";
    private static final String LANGUAGE_PARAM = "language";

    @Override
    public Forward execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        session.setAttribute(LANGUAGE_PARAM, "en");
        return new Forward(MAIN_PAGE, false);
    }
}
