package by.training.hospitality_network.controller.action;

import by.training.hospitality_network.controller.action.Action;
import by.training.hospitality_network.controller.action.AuthorizedUserAction;
import by.training.hospitality_network.dao.exception.DAOException;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AddReviewAction extends AuthorizedUserAction {

    @Override
    public Forward execute(HttpServletRequest req, HttpServletResponse resp) throws DAOException, IOException, ServletException {
      return new Forward("/review.jsp", false);
    }
}
