package by.training.hospitality_network.controller.action;

import by.training.hospitality_network.entity.User;
import by.training.hospitality_network.entity.UserRole;
import by.training.hospitality_network.service.ServiceProvider;
import by.training.hospitality_network.service.UserService;
import by.training.hospitality_network.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SaveUserAction extends Action {
    private static final Logger LOGGER
            = LogManager.getLogger(SaveUserAction.class);
    private static final String PASSWORD_PARAM = "password";
    private static final String EMAIL_PARAM = "email";
    private static final String NAME_PARAM = "name";
    private static final String SURNAME_PARAM = "surname";


    @Override
    public Forward execute(HttpServletRequest req, HttpServletResponse resp) {
        String password = req.getParameter(PASSWORD_PARAM);
        String email = req.getParameter(EMAIL_PARAM);
        String name = req.getParameter(NAME_PARAM);
        String surname = req.getParameter(SURNAME_PARAM);

        ServiceProvider provider = ServiceProvider.getInstance();
        UserService userService = provider.getUserService();
        boolean isRegistrationSuccessful = false;
        try {
            isRegistrationSuccessful = userService.registration(
                    name, surname, email, password);
        } catch (ServiceException e) {
            LOGGER.error(e);
        }
        HttpSession session = req.getSession();
        if (isRegistrationSuccessful) {
            User user = new User();
            user.setName(name);
            user.setSurname(surname);
            user.setEmail(email);
            user.setRole(UserRole.REGISTERED_USER);
            session.setAttribute("currentUser", user);
            req.setAttribute(
                    "message", "Registration is successfully completed");
        } else {
            req.setAttribute("error", "Email address already exists");
        }
        return new Forward("/registration.jsp", false);
    }
}
