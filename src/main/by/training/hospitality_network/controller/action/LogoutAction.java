package by.training.hospitality_network.controller.action;

import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LogoutAction extends AuthorizedUserAction {
    private static final Logger LOGGER
            = LogManager.getLogger(LogoutAction.class);
    @Override
    public Forward execute(HttpServletRequest request, HttpServletResponse response) throws DAOException {
        HttpSession session = request.getSession(false);
        User user = (User)session.getAttribute("currentUser");
        LOGGER.info(String.format("user \"%s\" is logged out", user.getEmail()));
        request.getSession(false).invalidate();
        return new Forward("/main.html");
    }
}
