package by.training.hospitality_network.controller.action;

import by.training.hospitality_network.controller.util.Pagination;
import by.training.hospitality_network.dao.util.EntitiesOnPage;
import by.training.hospitality_network.entity.User;
import by.training.hospitality_network.entity.UserRole;
import by.training.hospitality_network.service.CatalogService;
import by.training.hospitality_network.service.UserService;
import by.training.hospitality_network.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class SearchUsersAction extends AuthorizedUserAction {

    private static final Logger LOGGER
            = LogManager.getLogger(SearchUsersAction.class);
    private CatalogService catalogService = provider.getCatalogService();

    private static final String ERROR_PAGE = "/error.jsp";
    private static final String LIST_USERS_PAGE = "/list_users.jsp";
    private static final String LIST_USERS_PAGE_ADMIN = "/admin/list_users.jsp";
    private static final String ROLE_ATTRIBUTE = "role";

    private static final String COUNTRIES_ATTRIBUTE = "countries";
    private static final String CITIES_ATTRIBUTE = "cities";

    private static final int USERS_ON_PAGE_NUMBER = 10;



    @Override
    public Forward execute(HttpServletRequest req, HttpServletResponse resp) {
        UserService service = provider.getUserService();

        EntitiesOnPage<User> users = new EntitiesOnPage<>();
        EntitiesOnPage<User> usersOnPage;
        try {
            if(req.getParameter("pagesNumber") == null) {
                List<User> allUsers = service.readAll();
                users.setSearchResult(allUsers);
                users.setEntitiesNumber(allUsers.size());
                req.setAttribute("pagesNumber",
                        Pagination.calculatePagesNumber(users, USERS_ON_PAGE_NUMBER));
            } else {
                req.setAttribute("pagesNumber", req.getParameter("pagesNumber"));
            }

            int currentPage;
            if (req.getParameter("currentPage") != null) {
                currentPage = Integer.parseInt(req.getParameter("currentPage"));
            } else {
                currentPage = 1;
            }

            int startPosition = Pagination.calculatePageBegin(currentPage, USERS_ON_PAGE_NUMBER);
            usersOnPage
                    = service.readFromStartPosition(startPosition, USERS_ON_PAGE_NUMBER);
            req.setAttribute("currentPage", currentPage);
            req.setAttribute("users", usersOnPage.getSearchResult());

            List<String> countries;
            List<String> cities;
            countries = catalogService.readCountries();
            cities = catalogService.readCities();

            req.setAttribute(COUNTRIES_ATTRIBUTE, countries);
            req.setAttribute(CITIES_ATTRIBUTE, cities);

            HttpSession session = req.getSession();
            UserRole role = (UserRole) session.getAttribute(ROLE_ATTRIBUTE);
            if(role == UserRole.ADMINISTRATOR) {
                return new Forward(LIST_USERS_PAGE_ADMIN, false);
            } else {
                return new Forward(LIST_USERS_PAGE, false);
            }
        } catch (ServiceException e) {
            LOGGER.error(e);
            return new Forward(ERROR_PAGE, false);
        }
    }
}
