package by.training.hospitality_network.controller.action.user;

import by.training.hospitality_network.controller.action.Action;
import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.entity.Review;
import by.training.hospitality_network.entity.User;
import by.training.hospitality_network.service.ReviewService;
import by.training.hospitality_network.service.ServiceProvider;
import by.training.hospitality_network.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class MyReviewsAction extends RegisteredUserAction {
    private static Logger LOGGER
            = LogManager.getLogger(MyReviewsAction.class);
    private static final String ERROR_PAGE = "/error.jsp";
    private static final String MY_REVIEWS_PAGE = "/my_reviews.jsp";

    private static final String USER_ATTRIBUTE = "user";
    private ServiceProvider provider = ServiceProvider.getInstance();
    @Override
    public Forward execute(HttpServletRequest req, HttpServletResponse resp) throws DAOException, IOException, ServletException {
        HttpSession session = req.getSession();
        User currentUser = (User) session.getAttribute(USER_ATTRIBUTE);
        int userId = currentUser.getId();
        ReviewService service = provider.getReviewService();
        try {
            List<Review> reviews = service.readUsersReviews(userId);
            req.setAttribute("reviews", reviews);
        } catch (ServiceException e) {
            LOGGER.error(e);
            return new Forward(ERROR_PAGE);
        }


        return new Forward(MY_REVIEWS_PAGE, false);
    }
}
