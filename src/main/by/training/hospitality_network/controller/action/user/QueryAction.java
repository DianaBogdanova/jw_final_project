package by.training.hospitality_network.controller.action.user;

import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.entity.User;
import by.training.hospitality_network.service.UserService;
import by.training.hospitality_network.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class QueryAction extends RegisteredUserAction {
    private static final Logger LOGGER
            = LogManager.getLogger(QueryAction.class);
    private static final String ADDRESSEE_PARAM = "addressee";
    private static final String ERROR_PAGE = "/error.jsp";
    @Override
    public Forward execute(HttpServletRequest req, HttpServletResponse resp) throws DAOException {
        int addresseeId = Integer.parseInt(req.getParameter(ADDRESSEE_PARAM));
        UserService userService = provider.getUserService();
        User addressee;
        try {
           addressee = userService.readById(addresseeId);
        } catch (ServiceException e) {
            LOGGER.error(e);
            return new Forward(ERROR_PAGE);
        }
        String addresseeName
                = addressee.getName() + " " + addressee.getSurname();
        Forward forward = new Forward("/query.jsp", false);
        req.setAttribute("addresseeName", addresseeName);
        return forward;
    }
}
