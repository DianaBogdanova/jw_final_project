package by.training.hospitality_network.controller.action.user;

import by.training.hospitality_network.service.QueryService;
import by.training.hospitality_network.service.ServiceProvider;
import by.training.hospitality_network.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CancelAction extends RegisteredUserAction {
    private static final Logger LOGGER
            = LogManager.getLogger(CancelAction.class);
    private static final String ERROR_PAGE = "/error.jsp";
    private static final String QUERY_ID_PARAM = "outQuery";
    @Override
    public Forward execute(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        int id = Integer.parseInt(req.getParameter(QUERY_ID_PARAM));
        ServiceProvider provider = ServiceProvider.getInstance();
        QueryService service = provider.getQueryService();
        boolean isDone;
        try {
            isDone = service.cancel(id);
        } catch (ServiceException e) {
            LOGGER.error(e);
            return new Forward(ERROR_PAGE);
        }
        if(!isDone) {
            req.setAttribute("error", "Can't cancel the query");
        }
        return new Forward("/queries.html");
    }
}
