package by.training.hospitality_network.controller.action.user;

import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.entity.Query;
import by.training.hospitality_network.entity.User;
import by.training.hospitality_network.service.QueryService;
import by.training.hospitality_network.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class MyQueriesAction extends RegisteredUserAction {
    private static final Logger LOGGER
             = LogManager.getLogger(MyQueriesAction.class);
    private static final String ERROR_PAGE = "/error.jsp";
    private static final String MY_QUERIES_PAGE = "/my_queries.jsp";

    private static final String USER_ATTRIBUTE = "currentUser";
    @Override
    public Forward execute(HttpServletRequest req, HttpServletResponse resp) throws DAOException, IOException, ServletException {
        HttpSession session = req.getSession();
        User currentUser = (User) session.getAttribute(USER_ATTRIBUTE);
        Forward forward = new Forward(MY_QUERIES_PAGE, false);
        int userId = currentUser.getId();
        QueryService service = provider.getQueryService();
        try {
            List<Query> inQueries = service.readUsersInQueries(userId);
            List<Query> outQueries = service.readUsersOutQueries(userId);

            req.setAttribute("inQueries", inQueries);
            req.setAttribute("outQueries", outQueries);
        } catch (ServiceException e) {
            LOGGER.error(e);
            return new Forward(ERROR_PAGE);
        }


        return forward;
    }
}
