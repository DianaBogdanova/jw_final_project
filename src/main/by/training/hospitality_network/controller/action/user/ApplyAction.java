package by.training.hospitality_network.controller.action.user;

import by.training.hospitality_network.entity.Query;
import by.training.hospitality_network.entity.User;
import by.training.hospitality_network.service.QueryService;
import by.training.hospitality_network.service.ServiceProvider;
import by.training.hospitality_network.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.Date;


public class ApplyAction extends RegisteredUserAction {
    private static final Logger LOGGER
            = LogManager.getLogger(ApplyAction.class);
    private static final String ERROR_PAGE = "/error.jsp";
    private static final String USER_ATTRIBUTE = "currentUser";
    private static final String ADDRESSEE_PARAMETER = "addressee";
    private static final String ARRIVAL_DATE_PARAMETER = "date_from";
    private static final String LEAVING_DATE_PARAMETER = "date_to";

    @Override
    public Forward execute(HttpServletRequest req, HttpServletResponse resp) {
        HttpSession session = req.getSession();
        User user = (User)session.getAttribute(USER_ATTRIBUTE);
        int addresseeId;
        addresseeId = Integer.parseInt(req.getParameter(ADDRESSEE_PARAMETER));
        String arrival = req.getParameter(ARRIVAL_DATE_PARAMETER);
        String leaving = req.getParameter(LEAVING_DATE_PARAMETER);

        Date dateFrom =Date.valueOf(arrival);
        Date dateTo =Date.valueOf(leaving);

        Query query = null;
        if(user != null && addresseeId != 0) {
            query = new Query();
            query.setSurferId(user.getId());
            query.setHostId(addresseeId);
            query.setDateFrom(dateFrom);
            query.setDateTo(dateTo);
            query.setMessage(req.getParameter("message"));

        }
        ServiceProvider provider = ServiceProvider.getInstance();
        QueryService queryService = provider.getQueryService();
        Forward forward = new Forward("/query.html?addressee=" + addresseeId, true);
        boolean isDone;
        try {
            isDone = queryService.apply(query);
            //req.setAttribute("message", "completed successfully");
        } catch (ServiceException e) {
            LOGGER.error(e);
            return new Forward(ERROR_PAGE);
        }
        if(isDone) {
            forward.getAttributes().put("message", "Your request has been sent");
        } else {
            session.setAttribute("error", "Request sending error");
        }
        return forward;
    }
}
