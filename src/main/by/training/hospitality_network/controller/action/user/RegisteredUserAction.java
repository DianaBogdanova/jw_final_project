package by.training.hospitality_network.controller.action.user;

import by.training.hospitality_network.controller.action.Action;
import by.training.hospitality_network.entity.UserRole;

public abstract class RegisteredUserAction extends Action {
    public RegisteredUserAction() {
        getAllowRoles().add(UserRole.REGISTERED_USER);
    }
}
