package by.training.hospitality_network.controller.action.user;

import by.training.hospitality_network.service.QueryService;
import by.training.hospitality_network.service.ServiceProvider;
import by.training.hospitality_network.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RejectAction extends RegisteredUserAction {
    private static final Logger LOGGER
            = LogManager.getLogger(RejectAction.class);
    private static final String QUERY_ID_PARAM = "queryId";
    @Override
    public Forward execute(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        int id = Integer.parseInt(req.getParameter(QUERY_ID_PARAM));
        ServiceProvider provider = ServiceProvider.getInstance();
        QueryService service = provider.getQueryService();
        boolean isDone = false;
        try {
            isDone = service.reject(id);
        } catch (ServiceException e) {
            LOGGER.error(e);
        }
        if(!isDone) {
            req.setAttribute("error", "Can't accept the query");
        }
        return new Forward("/queries.html");
    }
}
