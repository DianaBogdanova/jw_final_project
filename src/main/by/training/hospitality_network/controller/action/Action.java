package by.training.hospitality_network.controller.action;

import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.entity.User;
import by.training.hospitality_network.entity.UserRole;
import by.training.hospitality_network.service.ServiceProvider;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public abstract class Action {
    protected ServiceProvider provider = ServiceProvider.getInstance();

    /** The set of roles for which this action is allowed. */
    private Set<UserRole> allowRoles = new HashSet<>();
    /** The user who call the action. */
    private User user;
    /** The name of the action. */
    private String name;

    /** The get() method for {@see allowRoles} field. */
    public Set<UserRole> getAllowRoles() {
        return allowRoles;
    }

    /** The get() method for {@see user} field. */
    public User getUser() {
        return user;
    }

    /** The set() method for {@see user} field. */
    public void setUser(final User user) {
        this.user = user;
    }

    /** The get() method for {@see name} field. */
    public String getName() {
        return name;
    }

    /** The set() method for {@see name} field.
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }


    public abstract Action.Forward execute(
            final HttpServletRequest request, HttpServletResponse response)
            throws DAOException, IOException, ServletException;

    public static class Forward {

        private String forward;
        private boolean redirect;
        private Map<String, Object> attributes = new HashMap<>();

        public Forward(final String forward_, final boolean redirect_) {
            forward = forward_;
            redirect = redirect_;
        }

        public Forward(final String forward) {
            this(forward, true);
        }

        public String getForward() {
            return forward;
        }

        public void setForward(final String forward) {
            this.forward = forward;
        }

        public boolean isRedirect() {
            return redirect;
        }

        public void setRedirect(final boolean redirect) {
            this.redirect = redirect;
        }

        public Map<String, Object> getAttributes() {
            return attributes;
        }
    }


}
