package by.training.hospitality_network.controller.action;

import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.entity.User;
import by.training.hospitality_network.service.ReviewService;
import by.training.hospitality_network.service.ServiceProvider;
import by.training.hospitality_network.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class SaveReviewAction extends AuthorizedUserAction {
    private static final Logger LOGGER
            = LogManager.getLogger(AddReviewAction.class);
    @Override
    public Forward execute(HttpServletRequest req, HttpServletResponse resp) throws DAOException, IOException, ServletException {
        HttpSession session = req.getSession(false);
        int userId = Integer.parseInt(req.getParameter("id"));
        User user = (User) session.getAttribute("currentUser");
        int authorId = user.getId();
        String text = req.getParameter("textReview");
        int grade = Integer.parseInt(req.getParameter("rating"));

        ServiceProvider provider = ServiceProvider.getInstance();
        ReviewService service = provider.getReviewService();
        boolean isDone;
        try {
            isDone = service.addReview(userId, authorId, text, grade);
            if(!isDone) {
                req.setAttribute("error", "Can not add a review!");
            } else {
                req.setAttribute("message", "Review is successfully added");
            }
        } catch (ServiceException e) {
            req.setAttribute("error", "Error adding a review");
            LOGGER.error(e);
            return new Forward("/error.html");
        }
        Forward forward = new Forward("/user/account.html", true);
        forward.getAttributes().put("otherUser", userId);
        return forward;
    }
}
