package by.training.hospitality_network.controller.action;

import by.training.hospitality_network.entity.UserRole;

import java.util.Arrays;

public abstract class AuthorizedUserAction extends Action {
    public AuthorizedUserAction() {
        getAllowRoles().addAll(Arrays.asList(UserRole.values()));
    }
}