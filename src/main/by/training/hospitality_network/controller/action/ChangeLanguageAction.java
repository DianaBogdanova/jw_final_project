package by.training.hospitality_network.controller.action;

import by.training.hospitality_network.dao.exception.DAOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ChangeLanguageAction extends Action {
    private static final String LANGUAGE_PARAM = "language";
    @Override
    public Forward execute(HttpServletRequest req, HttpServletResponse resp) throws DAOException, IOException, ServletException {
        String language = req.getParameter(LANGUAGE_PARAM);
        HttpSession session = req.getSession();
        session.setAttribute(LANGUAGE_PARAM, language);
        String lastAction = req.getHeader("referer");
        lastAction = lastAction.substring(lastAction.lastIndexOf('/'));
        //String url = req.getRequestURI();
        return new Forward(lastAction, true);
    }
}
