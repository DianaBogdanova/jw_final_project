package by.training.hospitality_network.controller.action;

import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.entity.Review;
import by.training.hospitality_network.entity.User;
import by.training.hospitality_network.entity.UserRole;
import by.training.hospitality_network.service.ReviewService;
import by.training.hospitality_network.service.ServiceProvider;
import by.training.hospitality_network.service.UserService;
import by.training.hospitality_network.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class OtherUserAccountAction extends AuthorizedUserAction {
    private static  final Logger LOGGER
            = LogManager.getLogger(OtherUserAccountAction.class);
    private static final String ACCOUNT_OWNER = "otherUser";
    private static final String ROLE_ATTRIBUTE = "role";


    @Override
    public Forward execute(HttpServletRequest req, HttpServletResponse resp) throws DAOException, IOException, ServletException {
        int otherUserId;
        if(req.getAttribute(ACCOUNT_OWNER) == null) {
            otherUserId = Integer.parseInt(req.getParameter(ACCOUNT_OWNER));
        } else {
            otherUserId = (Integer)(req.getAttribute(ACCOUNT_OWNER));
        }
        ServiceProvider provider = ServiceProvider.getInstance();
        UserService userService = provider.getUserService();
        ReviewService serviceR = provider.getReviewService();
        List<Review> otherReviews;
        try {
            otherReviews = serviceR.readUsersReviews(otherUserId);
        } catch (ServiceException e) {
            req.setAttribute("error", "Account view error");
            LOGGER.error(e);
            return new Forward("/error.html");
        }

        req.setAttribute("otherReviews", otherReviews);
        User user;
        try {
            user = userService.readById(otherUserId);
        } catch (ServiceException e) {
            LOGGER.error(e);
            return new Forward("/error.html");
        }
        req.setAttribute(ACCOUNT_OWNER, user);
        HttpSession session = req.getSession();
        if(session.getAttribute(ROLE_ATTRIBUTE) == UserRole.ADMINISTRATOR) {
            return new Forward("/admin/users_account.jsp", false);
        } else {
            return new Forward("/users_account.jsp", false);
        }
    }
}
