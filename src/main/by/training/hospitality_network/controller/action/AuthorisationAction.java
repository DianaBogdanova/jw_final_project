package by.training.hospitality_network.controller.action;

import by.training.hospitality_network.controller.util.Pagination;
import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.dao.util.EntitiesOnPage;
import by.training.hospitality_network.entity.Review;
import by.training.hospitality_network.entity.User;
import by.training.hospitality_network.service.ReviewService;
import by.training.hospitality_network.service.UserService;
import by.training.hospitality_network.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class AuthorisationAction extends Action {
    private static final Logger LOGGER
            = LogManager.getLogger(AuthorisationAction.class);
    private static final String PARAMETER_EMAIL = "email";
    private static final String PARAMETER_PASSWORD = "password";
    private static final String CREDENTIALS_ERROR
            = "Incorrect login or password. "
            + "You can reenter your login and password and try to reconnect.";

    private static final int REVIEWS_ON_PAGE_NUMBER = 15;

    @Override
    public Action.Forward execute(
            final HttpServletRequest req,
            final HttpServletResponse resp) {

        ReviewService serviceR = provider.getReviewService();

        String email = req.getParameter(PARAMETER_EMAIL);
        String password = req.getParameter(PARAMETER_PASSWORD);

        if (email != null && password != null) {
            HttpSession session = req.getSession();
            UserService service = provider.getUserService();

            User user;
            try {
                user = service.authorisation(email, password);
                if (user == null) {
                    req.setAttribute("error",
                            CREDENTIALS_ERROR);
                } else {
                    session.setAttribute("currentUser", user);
                    session.setAttribute("role", user.getRole());
                    int userId = user.getId();
                    EntitiesOnPage<Review> reviews = new EntitiesOnPage<>();
                    EntitiesOnPage<Review> reviewsOnPage;
                    if (req.getParameter("pagesNumber") == null) {
                        List<Review> allReviews = serviceR.readUsersReviews(userId);
                        reviews.setSearchResult(allReviews);
                        reviews.setEntitiesNumber(allReviews.size());
                        req.setAttribute("pagesNumber",
                                Pagination.calculatePagesNumber(reviews, REVIEWS_ON_PAGE_NUMBER));
                    } else {
                        req.setAttribute("pagesNumber", req.getParameter("pagesNumber"));
                    }

                    int currentPage;
                    if (req.getParameter("currentPage") != null) {
                        currentPage = Integer.parseInt(req.getParameter("currentPage"));
                    } else {
                        currentPage = 1;
                    }

                    int startPosition = Pagination.calculatePageBegin(currentPage, REVIEWS_ON_PAGE_NUMBER);
                    reviewsOnPage
                            = serviceR.readFromStartPosition(
                            startPosition, REVIEWS_ON_PAGE_NUMBER, userId);

                    Forward forward = new Forward("/account.html", true);
                    forward.getAttributes().put("currentPage", currentPage);
                    forward.getAttributes().put("reviews", reviewsOnPage.getSearchResult());
                    forward.getAttributes().put("avatarPath", user.getAvatarPath());
                    setUser(user);
                    LOGGER.info(String.format(
                            "User \"%s\" is logged in from %s (%s:%s)",
                            email, req.getRemoteAddr(), req.getRemoteHost(), req.getRemotePort()));
                    return forward;
                }
            } catch (ServiceException e) {
                req.setAttribute("error", CREDENTIALS_ERROR);
                LOGGER.info(String.format(
                        "User \"%s\" unsuccessfully tried to log in from %s (%s:%s)",
                        email, req.getRemoteAddr(), req.getRemoteHost(), req.getRemotePort()));
                return new Forward("/error.html");
            }
        }
        return null;
    }
}
