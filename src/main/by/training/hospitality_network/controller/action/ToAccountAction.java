package by.training.hospitality_network.controller.action;

import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.entity.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ToAccountAction extends AuthorizedUserAction {
    @Override
    public Forward execute(HttpServletRequest req, HttpServletResponse resp)
            throws DAOException, IOException, ServletException {

        return new Forward("/my_account.jsp", false);
    }
}
