package by.training.hospitality_network.controller.action;

import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.entity.User;
import by.training.hospitality_network.entity.UserRole;
import by.training.hospitality_network.service.CatalogService;
import by.training.hospitality_network.service.UserService;
import by.training.hospitality_network.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class FilterUsersAction extends AuthorizedUserAction {
    private static final Logger LOGGER
            = LogManager.getLogger(FilterUsersAction.class);

    private CatalogService catalogService = provider.getCatalogService();

    private static final String COUNTRY_PARAMETER = "country";
    private static final String IS_HOST_PARAMETER = "is_host";
    private static final String LIST_USERS_PAGE = "/list_users.jsp";
    private static final String LIST_USERS_PAGE_ADMIN = "/admin/list_users.jsp";


    private static final String COUNTRIES_ATTRIBUTE = "countries";
    private static final String CITIES_ATTRIBUTE = "cities";
    private static final String ROLE_ATTRIBUTE = "role";


    @Override
    public Forward execute(HttpServletRequest req, HttpServletResponse resp)
            throws DAOException, IOException, ServletException {
        String  country = req.getParameter(COUNTRY_PARAMETER);

        boolean isHost = false;
        if(req.getParameter(IS_HOST_PARAMETER) != null) {
            isHost = true;
        }

        UserService service = provider.getUserService();
        List<User> users = null;
        List<String> countries = null;
        List<String> cities = null;
        try {
           users = service.filterUsers(country, isHost);
            countries = catalogService.readCountries();
            cities = catalogService.readCities();
        } catch (ServiceException e) {
            LOGGER.error(e);
        }


        req.setAttribute(COUNTRIES_ATTRIBUTE, countries);
        req.setAttribute(CITIES_ATTRIBUTE, cities);

        req.setAttribute("users", users);

        HttpSession session = req.getSession();
        UserRole role = (UserRole) session.getAttribute(ROLE_ATTRIBUTE);
        if(role == UserRole.ADMINISTRATOR) {
            return new Forward(LIST_USERS_PAGE_ADMIN, false);
        } else {
            return new Forward(LIST_USERS_PAGE, false);
        }
    }
}
