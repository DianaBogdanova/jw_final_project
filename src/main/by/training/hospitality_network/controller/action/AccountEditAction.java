package by.training.hospitality_network.controller.action;


import by.training.hospitality_network.service.CatalogService;
import by.training.hospitality_network.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


public class AccountEditAction extends AuthorizedUserAction {
    private static final Logger LOGGER
            = LogManager.getLogger(AccountEditAction.class);
    private CatalogService catalogService = provider.getCatalogService();
    private static final String ACCOUNT_EDIT_PAGE = "/edit_account.jsp";
    private static final String ERROR_PAGE = "/error.jsp";

    private static final String COUNTRIES_ATTRIBUTE = "countries";
    private static final String CITIES_ATTRIBUTE = "cities";

    @Override
    public Forward execute(HttpServletRequest req, HttpServletResponse resp) {
        List<String> countries;
        List<String> cities;
        try {
            countries = catalogService.readCountries();
            cities = catalogService.readCities();
        } catch (ServiceException e) {
            LOGGER.error(e);
            return new Forward(ERROR_PAGE);
        }

        req.setAttribute(COUNTRIES_ATTRIBUTE, countries);
        req.setAttribute(CITIES_ATTRIBUTE, cities);

        return new Forward(ACCOUNT_EDIT_PAGE, false);
    }
}
