package by.training.hospitality_network.controller.action;

import by.training.hospitality_network.entity.User;
import by.training.hospitality_network.service.UserAccountService;
import by.training.hospitality_network.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AccountSaveAction extends AuthorizedUserAction {
    private static final Logger LOGGER
            = LogManager.getLogger(AccountSaveAction.class);
    private static final String EDIT_ACCOUNT_PAGE = "/edit_account.jsp";

    private static final String PHONE_PARAMETER = "phone";
    private static final String PROFILE_PARAMETER = "profile";
    private static final String IS_HOST_PARAMETER = "is_host";
    private static final String COUNTRY_PARAMETER = "country";
    private static final String CITY_PARAMETER = "city";

    private static final String USER_ATTRIBUTE = "currentUser";
    private static final String MESSAGE_ATTRIBUTE = "message";
    private static final String ERROR_ATTRIBUTE = "error";


    @Override
    public Forward execute(HttpServletRequest req, HttpServletResponse resp) {
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute(USER_ATTRIBUTE);
        String phone = req.getParameter(PHONE_PARAMETER);
        String profile = req.getParameter(PROFILE_PARAMETER);
        String country = req.getParameter(COUNTRY_PARAMETER);
        String city = req.getParameter(CITY_PARAMETER);

        boolean isHost = false;
        if(req.getParameter(IS_HOST_PARAMETER) != null) {
            isHost = true;
        }
        UserAccountService service = provider.getAccountService();
        String message;
        try {
            message = service.editAccount(phone, isHost, profile, user);
            user.setPhone(phone);
            user.setProfile(profile);
            user.setHost(isHost);
            user.setCity(city);
            user.setCountry(country);
            req.setAttribute(MESSAGE_ATTRIBUTE, message);
            req.setAttribute(USER_ATTRIBUTE, user);
        } catch (ServiceException e) {
            req.setAttribute(ERROR_ATTRIBUTE, "Data saving error");
            LOGGER.error("Account editing is failed", e);
        }
        return new Forward(EDIT_ACCOUNT_PAGE, false);
    }
}
