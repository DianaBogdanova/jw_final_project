package by.training.hospitality_network.controller;


import by.training.hospitality_network.controller.action.Action;
import by.training.hospitality_network.dao.exception.DAOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

@MultipartConfig
public class Controller extends HttpServlet {
    private static final Logger LOGGER
            = LogManager.getLogger(Controller.class);
    private static final String REDIRECTED_DATA_ATTRIBUTE
            = "redirectedData";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            handleRequest(req, resp);
        } catch (ServletException | IOException e) {

        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//C,U,D operations
        handleRequest(req, resp);
    }
    //CRUD operations: create, read, update, delete



    private void handleRequest(HttpServletRequest request,
                        HttpServletResponse response)
            throws ServletException, IOException {
        Action action = (Action) request.getAttribute("action");
        try {
            HttpSession session = request.getSession(false);
            if (session != null) {
                @SuppressWarnings("unchecked")
                Map<String, Object> attributes = (Map<String, Object>)
                        session.getAttribute(REDIRECTED_DATA_ATTRIBUTE);
                if (attributes != null) {
                    for(String key : attributes.keySet()) {
                        request.setAttribute(key, attributes.get(key));
                    }
                    session.removeAttribute(REDIRECTED_DATA_ATTRIBUTE);
                }
            }

            Action.Forward forward
                    = action.execute(request, response);
            if (session != null && forward != null
                    && !forward.getAttributes().isEmpty()) {
                session.setAttribute(REDIRECTED_DATA_ATTRIBUTE,
                        forward.getAttributes());
            }

            String requestedUri = request.getRequestURI();
            if (forward != null && forward.isRedirect()) {
                String redirectedUri
                        = request.getContextPath() + forward.getForward();
                LOGGER.debug(String.format("Request for URI \"%s\" id"
                                + " redirected to URI \"%s\"", requestedUri,
                        redirectedUri));
                response.sendRedirect(redirectedUri);
            } else {
                String jspPage;
                if (forward != null) {
                    jspPage = forward.getForward();
                } else {
                    jspPage = action.getName() + ".jsp";
                }
                jspPage = "/WEB-INF/jsp" + jspPage;
                LOGGER.debug(String.format("Request for URI \"%s\" is forwarded"
                        + " to JSP \"%s\"", requestedUri, jspPage));
                getServletContext().getRequestDispatcher(jspPage)
                        .forward(request, response);
            }
        } catch (DAOException e) {
            LOGGER.error("It is impossible to process request", e);
            request.setAttribute("error", "Ошибка обработки данных");
            getServletContext().getRequestDispatcher(
                    "/WEB-INF/jsp/error.jsp").forward(request, response);
        }
    }
}
