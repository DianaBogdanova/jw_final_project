package by.training.hospitality_network.controller.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServletRequest;

@WebListener
public class RequestListener implements ServletRequestListener {
    private static final Logger LOGGER
            = LogManager.getLogger(RequestListener.class);
    public void requestInitialized(ServletRequestEvent ev) {
        HttpServletRequest req = (HttpServletRequest) ev.getServletRequest();
        String uri = "Request Initialized for " + req.getRequestURI();
        String id = "Request Initialized with ID = "+ req.getRequestedSessionId();

        Integer reqCount = (Integer)req.getSession().getAttribute("counter");
        if(reqCount == null) {
            reqCount = 0;
        }
        LOGGER.info(uri + "\n" +id + ", Request Counter =" + reqCount);
    }
    public void requestDestroyed(ServletRequestEvent ev) {
        LOGGER.info("Request Destroyed: "
                + ev.getServletRequest().getAttribute("lifecycle"));

    }
}