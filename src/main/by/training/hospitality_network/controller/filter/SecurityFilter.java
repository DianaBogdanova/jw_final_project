package by.training.hospitality_network.controller.filter;
import by.training.hospitality_network.controller.action.Action;
import by.training.hospitality_network.controller.action.MainAction;
import by.training.hospitality_network.entity.User;
import by.training.hospitality_network.entity.UserRole;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Set;


public class SecurityFilter implements Filter {


    private static Logger LOGGER = LogManager.getLogger(SecurityFilter.class);

    @Override
    public void init(FilterConfig filterConfig) {}

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if(request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
            HttpServletRequest httpRequest = (HttpServletRequest)request;
            HttpServletResponse httpResponse = (HttpServletResponse)response;
            Action action = (Action)httpRequest.getAttribute("action");
            Set<UserRole> allowRoles = action.getAllowRoles();
            String userName = "user";
            HttpSession session = httpRequest.getSession(false);
            User user = null;
            if(session != null) {
                user = (User)session.getAttribute("user");
                action.setUser(user);
                String errorMassage = (String)session.getAttribute("SecurityFilterMassage");
                if(errorMassage != null) {
                    httpRequest.setAttribute("massage", errorMassage);
                    session.removeAttribute("SecurityFilterMessage");
                }
            }
            boolean canExecute = allowRoles == null;
            if(user != null) {
                userName = "\"" + user.getEmail() + "\" user";
                canExecute = canExecute || allowRoles.contains(user.getRole());
            }
            if(canExecute) {
                chain.doFilter(request, response);
            } else {
                LOGGER.info(String.format(
                        "Trying of %s access to forbidden resource \"%s\"", userName, action.getName()));
                if(session != null && action.getClass() != MainAction.class) {
                    session.setAttribute("SecurityFilterMessage", "Доступ запрещён");
                }
                httpResponse.sendRedirect(httpRequest.getContextPath() + "/login.html");
            }
        } else {
            LOGGER.error("It is impossible to use HTTP filter");
            request.getServletContext().getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
        }
    }

    @Override
    public void destroy() {}
}
