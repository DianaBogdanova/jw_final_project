package by.training.hospitality_network.controller.filter;

import by.training.hospitality_network.controller.action.*;
import by.training.hospitality_network.controller.action.administrator.*;
import by.training.hospitality_network.controller.action.user.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;


public class ActionFromUriFilter implements Filter {
    private static final Logger LOGGER = LogManager.getLogger(ActionFromUriFilter.class);

    private static Map<String, Class<? extends Action>> commands
            = new ConcurrentHashMap<>();

    static {
        commands.put("/", MainAction.class);
        commands.put("/main", MainAction.class);
        commands.put("/authorisation", AuthorisationAction.class);
        commands.put("/registration", RegistrationAction.class);
        commands.put("/signUp", SaveUserAction.class);
        commands.put("/logout", LogoutAction.class);

        commands.put("/account/edit", AccountEditAction.class);
        commands.put("/account/save", AccountSaveAction.class);
        commands.put("/account", ToAccountAction.class);

        commands.put("/search", SearchUsersAction.class);
        commands.put("/users/filter", FilterUsersAction.class);

        commands.put("/user/account", OtherUserAccountAction.class);

        commands.put("/query", QueryAction.class);
        commands.put("/queries", MyQueriesAction.class);

        commands.put("/apply", ApplyAction.class);

        commands.put("/reviews", MyReviewsAction.class);
        commands.put("/review/new", AddReviewAction.class);
        commands.put("/review/save", SaveReviewAction.class);
        commands.put("/review/delete", DeleteReviewAction.class);

        commands.put("/upload", ToUploadAction.class);
        commands.put("/avatar/upload", UploadPhotoAction.class);

        commands.put("/reject", RejectAction.class);
        commands.put("/accept", AcceptAction.class);
        commands.put("/cancel", CancelAction.class);

        commands.put("/place/change", PlaceChangeAction.class);

        commands.put("/user/lock", LockUserAction.class);
        commands.put("/user/unlock", UnlockUserAction.class);

        commands.put("/language", ChangeLanguageAction.class);

    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {}

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if(request instanceof HttpServletRequest) {
            HttpServletRequest httpRequest = (HttpServletRequest)request;
            String contextPath = httpRequest.getContextPath();
            String uri = httpRequest.getRequestURI();
            LOGGER.debug(String.format("Starting of processing of request for URI \"%s\"", uri));
            int beginAction = contextPath.length();
            int endAction = uri.lastIndexOf('.');
            String actionName;
            if(endAction >= 0) {
                actionName = uri.substring(beginAction, endAction);
            } else {
                actionName = uri.substring(beginAction);
            }
            Class<? extends Action> actionClass = commands.get(actionName);
            try {
                Action action = actionClass.getConstructor().newInstance();
                action.setName(actionName);
                httpRequest.setAttribute("action", action);
                chain.doFilter(request, response);
            } catch (InstantiationException | IllegalAccessException | NullPointerException e) {
                LOGGER.error("It is impossible to create action handler object", e);
                httpRequest.setAttribute("error", String.format("Запрошенный адрес %s не может быть обработан сервером", uri));
                httpRequest.getServletContext().getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
            } catch (NoSuchMethodException e) {
                LOGGER.error("Matching method is not found", e);
            } catch (InvocationTargetException e) {
                LOGGER.error("Constructor of " + actionClass.getSimpleName()
                        + " throws an exception", e);
            }
        } else {
            LOGGER.error("It is impossible to use HTTP filter");
            request.getServletContext().getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
        }
    }

    @Override
    public void destroy() {}
}