package by.training.hospitality_network.controller.filter;

import javax.servlet.*;
import java.io.IOException;

public class EncodingFilter implements Filter {
    private static final String ENCODING_PARAMETER = "encoding";
    private static final String ENCODING_DEFAULT = "UTF-8";

    private String code;
    @Override
    public void init(final FilterConfig fConfig) {
        code = fConfig.getInitParameter(ENCODING_PARAMETER);
        if (code == null) {
            code = ENCODING_DEFAULT;
        }
    }

    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        String codeRequest = request.getCharacterEncoding();
        if (!code.equalsIgnoreCase(codeRequest)) {
            request.setCharacterEncoding(code);
            response.setCharacterEncoding(code);
        }
        chain.doFilter(request, response);

    }

    @Override
    public void destroy() {
        code = null;
    }

}
