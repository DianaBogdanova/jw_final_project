package by.training.hospitality_network.main;


import org.springframework.security.crypto.bcrypt.BCrypt;

public class Main {

    public static void main(String[] args) {
        System.out.println(BCrypt.hashpw("Lemanlem64", BCrypt.gensalt()));
}
}
