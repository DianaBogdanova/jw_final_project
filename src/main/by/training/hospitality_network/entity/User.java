package by.training.hospitality_network.entity;

import java.io.Serializable;
import java.util.Objects;

public class User extends Entity implements Serializable {
    private static final long serialVersionUID = -6331540027622834743L;

    private String name;
    private String surname;
    private String country;
    private String city;
    private String email;

    private UserRole role;
    private String phone;
    private boolean isHost;
    private int rating;
    private String profile;
    private boolean isActive;

    private String avatarPath;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean getIsHost() {
        return isHost;
    }

    public void setHost(boolean status) {
        isHost = status;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getAvatarPath() {
        return avatarPath;
    }

    public void setAvatarPath(String avatarPath) {
        this.avatarPath = avatarPath;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return isHost == user.isHost &&
                rating == user.rating &&
                isActive == user.isActive &&
                Objects.equals(name, user.name) &&
                Objects.equals(surname, user.surname) &&
                Objects.equals(country, user.country) &&
                Objects.equals(city, user.city) &&
                Objects.equals(email, user.email) &&
                role == user.role &&
                Objects.equals(phone, user.phone) &&
                Objects.equals(profile, user.profile) &&
                Objects.equals(avatarPath, user.avatarPath);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, surname, country, city, email, role, phone, isHost, rating, profile, isActive, avatarPath);
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
