package by.training.hospitality_network.entity;

public enum UserRole {
    ADMINISTRATOR("администратор"),
    REGISTERED_USER("зарегистрированный пользователь");

    private String name;

    UserRole(final String roleName) {
        name = roleName;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return ordinal();
    }

    public static UserRole getById(final Integer id) {
        return UserRole.values()[id];
    }
}
