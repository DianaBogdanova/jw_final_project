package by.training.hospitality_network.entity;

import java.sql.Date;
import java.util.Objects;

public class Query extends Entity {
    public enum Condition {
        ACCEPTED, REJECTED, PENDING
    }
    private int surferId;
    private String surferName;
    private int hostId;
    private String hostName;

    private Date dateFrom;
    private Date dateTo;
    private Condition condition;
    private String message;

    public String getSurferName() {
        return surferName;
    }

    public void setSurferName(String surferName) {
        this.surferName = surferName;
    }

    public int getHostId() {
        return hostId;
    }

    public void setHostId(int hostId) {
        this.hostId = hostId;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public int getCondition() {
        return condition.ordinal();
    }

    public void setCondition(final Condition condition) {
        this.condition = condition;
    }
    public void setCondition(final int id) {
        condition = Condition.values()[id];
    }

    public String getMessage() {
        return message;
    }

    public int getSurferId() {
        return surferId;
    }

    public void setSurferId(int surferId) {
        this.surferId = surferId;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Query query = (Query) o;
        return surferName == query.surferName &&
                hostId == query.hostId &&
                Objects.equals(dateFrom, query.dateFrom) &&
                Objects.equals(dateTo, query.dateTo) &&
                condition == query.condition;
    }

    @Override
    public int hashCode() {

        return Objects.hash(surferName, hostId, dateFrom, dateTo, condition);
    }
}
