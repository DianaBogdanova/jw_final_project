package by.training.hospitality_network.entity;

import java.sql.Blob;
import java.util.Base64;

public class Image extends Entity {
    private Blob photo;
    private int userId;
    private int placeId;
    private String path;

    private byte[] image;
    private String base64Image;

    public Blob getPhoto() {
        return photo;
    }

    public void setPhoto(Blob image) {
        this.photo = image;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getPlaceId() {
        return placeId;
    }

    public void setPlaceId(int placeId) {
        this.placeId = placeId;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getBase64Image() {
        base64Image = Base64.getEncoder().encodeToString(this.image);
        return base64Image;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
