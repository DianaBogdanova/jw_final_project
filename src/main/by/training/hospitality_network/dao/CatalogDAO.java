package by.training.hospitality_network.dao;

import by.training.hospitality_network.dao.exception.DAOException;

import java.util.List;

public interface CatalogDAO {
    List<String> readCountries() throws DAOException;
    List<String> readCities() throws DAOException;
    boolean cityMatchesCountry(final String city, final String country) throws DAOException;
}
