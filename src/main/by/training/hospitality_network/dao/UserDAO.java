package by.training.hospitality_network.dao;

import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.dao.util.EntitiesOnPage;
import by.training.hospitality_network.entity.User;

import java.util.List;

public interface UserDAO extends CommonDAO<User>{
    User authentication(String login, String password) throws DAOException;
    boolean registration(String name, String surname,
                         String password, String email)
            throws DAOException;
    List<User> readAll() throws DAOException;
    EntitiesOnPage<User> readFromStartPosition(
            int startPosition, int usersNumber) throws DAOException;
    List<User> filterUsers(String country, boolean isHost) throws DAOException;

    int calcRating(final int userId) throws DAOException;

    boolean lock(int id) throws DAOException;
    boolean unlock(int id) throws DAOException;
}
