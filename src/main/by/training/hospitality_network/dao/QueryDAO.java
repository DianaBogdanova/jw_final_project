package by.training.hospitality_network.dao;

import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.entity.Query;

import java.util.List;

public interface QueryDAO extends CommonDAO<Query> {
    List<Query> readUsersInQueries(int userId) throws DAOException;
    List<Query> readUsersOutQueries(int userId) throws DAOException;


    boolean accept(int id) throws DAOException;
    boolean reject(int id) throws DAOException;
    boolean cancel(int id) throws DAOException;

}
