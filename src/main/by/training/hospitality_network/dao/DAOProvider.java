package by.training.hospitality_network.dao;

import by.training.hospitality_network.dao.impl.*;

public final class DAOProvider {
    private static final DAOProvider INSTANCE
            = new DAOProvider();
    private final UserDAO userDaoImpl = new UserDAOImpl();
    private final QueryDAO queryDAOImpl = new QueryDAOImpl();
    private final UserAccountDAO userAccountDAO
            = new UserAccountDAOImpl();
    private final GalleryDAO galleryDAO
            = new GalleryDAOImpl();
    private final QueryDAO queryDAO
            = new QueryDAOImpl();
    private final ReviewDAO reviewDAO
            = new ReviewDAOImpl();
    private final CatalogDAO catalogDAO
            = new CatalogDAOImpl();

    private DAOProvider() {

    }

    public static DAOProvider getInstance() {
        return INSTANCE;
    }
    public UserDAO getUserDaoImpl() {
        return userDaoImpl;
    }
    public QueryDAO getQueryDAOImpl() {
        return queryDAOImpl;
    }
    public UserAccountDAO getUserAccountDAO() {
        return userAccountDAO;
    }
    public GalleryDAO getGalleryDAO() {
        return galleryDAO;
    }
    public QueryDAO getQueryDAO() {
        return queryDAO;
    }

    public ReviewDAO getReviewDAO() {
        return reviewDAO;
    }

    public CatalogDAO getCatalogDAO() {
        return catalogDAO;
    }
}
