package by.training.hospitality_network.dao;

import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.entity.User;

import java.util.List;

public interface UserAccountDAO {
    boolean editAccount(String phone,
                        boolean isHost, String profile, User user) throws DAOException;
}
