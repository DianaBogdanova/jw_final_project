package by.training.hospitality_network.dao.impl;

import by.training.hospitality_network.dao.exception.ConnectionPoolException;
import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.dao.util.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public abstract class DAOImpl {

    private static final ConnectionPool POOL
            = ConnectionPool.getInstance();

    public static ConnectionPool getPool() {
        return POOL;
    }

    public void delete(int identity, String sql) throws DAOException {
        Connection con = null;
        PreparedStatement statement = null;
        try {
            con = POOL.takeConnection();
            statement = con.prepareStatement(sql);
            statement.setInt(1, identity);
            statement.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(
                    "SQL exception (request or table failed): " + e);
        } finally {
            POOL.finallyClose(con, statement);
        }
    }
}
