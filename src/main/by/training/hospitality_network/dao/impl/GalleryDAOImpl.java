package by.training.hospitality_network.dao.impl;

import by.training.hospitality_network.dao.GalleryDAO;
import by.training.hospitality_network.dao.exception.ConnectionPoolException;
import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.entity.Image;

import java.io.InputStream;
import java.sql.*;

public class GalleryDAOImpl extends DAOImpl implements GalleryDAO {
    private static final String QUERY_INSERT_IMAGE
            = "INSERT INTO avatars (`user_id`, `path`) "
            + "VALUES (?, ?)";
    private static final String SQL_CREATE_IMAGE
            = "INSERT INTO `user_photos` (`user_id`, `path`) "
            + "VALUES (?, ?)";
    private static final String QUERY_GET_USER_PHOTO
            = "SELECT `photo` FROM avatars WHERE `user_id` = ?";
    private static final String SQL_CHANGE_AVATAR
            = "UPDATE avatars SET `path`=? WHERE `user_id`=?";

    public boolean create(final InputStream fileContent, final int id) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        int result;
        try {
            con = getPool().takeConnection();
            ps = con.prepareStatement(QUERY_INSERT_IMAGE);
            ps.setInt(1, id);
            ps.setBinaryStream(2, fileContent);
            result = ps.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            getPool().finallyClose(con, ps);
        }
        return result==1;
    }

    @Override
    public boolean create(final Image image) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        int result;
        try {
            con = getPool().takeConnection();
            ps = con.prepareStatement(SQL_CHANGE_AVATAR);
            ps.setString(1, image.getPath());
            ps.setInt(2, image.getUserId());
            result = ps.executeUpdate();

            if(result != 1) {
                con = getPool().takeConnection();
                ps.close();
                ps = con.prepareStatement(QUERY_INSERT_IMAGE);
                ps.setInt(1, image.getUserId());
                ps.setString(2, image.getPath());
                result = ps.executeUpdate();
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            getPool().finallyClose(con, ps);
        }
        return result == 1;
    }

    @Override
    public Image read(int id) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = getPool().takeConnection();
            ps = con.prepareStatement(QUERY_GET_USER_PHOTO);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            Image image = null;
            if(rs.next()) {
                image  = new Image();
                Blob blob = rs.getBlob("photo");
                image.setPhoto(blob);
                image.setUserId(id);
            }
            return image;
        } catch (SQLException  | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            getPool().finallyClose(con, ps,rs);
        }

    }

    @Override
    public boolean update(Image entity) throws DAOException {
return false;
    }

    @Override
    public boolean delete(int identity) throws DAOException {
return false;
    }
}
