package by.training.hospitality_network.dao.impl;

import by.training.hospitality_network.dao.CatalogDAO;
import by.training.hospitality_network.dao.exception.ConnectionPoolException;
import by.training.hospitality_network.dao.exception.DAOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CatalogDAOImpl extends DAOImpl implements CatalogDAO {
    private static final String SQL_SELECT_COUNTRIES
            = "SELECT `country` FROM `countries_catalog`";
    private static final String SQL_SELECT_CITIES
            = "SELECT `city` FROM `cities_catalog`";
    private static final String SQL_CHECK_CITY
            = "SELECT `country` FROM `cities_catalog` LEFT JOIN `countries_catalog` "
            + "ON `country_id`=countries_catalog.id WHERE `city`=?";
    @Override
    public List<String> readCountries() throws DAOException {
        Connection con = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            con = getPool().takeConnection();
            statement = con.prepareStatement(SQL_SELECT_COUNTRIES);
            resultSet = statement.executeQuery();
            List<String> countries = new ArrayList<>();
            return fillCountries(resultSet, countries);
        } catch(SQLException | ConnectionPoolException e) {
            throw new DAOException(
                    DAOException.SQL_EXCEPTION_MESSAGE, e);
        } finally {
            getPool().finallyClose(con, statement, resultSet);
        }
    }

    @Override
    public List<String> readCities() throws DAOException {
        Connection con = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            con = getPool().takeConnection();
            statement = con.prepareStatement(SQL_SELECT_CITIES);
            resultSet = statement.executeQuery();
            List<String> cities = new ArrayList<>();
            return fillCities(resultSet, cities);
        } catch(SQLException | ConnectionPoolException e) {
            throw new DAOException(
                    DAOException.SQL_EXCEPTION_MESSAGE, e);
        } finally {
            getPool().finallyClose(con, statement, resultSet);
        }
    }

    public boolean cityMatchesCountry(final String city, final String country) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        String cntr = null;
        try {
            con = getPool().takeConnection();
            ps = con.prepareStatement(SQL_CHECK_CITY);
            ps.setString(1, city);
            resultSet = ps.executeQuery();
            if(resultSet.next()) {
                cntr = resultSet.getString("country");
                return cntr.equals(country);
            } else {
                return false;
            }
        } catch(SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            getPool().finallyClose(con, ps, resultSet);
        }
    }
    private List<String> fillCountries(final ResultSet resultSet,
                                       final List<String> countries) throws SQLException {
        while(resultSet.next()){
            countries.add(resultSet.getString("country"));
        }
        return countries;
    }
    private List<String> fillCities(final ResultSet resultSet,
                                    final List<String> cities) throws SQLException {
        while(resultSet.next()){
            cities.add(resultSet.getString("city"));
        }
        return cities;
    }
}
