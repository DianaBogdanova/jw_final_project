package by.training.hospitality_network.dao.impl;

import by.training.hospitality_network.dao.QueryDAO;
import by.training.hospitality_network.dao.exception.ConnectionPoolException;
import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.entity.Query;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class QueryDAOImpl extends DAOImpl implements QueryDAO {
    private static final String SQL_INSERT_QUERY
            = "INSERT INTO `queries`("
            + "`surfer_id`, `host_id`, "
            + "`date_from`, `date_to`, `message`)"
            +" VALUES (?, ?, ?, ?, ?)";
    private static final String SQL_READ_IN_QUERIES
            = "SELECT queries.`id`, `name`, `surname`, `surfer_id`, `date_from`, "
            + "`date_to`, `condition`, `message` FROM `queries` LEFT JOIN "
            + "users on users.id = queries.surfer_id WHERE `host_id` = ? ORDER BY `date_to` DESC";

    private static final String SQL_READ_OUT_QUERIES
            = "SELECT queries.`id`, `name`, `surname`, `host_id`, `date_from`, "
            + "`date_to`, `condition`, `message` FROM `queries` LEFT JOIN "
            + "users on users.id = queries.host_id "
            + "WHERE `surfer_id` = ? ORDER BY `date_to` DESC";

    private static final String SQL_REJECT_QUERY
            = "UPDATE `queries` SET `condition`=1 WHERE id=?";

    private static final String SQL_ACCEPT_QUERY
            = "UPDATE `queries` SET `condition`=0 WHERE id=?";

    private static final String SQL_CANCEL_QUERY
            = "DELETE FROM `queries` WHERE `id`=?";

    @Override
    public boolean create(final Query query) throws DAOException {
        Connection con = null;
        PreparedStatement st = null;
        int result;
        try{
            con = getPool().takeConnection();
            st = con.prepareStatement(SQL_INSERT_QUERY);
            st.setInt(1, query.getSurferId());
            st.setInt(2, query.getHostId());
            st.setDate(3, query.getDateFrom());
            st.setDate(4, query.getDateTo());
            st.setString(5, query.getMessage());
            result = st.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            getPool().finallyClose(con, st);
        }
        return result == 1;
    }

    @Override
    public Query read(int identity) throws DAOException {
        return null;
    }

    @Override
    public boolean update(Query entity) throws DAOException {
return false;
    }

    @Override
    public boolean delete(int identity) throws DAOException {
return false;
        }

    @Override
    public List<Query> readUsersInQueries(final int userId) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Query> outQueries;
        try {
            con = getPool().takeConnection();
            ps = con.prepareStatement(SQL_READ_IN_QUERIES);
            ps.setInt(1, userId);
            rs = ps.executeQuery();
            outQueries = new ArrayList<>();
            while (rs.next()) {
                Query query = createInQuery(rs, userId);
                outQueries.add(query);
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            getPool().finallyClose(con, ps, rs);
        }
        return outQueries;
    }

    @Override
    public List<Query> readUsersOutQueries(int userId) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Query> outQueries;
        try {
            con = getPool().takeConnection();
            ps = con.prepareStatement(SQL_READ_OUT_QUERIES);
            ps.setInt(1, userId);
            rs = ps.executeQuery();
            outQueries = new ArrayList<>();
            while (rs.next()) {
                Query query = createOutQuery(rs, userId);
                outQueries.add(query);
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            getPool().finallyClose(con, ps, rs);
        }
        return outQueries;
    }

    @Override
    public boolean accept(int id) throws DAOException {
        return updateQuery(id, SQL_ACCEPT_QUERY);
    }

    @Override
    public boolean reject(final int id) throws DAOException {
        return updateQuery(id, SQL_REJECT_QUERY);

    }

    @Override
    public boolean cancel(final int id) throws DAOException {
        return updateQuery(id, SQL_CANCEL_QUERY);
    }

    private boolean updateQuery(final int id, final String sql) throws DAOException {
        Connection con = null;
        PreparedStatement st = null;
        int result;
        try{
            con = getPool().takeConnection();
            st = con.prepareStatement(sql);
            st.setInt(1, id);
            result = st.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            getPool().finallyClose(con, st);
        }
        return result == 1;
    }


    private Query createInQuery(final ResultSet rs, final int hostId) throws SQLException {
        Query query = new Query();
        String surferName
                = rs.getString("name")
                + " " + rs.getString ("surname");
        query.setHostId(hostId);
        query.setSurferName(surferName);
        query.setId(rs.getInt("id"));
        query.setSurferId(rs.getInt("surfer_id"));
        query.setDateFrom(rs.getDate("date_from"));
        query.setDateTo(rs.getDate("date_to"));
        query.setCondition(rs.getInt("condition"));
        query.setMessage(rs.getString("message"));

        return query;
    }
    private Query createOutQuery(final ResultSet rs, final int surferId) throws SQLException {
        Query query = new Query();
        String hostName
                = rs.getString("name")
                + " " + rs.getString ("surname");
        query.setHostId(rs.getInt("host_id"));
        query.setHostName(hostName);
        query.setId(rs.getInt("id"));
        query.setSurferId(surferId);
        query.setDateFrom(rs.getDate("date_from"));
        query.setDateTo(rs.getDate("date_to"));
        query.setCondition(rs.getInt("condition"));
        query.setMessage(rs.getString("message"));

        return query;
    }
}
