package by.training.hospitality_network.dao.impl;

import by.training.hospitality_network.dao.ReviewDAO;
import by.training.hospitality_network.dao.exception.ConnectionPoolException;
import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.dao.util.EntitiesOnPage;
import by.training.hospitality_network.entity.Review;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ReviewDAOImpl extends DAOImpl implements ReviewDAO {
    private static final String SQL_READ_USERS_REVIEWS
            = "SELECT reviews.id, `name`, `surname`, `user_id`, `text`, `rating`"
            + "FROM `reviews` LEFT JOIN "
            + "users on users.id = reviews.author_id "
            + "WHERE `user_id` = ?";
    private static final String SQL_ADD_REVIEW
            = "INSERT INTO `reviews` (`user_id`, `author_id`, `text`, `rating`) "
            + "VALUES (?, ?, ?, ?)";
    private static final String SQL_DELETE_REVIEW
            = "DELETE FROM `reviews` WHERE id=?";

    private static final String SQL_READ_REVIEWS_LIMIT
            = "SELECT reviews.`id`, `name`, `surname`, `user_id`, `text`, `rating` "
            + "FROM `reviews` LEFT JOIN `users` ON users.id = reviews.author_id "
            + "WHERE `user_id` = ? "
            + "LIMIT ?,?";

    @Override
    public boolean create(final Review review) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int result;
        try {
            con = getPool().takeConnection();
            ps = con.prepareStatement(SQL_ADD_REVIEW);
            ps.setInt(1, review.getUserId());
            ps.setInt(2, review.getAuthorId());
            ps.setString(3, review.getText());
            ps.setInt(4, review.getRating());
            result = ps.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            getPool().finallyClose(con, ps, rs);
        }
        return result == 1;
    }

    @Override
    public Review read(int identity) throws DAOException {
        return null;
    }

    @Override
    public boolean update(Review entity) throws DAOException {
return false;
    }

    @Override
    public boolean delete(final int id) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        int result;
        try {
            con = getPool().takeConnection();
            ps = con.prepareStatement(SQL_DELETE_REVIEW);
            ps.setInt(1, id);
            result = ps.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException(e);
        } finally {
            getPool().finallyClose(con, ps);
        }
        return result == 1;
    }

    @Override
    public List<Review> readUsersReviews(int userId) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Review> reviews;
        try {
            con = getPool().takeConnection();
            ps = con.prepareStatement(SQL_READ_USERS_REVIEWS);
            ps.setInt(1, userId);
            rs = ps.executeQuery();
            reviews = new ArrayList<>();
            while (rs.next()) {
                Review review = createReview(rs, userId);
                reviews.add(review);
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            getPool().finallyClose(con, ps, rs);
        }
        return reviews;
    }

    public EntitiesOnPage<Review> readFromStartPosition(
            int startPosition, int reviewsNumber, int userId) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        try {
            con = getPool().takeConnection();
            ps = con.prepareStatement(SQL_READ_REVIEWS_LIMIT);
            ps.setInt(1, userId);
            ps.setInt(2, startPosition);
            ps.setInt(3, reviewsNumber);
            resultSet = ps.executeQuery();
            List<Review> reviews = new ArrayList<>();
            while (resultSet.next()) {
                Review review = createReview(resultSet, userId);
                reviews.add(review);
            }
            EntitiesOnPage<Review> reviewsOnPage = new EntitiesOnPage<>();
            reviewsOnPage.setEntitiesNumber(reviews.size());
            reviewsOnPage.setSearchResult(reviews);
            return reviewsOnPage;
        } catch(SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            getPool().finallyClose(con, ps, resultSet);
        }
    }


    private Review createReview(final ResultSet rs, final int userId) throws SQLException {
        Review review = new Review();
        review.setId(rs.getInt("reviews.id"));
        String authorName
                = rs.getString("name")
                + " " + rs.getString ("surname");
        review.setAuthorName(authorName);
        review.setUserId(userId);
        review.setText(rs.getString("text"));
        review.setRating(rs.getInt("rating"));

        return review;
    }
}
