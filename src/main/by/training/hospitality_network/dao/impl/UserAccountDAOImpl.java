package by.training.hospitality_network.dao.impl;

import by.training.hospitality_network.dao.UserAccountDAO;
import by.training.hospitality_network.dao.exception.ConnectionPoolException;
import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.entity.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserAccountDAOImpl extends DAOImpl implements UserAccountDAO {
    private static final String QUERY_SELECT_COUNTRIES
            = "SELECT `country` FROM `countries_catalog`";
    private static final String QUERY_SELECT_CITIES
            = "SELECT `city` FROM `cities_catalog`";
    private static final String QUERY_EDIT_ACCOUNT
            = "UPDATE users_info SET `phone`= ?, `is_host`=?, `profile`=? "
            + "WHERE `id` = ?;";


    @Override
    public boolean editAccount(final String phone, final boolean isHost,
                               final String profile,
                               final User user) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        int accountEditingResult = 0;

        try {
            con = getPool().takeConnection();
            ps = con.prepareStatement(QUERY_EDIT_ACCOUNT);
            ps.setLong(1, Long.parseLong(phone));
            ps.setBoolean(2, isHost);
            ps.setString(3, profile);
            ps.setInt(4, user.getId());
            accountEditingResult = ps.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
           throw new DAOException(e);
        } finally {
            getPool().finallyClose(con, ps);
        }
        return accountEditingResult == 1;
    }

    private List<String> fillCountries(final ResultSet resultSet,
                                       final List<String> countries) throws SQLException {
        while(resultSet.next()){
            countries.add(resultSet.getString("country"));
        }
        return countries;
    }
    private List<String> fillCities(final ResultSet resultSet,
                                    final List<String> cities) throws SQLException {
        while(resultSet.next()){
            cities.add(resultSet.getString("city"));
        }
        return cities;
    }

}
