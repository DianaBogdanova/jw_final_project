package by.training.hospitality_network.dao.impl;

import by.training.hospitality_network.dao.UserDAO;
import by.training.hospitality_network.dao.exception.ConnectionPoolException;
import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.dao.util.EntitiesOnPage;
import by.training.hospitality_network.entity.User;
import by.training.hospitality_network.entity.UserRole;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.crypto.bcrypt.BCrypt;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class UserDAOImpl extends DAOImpl
        implements UserDAO {
    private static final Logger LOGGER
            = LogManager.getLogger(UserDAOImpl.class);

    private static final String SQL_FIND_USER
            = "SELECT users.`id`, `surname`, `name`, `password`, `country`, `city`, "
            + "`role`, `phone`, is_host, `rating`, `profile`, `path`, `is_active` "
            + "FROM users LEFT JOIN users_info ON users.id = users_info.id "
            + "LEFT JOIN avatars ON `user_id` = users.id LEFT JOIN `countries_catalog` "
            + "ON users_info.country_id = countries_catalog.id LEFT JOIN `cities_catalog` c2 "
            + "ON users_info.city_id = c2.id "
            + "WHERE email = ?";

    private static final String SQL_CHECK_EMAIL
            = "SELECT users.`id` FROM `users` WHERE email = ?";
    private static final String SQL_CREATE_USER
            = "INSERT INTO users (`name`, `surname`, `email`, "
                + "`password`, `role`) VALUES (?, ?, ?, ?, ?)";
    private static final String QUERY_CREATE_USER_ACCOUNT
            = "INSERT INTO users_info (`id`) VALUES (?)";
    private static final String QUERY_FIND_BY_COUNTRY
            = "SELECT `id` FROM users WHERE users.id=( "
            + "SELECT owner_id FROM places WHERE country_id=?)";
    private static final String SQL_READ_USERS
            = "SELECT users.`id`, `name`, `surname`, `email`, `role`, "
                + "`phone`, is_host, `rating`, `profile`, `is_active`, `path`, "
            + "`country`, `city` "
            + "FROM users LEFT JOIN users_info ON users.id = users_info.id "
            + "LEFT JOIN `avatars` ON users.id = avatars.user_id LEFT JOIN `countries_catalog` "
            + "ON users_info.country_id = countries_catalog.id LEFT JOIN `cities_catalog` c2 "
            + "ON users_info.city_id = c2.id ORDER BY `rating` DESC";

    private static final String SQL_READ_USERS_LIMIT
            = "SELECT users.`id`, `name`, `surname`, `email`, `role`, "
                + "`phone`, is_host, `rating`, `profile`, `is_active`, `path`, "
                + "`country`, `city` "
                + "FROM users LEFT JOIN users_info ON users.id = users_info.id "
                + "LEFT JOIN `avatars` ON users.id = avatars.user_id LEFT JOIN `countries_catalog` "
                + " ON users_info.country_id = countries_catalog.id LEFT JOIN `cities_catalog` c2 "
                + "ON users_info.city_id = c2.id ORDER BY `rating` DESC  "
            + "LIMIT ?,?";
    private static final String SQL_READ_USER_BY_ID
            = "SELECT `name`, `surname`, `email`, `role`, `path`, "
            + "`phone`, is_host, `rating`, `profile`, `is_active`, `country`, `city` "
            + "FROM users LEFT JOIN users_info ON users.id = users_info.id LEFT JOIN `avatars` ON users.id = avatars.user_id "
            + "LEFT JOIN `countries_catalog` "
            + "ON users_info.country_id = countries_catalog.id LEFT JOIN `cities_catalog` c2 "
            + "ON users_info.city_id = c2.id "
            + "WHERE users.`id`=?";

    private static final String SQL_READ_RATING
            = "SELECT `rating` FROM `reviews` " +
            "WHERE `user_id` = ?";
    private static final String SQL_LOCK
            = "UPDATE `users_info` SET `is_active`= 0 WHERE `id`=?";
    private static final String SQL_UNLOCK
            = "UPDATE `users_info` SET `is_active`= 1 WHERE `id`=?";


    private static final String SQL_READ_USERS_BY_COUNTRY_HOST
            = "SELECT users.`id`, `name`, `surname`, `email`, `role`, `path`, "
            + "`phone`, `rating`, `profile`, `is_active`, `country`, `city`, `is_host`"
            + "FROM users LEFT JOIN users_info ON users.id = users_info.id "
            + "LEFT JOIN `avatars` ON users.id = avatars.user_id LEFT JOIN `countries_catalog` "
            + "ON users_info.country_id = countries_catalog.id LEFT JOIN `cities_catalog` c2 "
            + "ON users_info.city_id = c2.id "
            + "WHERE `country`=? AND is_host=?";


    @Override
    public User authentication(String email, String password) throws DAOException {
        User user = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = getPool().takeConnection();
            preparedStatement
                    = connection.prepareStatement(SQL_FIND_USER);
            preparedStatement.setString(1, email);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()
                    && BCrypt.checkpw(password,
                    resultSet.getString("password"))) {
                user = createUser(resultSet, email);
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            getPool().finallyClose(
                    connection, preparedStatement, resultSet);
        }
        return user;
    }

    @Override
    public boolean registration(final String name, final String surname,
                               final String email, final String password)
            throws DAOException {
        if (isUsed(email)) {
            return false;
        }
        int resultAccount;
        Connection con = null;
        PreparedStatement userPst = null;
        PreparedStatement accountPst = null;
        ResultSet resultSet = null;
        try {
            con = getPool().takeConnection();
            con.setAutoCommit(false);

            userPst = con.prepareStatement(SQL_CREATE_USER, Statement.RETURN_GENERATED_KEYS);
            userPst.setString(1, name);
            userPst.setString(2, surname);
            userPst.setString(3, email);
            userPst.setString(4,
                    BCrypt.hashpw(password, BCrypt.gensalt()));
            userPst.setInt(5, 2);
            userPst.executeUpdate();
            resultSet = userPst.getGeneratedKeys();
            if(resultSet.next()) {
                accountPst = con.prepareStatement(QUERY_CREATE_USER_ACCOUNT);
                accountPst.setInt(1, resultSet.getInt(1));
                resultAccount = accountPst.executeUpdate();
                if(resultAccount == 1) {
                    con.commit();
                    LOGGER.info("Account is created");
                    return true;
                } else {
                    con.rollback();
                    LOGGER.warn("Can not create users account");
                    return false;
                }
            } else {
                LOGGER.error(
                        "There is no autoincremented index after "
                                + "trying to add record into table `users`");
                con.rollback();
                throw new DAOException("Registration is failed");
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            getPool().finallyClose(con, userPst, resultSet);
            getPool().finallyClose(accountPst);
        }
    }

    @Override
    public List<User> readAll() throws DAOException {
        Connection con = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            con = getPool().takeConnection();
            statement = con.prepareStatement(SQL_READ_USERS);
            resultSet = statement.executeQuery();
            List<User> users = new ArrayList<>();
            return fillUsers(resultSet, users);
        } catch(SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            getPool().finallyClose(con, statement, resultSet);
        }
    }

    @Override
    public EntitiesOnPage<User> readFromStartPosition(
            int startPosition, int usersNumber) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        try {
            con = getPool().takeConnection();
            ps = con.prepareStatement(SQL_READ_USERS_LIMIT);
            ps.setInt(1, startPosition);
            ps.setInt(2, usersNumber);
            resultSet = ps.executeQuery();
            List<User> users = new ArrayList<>();
            users = fillUsers(resultSet, users);
            EntitiesOnPage<User> entities = new EntitiesOnPage<>();
            entities.setEntitiesNumber(users.size());
            entities.setSearchResult(users);
            return entities;
        } catch(SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            getPool().finallyClose(con, ps, resultSet);
        }
    }

    @Override
    public List<User> filterUsers(final String country,
                                  final boolean isHost) throws DAOException {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Connection con = null;
        try {
            con = getPool().takeConnection();
            statement = con.prepareStatement(SQL_READ_USERS_BY_COUNTRY_HOST);
            statement.setString(1, country);
            statement.setBoolean(2, isHost);
            resultSet = statement.executeQuery();
            List<User> users = new ArrayList<>();
            while(resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setEmail(resultSet.getString("email"));
                user.setRole(UserRole.getById(
                        resultSet.getInt("role")));
                user.setName(resultSet.getString("name"));
                user.setSurname(resultSet.getString("surname"));
                user.setPhone(resultSet.getString("phone"));
                user.setHost(isHost);
                int rating = calcRating(resultSet.getInt("id"));
                user.setRating(rating);
                user.setProfile(resultSet.getString("profile"));
                user.setActive(resultSet.getBoolean("is_active"));
                user.setAvatarPath(resultSet.getString("path"));
                user.setCountry(country);
                user.setCity(resultSet.getString("city"));
                users.add(user);
            }
            return users;
        } catch(SQLException | ConnectionPoolException e) {
            throw new DAOException(
                    DAOException.SQL_EXCEPTION_MESSAGE + e);
        }  finally {
            getPool().finallyClose(con, statement, resultSet);
        }
    }


    @Override
    public boolean lock(int id) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        int result;
        try {
            con = getPool().takeConnection();
            ps = con.prepareStatement(SQL_LOCK);
            ps.setInt(1, id);
            result = ps.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
           throw new DAOException("Can not lock the user", e);
        } finally {
            getPool().finallyClose(con, ps);
        }
        return result == 1;
    }

    @Override
    public boolean unlock(int id) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        int result;
        try {
            con = getPool().takeConnection();
            ps = con.prepareStatement(SQL_UNLOCK);
            ps.setInt(1, id);
            result = ps.executeUpdate();
        } catch (ConnectionPoolException | SQLException e) {
            throw new DAOException("Can not unlock the user", e);
        } finally {
            getPool().finallyClose(con, ps);
        }
        return result == 1;
    }



    /**
      * The method fills the list with users and their
      * fields using data from the DB.
      * @param resultSet the result of the query execution.
      * @param users the list of users found by the given parameter.
      * @return the list of users found by the given parameter
      * @throws SQLException when the request on table is failed
      */
    private List<User> fillUsers(final ResultSet resultSet,
                                 final List<User> users) throws SQLException, DAOException {
        User user;
        while(resultSet.next()) {
            user = createUser(resultSet);
            users.add(user);
        }
        return users;
    }

    @Override
    public boolean create(User entity) throws DAOException {
        return false;
    }

    @Override
    public User read(final int id) throws DAOException {
        User user = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = getPool().takeConnection();
            preparedStatement
                    = connection.prepareStatement(SQL_READ_USER_BY_ID);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = createUser(resultSet, id);
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            getPool().finallyClose(
                    connection, preparedStatement, resultSet);
        }
        return user;
    }

    @Override
    public boolean update(User user) throws DAOException {
return false;
    }

    @Override
    public boolean delete(int identity) throws DAOException {
        return false;
    }

    @Override
    public int calcRating(final int userId) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int rating = 0;
        try {
            con = getPool().takeConnection();
            ps = con.prepareStatement(SQL_READ_RATING);
            ps.setInt(1, userId);
            rs = ps.executeQuery();
            while (rs.next()) {
                int grade = rs.getInt("rating");
                rating += grade;
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            getPool().finallyClose(con, ps, rs);
        }
        return rating;
    }

    private User createUser(final ResultSet resultSet, final String email)
            throws SQLException, DAOException {
        User user = new User();
        user.setId(resultSet.getInt("id"));
        user.setEmail(email);
        user.setRole(UserRole.getById(
                resultSet.getInt("role")));
        user.setName(resultSet.getString("name"));
        user.setSurname(resultSet.getString("surname"));
        user.setPhone(resultSet.getString("phone"));
        user.setHost(
                resultSet.getBoolean("is_host"));
        int rating = calcRating(resultSet.getInt("id"));
        user.setRating(rating);
        user.setProfile(resultSet.getString("profile"));
        user.setAvatarPath(resultSet.getString("path"));
        user.setActive(resultSet.getBoolean("is_active"));
        user.setCountry(resultSet.getString("country"));
        user.setCity(resultSet.getString("city"));
        return user;
    }
    private User createUser(final ResultSet resultSet, final int id)
            throws SQLException, DAOException {
        User user = new User();
        user.setId(id);
        user.setEmail(resultSet.getString("email"));
        user.setRole(UserRole.getById(
                resultSet.getInt("role")));
        user.setName(resultSet.getString("name"));
        user.setSurname(resultSet.getString("surname"));
        user.setPhone(resultSet.getString("phone"));
        user.setHost(
                resultSet.getBoolean("is_host"));
        int rating = calcRating(id);
        user.setRating(rating);
        user.setProfile(resultSet.getString("profile"));
        user.setActive(resultSet.getBoolean("is_active"));
        user.setAvatarPath(resultSet.getString("path"));
        user.setCountry(resultSet.getString("country"));
        user.setCity(resultSet.getString("city"));
        return user;
    }

    private User createUser(final ResultSet resultSet)
            throws SQLException, DAOException {
        User user = new User();
        user.setId(resultSet.getInt("id"));
        user.setEmail(resultSet.getString("email"));
        user.setRole(UserRole.getById(
                resultSet.getInt("role")));
        user.setName(resultSet.getString("name"));
        user.setSurname(resultSet.getString("surname"));
        user.setPhone(resultSet.getString("phone"));
        user.setHost(
                resultSet.getBoolean("is_host"));
        int rating = calcRating(resultSet.getInt("id"));
        user.setRating(rating);
        user.setProfile(resultSet.getString("profile"));
        if(resultSet.getString("path") != null) {
            user.setAvatarPath(resultSet.getString("path"));
        }
        user.setActive(resultSet.getBoolean("is_active"));
        user.setCountry(resultSet.getString("country"));
        user.setCity(resultSet.getString("city"));
        return user;
    }

    private boolean isUsed(final String email)
            throws DAOException {
        Connection con = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            con = getPool().takeConnection();
            statement = con.prepareStatement(SQL_CHECK_EMAIL);
            statement.setString(1, email);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return true;
            }
        } catch (SQLException | ConnectionPoolException ex) {
            throw new DAOException(ex);
        } finally {
            getPool().finallyClose(con, statement, resultSet);
        }
        return false;
    }
}

