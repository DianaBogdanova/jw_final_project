package by.training.hospitality_network.dao;

import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.entity.Image;

import java.io.InputStream;

public interface GalleryDAO extends CommonDAO<Image> {
    boolean create(final InputStream fileContent, final int id) throws DAOException;
}
