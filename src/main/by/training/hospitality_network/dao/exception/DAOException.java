package by.training.hospitality_network.dao.exception;

public class DAOException extends Exception {
    public static final String SQL_EXCEPTION_MESSAGE
            = "SQL exception (request or table failed): ";
    public static final String STATEMENT_CLOSING_ERROR
            = "Statement wasn't closed";
    public static final String RESILTSET_CLOSING_ERROR
            = "ResultSet wasn't closed";
    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }

    public DAOException(String message) {
        super(message);
    }

    public DAOException(Throwable cause) {
        super(cause);
    }
}
