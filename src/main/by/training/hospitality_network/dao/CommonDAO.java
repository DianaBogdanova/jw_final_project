package by.training.hospitality_network.dao;

import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.entity.Entity;

public interface CommonDAO <T extends Entity> {
    boolean create(T entity) throws DAOException;
    T read(int identity) throws DAOException;
    boolean update(T entity) throws DAOException;
    boolean delete(int identity) throws DAOException;
}
