package by.training.hospitality_network.dao;

import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.dao.util.EntitiesOnPage;
import by.training.hospitality_network.entity.Review;

import java.util.List;

public interface ReviewDAO extends CommonDAO<Review> {
    List<Review> readUsersReviews(final int userId) throws DAOException;
    EntitiesOnPage<Review> readFromStartPosition(
            int startPosition, int reviewsNumber, int userId) throws DAOException;
}
