package by.training.hospitality_network.dao.util;


public abstract class SQLQueryStorage {

    /**User*/

    private static final String QUERY_FIND_USER
            = "SELECT users.`id`, `surname`, `name`, `password`, "
            + "`role`, `phone`, is_host, `rating`, `profile` "
            + "FROM users LEFT JOIN users_info ON users.id = users_info.id "
            + "WHERE email = ?";
    private static final String QUERY_CHECK_EMAIL
            = "SELECT users.`id` FROM `users` WHERE email = ?";
    private static final String QUERY_INSERT_USER
            = "INSERT INTO users (`name`, `surname`, `email`, "
            + "`password`, `role`) VALUES (?, ?, ?, ?, ?)";
    private static final String QUERY_CREATE_EMPTY_USER_ACCOUNT
            = "INSERT INTO users_info (`id`) VALUES (?)";
    private static final String QUERY_FIND_BY_COUNTRY
            = "SELECT `id` FROM users WHERE users.id=( "
            + "SELECT owner_id FROM places WHERE country_id=?)";
    private static final String QUERY_READ_ALL_USERS
            = "SELECT users.`id`, `name`, `surname`, `email`, `role`, "
            + "`phone`, is_host, `rating`, `profile`"
            + "FROM users LEFT JOIN users_info ON users.id = users_info.id ORDER BY `rating` DESC";



    /**User account data*/

    private static final String QUERY_SELECT_COUNTRIES
            = "SELECT `country` FROM `countries_catalog`";
    private static final String QUERY_SELECT_CITIES
            = "SELECT `city` FROM `cities_catalog`";
    private static final String QUERY_EDIT_ACCOUNT
            = "UPDATE users_info SET `phone`= ?, "
            + "`profile` = ?, `is_host` = ? WHERE `id` = ?";

    /**Queries*/
    private static final String SQL_CREATE_QUERY
            = "INSERT INTO `queries`("
            + "`surfer_id`, `host_id`, `place_id`, "
            + "`date_from`, `date_to`)"
            +" VALUES (?, ?, ?, ?, ?)";
    private static final String SQL_READ_HOSTS_QUERIES
            = "SELECT `surfer_id`, `place_id`, `date_from`, `date_to` "
            + "FROM `queries` WHERE `host_id` = ? AND `condition` = 2";
    private static final String SQL_READ_SURFERS_QUERIES
            = "SELECT `host_id`, `place_id`, `date_from`, `date_to`, `condition` "
            + "FROM `queries` WHERE `surfer_id` = ?";


    /**Reviews*/

    private static final String QUERY_READ_REVIEWS
            = "SELECT `author_id`, `text` FROM `reviews` WHERE `user_id` = ?";
}
