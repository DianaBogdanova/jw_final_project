package by.training.hospitality_network.dao.util;

import java.util.ResourceBundle;

import static by.training.hospitality_network.dao.util.DBParameter.BUNDLE_NAME;

public final class DBResourceManager {
    private static final DBResourceManager INSTANCE = new DBResourceManager();
    private ResourceBundle bundle = ResourceBundle.getBundle(BUNDLE_NAME);

    private DBResourceManager() {
    }

    public static DBResourceManager getInstance() {
        return INSTANCE;
    }

    public String getValue(String resourceKey) {
        return bundle.getString(resourceKey);
    }
}
