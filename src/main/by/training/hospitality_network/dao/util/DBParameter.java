package by.training.hospitality_network.dao.util;

public final class DBParameter {
    public static final String BUNDLE_NAME = "resources/database";
    public static final String DB_DRIVER = "databaseDriver";
    public static final String DB_URL = "databaseURL";
    public static final String DB_USER = "databaseLogin";
    public static final String DB_PASSWORD = "databasePassword";
    public static final String DB_POOL_SIZE = "databasePoolSize";
}

