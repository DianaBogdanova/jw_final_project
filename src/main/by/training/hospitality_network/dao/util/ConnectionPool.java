package by.training.hospitality_network.dao.util;

import by.training.hospitality_network.dao.exception.ConnectionPoolException;
import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.dao.exception.NotDBDriverException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;

import static by.training.hospitality_network.dao.util.DBParameter.*;

public final class ConnectionPool {
    private static final ConnectionPool INSTANCE = new ConnectionPool();
    private static final Logger LOGGER = LogManager.getLogger(ConnectionPool.class);
    private BlockingQueue<Connection> freeConnections;
    private BlockingQueue<Connection> usedConnections;

    private String url;
    private String driverName;
    private String user;
    private String password;
    private int poolSize;

    private ConnectionPool() {
        DBResourceManager dbResourceManager = DBResourceManager.getInstance();
        driverName = dbResourceManager.getValue(DB_DRIVER);
        url = dbResourceManager.getValue(DB_URL);
        user = dbResourceManager.getValue(DB_USER);
        password = dbResourceManager.getValue(DB_PASSWORD);
        try {
            poolSize = Integer.parseInt(dbResourceManager.getValue(DB_POOL_SIZE));
        } catch (NumberFormatException ex) {
            poolSize = 5;
        }
        try {
            init();
        } catch (ConnectionPoolException e) {
            LOGGER.error("Pool data initialisation is failed", e);
        }
    }

    public static ConnectionPool getInstance() {
        return INSTANCE;
    }

    private void init() throws ConnectionPoolException {
        try {
            Class.forName(driverName);
            usedConnections = new ArrayBlockingQueue<>(poolSize);
            freeConnections = new ArrayBlockingQueue<>(poolSize);
            for (int i = 0; i < poolSize; i++) {
                Connection connection = DriverManager.getConnection(url, user, password);
                PooledConnection pooledConnection = new PooledConnection(connection);
                freeConnections.add(pooledConnection);
            }
        } catch (ClassNotFoundException e) {
            LOGGER.error("Driver wasn't found", e);
            throw new NotDBDriverException(
                    "Can't find a driver", e);
        } catch (SQLException ex) {
            throw new ConnectionPoolException(
                    "SQLException in connection pool during it's initialization", ex);
        }
    }


    public void dispose() {
        clearConnectionQueue();
    }

    private void clearConnectionQueue() {
        try {
            closeConnectionsQueue(usedConnections);
            closeConnectionsQueue(freeConnections);
        } catch (SQLException e) {
            LOGGER.error("Error closing the connection", e);
        }
    }

    /**
     * The method closes all the connections from
     * the passed queue.
     * @param queue the queue to close all the
     *              connections in.
     * @throws SQLException when connection
     * can not be closed.
     */
    private void closeConnectionsQueue(
            final BlockingQueue<Connection> queue)
            throws SQLException {
        Connection connection;
        while ((connection = queue.poll()) != null) {
            if (!connection.getAutoCommit()) {
                connection.commit();
            }
            ((PooledConnection) connection).reallyClose();
        }
    }
    public Connection takeConnection() throws ConnectionPoolException {
        Connection connection;
        try {
            connection = freeConnections.take();
            usedConnections.add(connection);
        } catch (InterruptedException ex) {
            LOGGER.error("Interrupt!", ex);
            Thread.currentThread().interrupt();
            throw new ConnectionPoolException(
                    "Error connecting to the data source", ex);
        }
        return connection;
    }

    public void finallyClose(final Connection con, final Statement st) {
        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                LOGGER.error("Can`t close statement", e);
            }
        }

        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                LOGGER.error("Can`t close connection", e);
            }
        }
    }
    public void finallyClose(final Statement st) {
        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                LOGGER.error("Can`t close statement", e);
            }
        }
    }
    public void finallyClose(final Connection con, final Statement st, final ResultSet rs)
            throws DAOException {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                LOGGER.error("Can't close resultSet", e);
            }
        }
        finallyClose(con, st);
    }
    public void finallyClose(final Statement st, final ResultSet rs)
            throws DAOException {
        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                LOGGER.error("Can't close statement", e);
            }
        }
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                LOGGER.error("Can't close resultSet", e);
            }
        }
    }


    protected class PooledConnection implements Connection {
        /**
         * The {@see Connection} object is used not to implement
         * all the methods of the {@see Connection} interface.
         */
        private Connection connection;

        public PooledConnection(Connection connection) throws SQLException {
            this.connection = connection;
            this.connection.setAutoCommit(true);
        }

        /**
         * The method closes the connection.
         * @throws SQLException when the connection
         * can not be closed.
         */
        public void reallyClose() throws SQLException {
            connection.close();
        }

        /**
         * The method clears warnings delegating
         * it to the {@see connection} object.
         * @throws SQLException when warnings
         * can not be cleared.
         */
        @Override
        public void clearWarnings() throws SQLException {
            connection.clearWarnings();
        }

        /**
         * The method prevents the attempt to close the connection twice,
         * returns the changed connection settings to the initial state,
         * removes the connection from the queue of occupied connections
         * and adds it to the queue of available connections.
         * The expected result - the connection is moved to the
         * queue of available connections
         * @throws SQLException if some of this actions can't be completed
         */
        @Override
        public void close() throws SQLException {
            if (connection.isClosed()) {
                throw new SQLException(
                        "Attempting to close closed connection");
            }
            if (connection.isReadOnly()) {
                connection.setReadOnly(false);
            }
            if (!usedConnections.remove(this)) {
                throw new SQLException(
                        "Error deleting connection from the given away connections pool");
            }
            if (!freeConnections.offer(this)) {
                throw new SQLException(
                        "Error allocating connection in the pool");
            }
        }

        @Override
        public Statement createStatement() throws SQLException {
            return connection.createStatement();
        }

        @Override
        public PreparedStatement prepareStatement(String sql) throws SQLException {
            return connection.prepareStatement(sql);
        }

        @Override
        public CallableStatement prepareCall(String sql) throws SQLException {
            return connection.prepareCall(sql);
        }

        @Override
        public String nativeSQL(String sql) throws SQLException {
            return connection.nativeSQL(sql);
        }

        @Override
        public void setAutoCommit(boolean autoCommit) throws SQLException {
            connection.setAutoCommit(autoCommit);
        }

        @Override
        public boolean getAutoCommit() throws SQLException {
            return connection.getAutoCommit();
        }

        @Override
        public void commit() throws SQLException {
            connection.commit();
        }

        @Override
        public void rollback() throws SQLException {
            connection.rollback();
        }

        @Override
        public boolean isClosed() throws SQLException {
            return connection.isClosed();
        }

        @Override
        public DatabaseMetaData getMetaData() throws SQLException {
            return connection.getMetaData();
        }

        @Override
        public void setReadOnly(boolean readOnly) throws SQLException {
            connection.setReadOnly(readOnly);
        }

        @Override
        public boolean isReadOnly() throws SQLException {
            return connection.isReadOnly();
        }

        @Override
        public void setCatalog(String catalog) throws SQLException {
            connection.setCatalog(catalog);
        }

        @Override
        public String getCatalog() throws SQLException {
            return connection.getCatalog();
        }

        @Override
        public void setTransactionIsolation(int level) throws SQLException {
            connection.setTransactionIsolation(level);
        }

        @Override
        public int getTransactionIsolation() throws SQLException {
            return connection.getTransactionIsolation();
        }

        @Override
        public SQLWarning getWarnings() throws SQLException {
            return connection.getWarnings();
        }

        @Override
        public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
            return connection.createStatement(resultSetType, resultSetConcurrency);
        }

        @Override
        public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency)
                throws SQLException {
            return connection.prepareStatement(sql, resultSetType, resultSetConcurrency);
        }

        @Override
        public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency)
                throws SQLException {
            return connection.prepareCall(sql, resultSetType, resultSetConcurrency);
        }

        @Override
        public Map<String, Class<?>> getTypeMap() throws SQLException {
            return connection.getTypeMap();
        }


        @Override
        public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
            connection.setTypeMap(map);
        }

        @Override
        public void setHoldability(int holdability) throws SQLException {
            connection.setHoldability(holdability);
        }

        @Override
        public int getHoldability() throws SQLException {
            return connection.getHoldability();
        }

        @Override
        public Savepoint setSavepoint() throws SQLException {
            return connection.setSavepoint();
        }

        @Override
        public Savepoint setSavepoint(String name) throws SQLException {
            return connection.setSavepoint(name);
        }

        @Override
        public void rollback(Savepoint savepoint) throws SQLException {
            connection.rollback(savepoint);
        }

        @Override
        public void releaseSavepoint(Savepoint savepoint) throws SQLException {
            connection.releaseSavepoint(savepoint);
        }

        @Override
        public Statement createStatement(final int resultSetType,
                                         final int resultSetConcurrency,
                                         final int resultSetHoldability)
                throws SQLException {
            return connection.createStatement(
                    resultSetType, resultSetConcurrency, resultSetHoldability);
        }

        @Override
        public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency,
                                                  int resultSetHoldability) throws SQLException {
            return connection.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
        }

        @Override
        public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency,
                                             int resultSetHoldability) throws SQLException {
            return connection.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
        }

        @Override
        public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
            return connection.prepareStatement(sql, autoGeneratedKeys);
        }

        @Override
        public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
            return connection.prepareStatement(sql, columnIndexes);
        }

        @Override
        public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
            return connection.prepareStatement(sql, columnNames);
        }

        @Override
        public Clob createClob() throws SQLException {
            return connection.createClob();
        }

        @Override
        public Blob createBlob() throws SQLException {
            return connection.createBlob();
        }

        @Override
        public NClob createNClob() throws SQLException {
            return connection.createNClob();
        }

        @Override
        public SQLXML createSQLXML() throws SQLException {
            return connection.createSQLXML();
        }

        @Override
        public boolean isValid(int timeout) throws SQLException {
            return connection.isValid(timeout);
        }

        @Override
        public void setClientInfo(String name, String value) throws SQLClientInfoException {
            connection.setClientInfo(name, value);
        }


        @Override
        public void setClientInfo(Properties properties) throws SQLClientInfoException {
            connection.setClientInfo(properties);
        }

        @Override
        public String getClientInfo(String name) throws SQLException {
            return connection.getClientInfo(name);
        }

        @Override
        public Properties getClientInfo() throws SQLException {
            return connection.getClientInfo();
        }

        @Override
        public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
            return connection.createArrayOf(typeName, elements);
        }

        @Override
        public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
            return connection.createStruct(typeName, attributes);
        }

        @Override
        public void setSchema(String schema) throws SQLException {
            connection.setSchema(schema);
        }

        @Override
        public String getSchema() throws SQLException {
            return connection.getSchema();
        }

        @Override
        public void abort(Executor executor) throws SQLException {
            connection.abort(executor);
        }

        @Override
        public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
            connection.setNetworkTimeout(executor, milliseconds);
        }

        @Override
        public int getNetworkTimeout() throws SQLException {
            return connection.getNetworkTimeout();
        }

        @Override
        public <T> T unwrap(Class<T> iface) throws SQLException {
            return connection.unwrap(iface);
        }

        @Override
        public boolean isWrapperFor(Class<?> iface) throws SQLException {
            return connection.isWrapperFor(iface);
        }
    }
}