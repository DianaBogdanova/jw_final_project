package by.training.hospitality_network.dao.util;

import by.training.hospitality_network.entity.Entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class EntitiesOnPage<T extends Entity> implements Serializable {
    private static final long serialVersionUID = -6917289708468034257L;

    private int entitiesNumber;
    private List<T> searchResult = new ArrayList<>();

    public int getEntitiesNumber() {
        return entitiesNumber;
    }

    public void setEntitiesNumber(int entitiesNumber) {
        this.entitiesNumber = entitiesNumber;
    }

    public List<T> getSearchResult() {
        return searchResult;
    }

    public void setSearchResult(List<T> entities) {
        searchResult = entities;
    }


}
