package test.by.training.hospitality_network.service;

import by.training.hospitality_network.dao.exception.DAOException;
import by.training.hospitality_network.entity.User;
import by.training.hospitality_network.entity.UserRole;
import by.training.hospitality_network.service.UserService;
import by.training.hospitality_network.service.exception.ServiceException;
import by.training.hospitality_network.service.impl.UserServiceImpl;
import org.testng.annotations.*;

import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;


public class UserServiceImplTest {
    private List<User> expectedUsers;
    private UserService userService = new UserServiceImpl();
    private User user1;
    private User user2;
    private User user3;
    private User user4;
    private User user5;
    private User user6;
    private User user7;
    private User user8;
    private User user10;
    private User user11;
    private User user12;
    private User user13;
    private User user14;
    private User user15;
    private User user16;
    private User user17;
    private User user18;

    @BeforeClass
    public void setUp() throws ServiceException {
        user1 = new User();
        user1.setId(1);
        user1.setEmail("admin@mail.ru");
        user1.setRole(UserRole.ADMINISTRATOR);
        user1.setName("Ivan");
        user1.setSurname("Ivanov");
        user1.setPhone(null);
        user1.setHost(false);
        user1.setRating(14);
        user1.setProfile(null);
        user1.setAvatarPath("");
        user1.setActive(true);
        user1.setCountry(null);
        user1.setCity(null);

        user2 = new User();
        user2.setId(2);
        user2.setEmail("polino89@mail.ru");
        user2.setRole(UserRole.REGISTERED_USER);
        user2.setName("Max");
        user2.setSurname("Polino");
        user2.setPhone("+79668182134");
        user2.setHost(false);
        user2.setRating(9);
        user2.setProfile("I like travelling.");
        user2.setAvatarPath("../../img/avatar/userm1.png");
        user2.setActive(true);
        user2.setCountry("Russia");
        user2.setCity("Moscow");

        user3 = new User();
        user3.setId(3);
        user3.setEmail("anechka@mail.ru");
        user3.setRole(UserRole.REGISTERED_USER);
        user3.setName("Anna");
        user3.setSurname("Fedorova");
        user3.setPhone("+375294444444");
        user3.setHost(true);
        user3.setRating(10);
        user3.setProfile("I like to receive quests");
        user3.setAvatarPath("../../img/avatar/userw1.jpg");
        user3.setActive(true);
        user3.setCountry("Belarus");
        user3.setCity("Minsk");

        user4 = new User();
        user4.setId(4);
        user4.setEmail("grinevich@gmail.ru");
        user4.setRole(UserRole.REGISTERED_USER);
        user4.setName("Olga");
        user4.setSurname("Grinevich");
        user4.setPhone("+79668002134");
        user4.setHost(false);
        user4.setRating(10);
        user4.setProfile("I like sport.");
        user4.setAvatarPath("../../img/avatar/userw2.jpg");
        user4.setActive(true);
        user4.setCountry("Russia");
        user4.setCity("Moscow");

        user5 = new User();
        user5.setId(5);
        user5.setEmail("posa@mail.ru");
        user5.setRole(UserRole.REGISTERED_USER);
        user5.setName("Chopra");
        user5.setSurname("Prienco");
        user5.setPhone("+375291244444");
        user5.setHost(true);
        user5.setRating(10);
        user5.setProfile("I am pleased to support communication after the trip.");
        user5.setAvatarPath("../../img/avatar/userw3.jpg");
        user5.setActive(true);
        user5.setCountry("Belarus");
        user5.setCity("Minsk");

        user6 = new User();
        user6.setId(6);
        user6.setEmail("abramson@gmail.com");
        user6.setRole(UserRole.REGISTERED_USER);
        user6.setName("Adrian");
        user6.setSurname("Abramson");
        user6.setPhone("+375294234544");
        user6.setHost(true);
        user6.setRating(10);
        user6.setProfile("I like reading books");
        user6.setAvatarPath("../../img/avatar/userm2.jpg");
        user6.setActive(true);
        user6.setCountry("Belarus");
        user6.setCity("Minsk");

        user7 = new User();
        user7.setId(7);
        user7.setEmail("alexa@gmail.com");
        user7.setRole(UserRole.REGISTERED_USER);
        user7.setName("Alexa");
        user7.setSurname("Adderiy");
        user7.setPhone("+375298124223");
        user7.setHost(true);
        user7.setRating(10);
        user7.setProfile("I can provide you with one floor in my big house.");
        user7.setAvatarPath("../../img/avatar/userw4.jpeg");
        user7.setActive(true);
        user7.setCountry("Belarus");
        user7.setCity("Minsk");


        user8 = new User();
        user8.setId(8);
        user8.setEmail("AlexandraHodges@gmail.com");
        user8.setRole(UserRole.REGISTERED_USER);
        user8.setName("Alexandra");
        user8.setSurname("Hodges");
        user8.setPhone("+375336144444");
        user8.setHost(false);
        user8.setRating(10);
        user8.setProfile("I am a vegetarian");
        user8.setAvatarPath("../../img/avatar/userw5.jpg");
        user8.setActive(true);
        user8.setCountry("Belarus");
        user8.setCity("Minsk");

        user10 = new User();
        user10.setId(9);
        user10.setEmail("whiteWhite@mail.ru");
        user10.setRole(UserRole.REGISTERED_USER);
        user10.setName("Andrew");
        user10.setSurname("White");
        user10.setPhone("+375292164454");
        user10.setHost(false);
        user10.setRating(-10);
        user10.setProfile("I play the piano at night. If you like the sounds of the piano at night - welcome");
        user10.setAvatarPath("../../img/avatar/userm3.jpg");
        user10.setActive(true);
        user10.setCountry("Belarus");
        user10.setCity("Minsk");

        user11 = new User();
        user11.setId(10);
        user11.setEmail("angelangel@gmail.com");
        user11.setRole(UserRole.REGISTERED_USER);
        user11.setName("Angel");
        user11.setSurname("Wayne");
        user11.setPhone("+375295121744");
        user11.setHost(false);
        user11.setRating(-1);
        user11.setProfile("I am a sports, erudite person without bad habits");
        user11.setAvatarPath("../../img/avatar/userw6.jpg");
        user11.setActive(true);
        user11.setCountry("Belarus");
        user11.setCity("Minsk");

        user12 = new User();
        user12.setId(11);
        user12.setEmail("anita@mail.ru");
        user12.setRole(UserRole.REGISTERED_USER);
        user12.setName("Anita");
        user12.setSurname("Walter");
        user12.setPhone("+79668180034");
        user12.setHost(false);
        user12.setRating(9);
        user12.setProfile("I can teach you to dance tango and play Chinese chess");
        user12.setAvatarPath("../../img/avatar/userw7.jpg");
        user12.setActive(true);
        user12.setCountry("Russia");
        user12.setCity("Moscow");

        user13 = new User();
        user13.setId(12);
        user13.setEmail("ava@gmail.com");
        user13.setRole(UserRole.REGISTERED_USER);
        user13.setName("Ava");
        user13.setSurname("Thorndike");
        user13.setPhone("+79618182123");
        user13.setHost(true);
        user13.setRating(10);
        user13.setProfile("I will be glad to invite you to my house if you are not going to steal my clothes.");
        user13.setAvatarPath("../../img/avatar/userm4.jpg");
        user13.setActive(true);
        user13.setCountry("Russia");
        user13.setCity("Moscow");

        user14 = new User();
        user14.setId(13);
        user14.setEmail("Shackley@mail.ru");
        user14.setRole(UserRole.REGISTERED_USER);
        user14.setName("Benjamin");
        user14.setSurname("Shackley");
        user14.setPhone("+79668128934");
        user14.setHost(false);
        user14.setRating(10);
        user14.setProfile("I love to eat and cook. I can feed your family");
        user14.setAvatarPath("../../img/avatar/userm5.jpg");
        user14.setActive(true);
        user14.setCountry("Russia");
        user14.setCity("Moscow");

        user15 = new User();
        user15.setId(14);
        user15.setEmail("barbara@gmail.com");
        user15.setRole(UserRole.REGISTERED_USER);
        user15.setName("Barbara");
        user15.setSurname("Reynolds");
        user15.setPhone("+79667182131");
        user15.setHost(true);
        user15.setRating(10);
        user15.setProfile("Men please do not disturb me. Women are welcome");
        user15.setAvatarPath("../../img/avatar/userw8.jpg");
        user15.setActive(true);
        user15.setCountry("Russia");
        user15.setCity("Moscow");

        user16 = new User();
        user16.setId(15);
        user16.setEmail("Peacock@mail.ru");
        user16.setRole(UserRole.REGISTERED_USER);
        user16.setName("Bruce");
        user16.setSurname("Peacock");
        user16.setPhone("+79666782139");
        user16.setHost(true);
        user16.setRating(5);
        user16.setProfile("Do you like to receive guests? Then invite me and I will convince you otherwise.");
        user16.setAvatarPath("../../img/avatar/userm6.jpg");
        user16.setActive(true);
        user16.setCountry("Russia");
        user16.setCity("Moscow");

        user17 = new User();
        user17.setId(16);
        user17.setEmail("OtisBryan@gmail.com");
        user17.setRole(UserRole.REGISTERED_USER);
        user17.setName("Bryan");
        user17.setSurname("Otis");
        user17.setPhone("+34694493904");
        user17.setHost(true);
        user17.setRating(15);
        user17.setProfile("I have visited 37 countries");
        user17.setAvatarPath("../../img/avatar/userm7.jpg");
        user17.setActive(true);
        user17.setCountry("Germany");
        user17.setCity("Berlin");

        user18 = new User();
        user18.setId(17);
        user18.setEmail("Nevill@gmail.com");
        user18.setRole(UserRole.REGISTERED_USER);
        user18.setName("Caroline");
        user18.setSurname("Nevill");
        user18.setPhone("+34692493993");
        user18.setHost(false);
        user18.setRating(10);
        user18.setProfile("I love to dance, sing in the shower and tell jokes. I am a vegetarian");
        user18.setAvatarPath("../../img/avatar/userw9.jpg");
        user18.setActive(true);
        user18.setCountry("Germany");
        user18.setCity("Berlin");

        User user19 = new User();
        user19.setId(18);
        user19.setEmail("MillerChristian@mail.ru");
        user19.setRole(UserRole.REGISTERED_USER);
        user19.setName("Christian");
        user19.setSurname("Miller");
        user19.setPhone("+34696493903");
        user19.setHost(true);
        user19.setRating(-5);
        user19.setProfile("I am pleased to invite you to my house. I will not object to children and animals");
        user19.setAvatarPath("../../img/avatar/userm7.jpg");
        user19.setActive(true);
        user19.setCountry("Germany");
        user19.setCity("Berlin");

        User user20 = new User();
        user20.setId(19);
        user20.setEmail("CodyMacey@mail.ru");
        user20.setRole(UserRole.REGISTERED_USER);
        user20.setName("Cody");
        user20.setSurname("Macey");
        user20.setPhone("+34695493901");
        user20.setHost(false);
        user20.setRating(-5);
        user20.setProfile("I just want to spend the night. I do not need to advise anything and introduce me to your dog.");
        user20.setAvatarPath("../../img/avatar/userm8.jpg");
        user20.setActive(true);
        user20.setCountry("Germany");
        user20.setCity("Berlin");

        User user21 = new User();
        user21.setId(20);
        user21.setEmail("Leman@gmail.com");
        user21.setRole(UserRole.REGISTERED_USER);
        user21.setName("Ethan");
        user21.setSurname("Leman");
        user21.setPhone("0033142955001");
        user21.setHost(true);
        user21.setRating(-1);
        user21.setProfile("I like new acquaintances, I can live in any conditions, for me it is only important to meet interesting people.");
        user21.setAvatarPath("../../img/avatar/userm9.jpg");
        user21.setActive(true);
        user21.setCountry("France");
        user21.setCity("Paris");

        User user22 = new User();
        user22.setId(21);
        user22.setEmail("Bobko@gmail.com");
        user22.setRole(UserRole.REGISTERED_USER);
        user22.setName("Anna");
        user22.setSurname("Bobko");
        user22.setPhone("0033642155001");
        user22.setHost(true);
        user22.setRating(10);
        user22.setProfile("I am sociable, cheerful, I like active rest. Traveling with my cat");
        user22.setAvatarPath("../../img/avatar/userw10.jpg");
        user22.setActive(true);
        user22.setCountry("France");
        user22.setCity("Paris");

        User user23 = new User();
        user23.setId(21);
        user23.setEmail("di@mail.ru");
        user23.setRole(UserRole.REGISTERED_USER);
        user23.setName("Diana");
        user23.setSurname("Bogdanova");
        user23.setPhone("+375298387178");
        user23.setHost(true);
        user23.setRating(15);
        user23.setProfile("I like chocolate");
        user23.setAvatarPath("../../img/avatar/userw12.jpg");
        user23.setActive(true);
        user23.setCountry("Belarus");
        user23.setCity("Minsk");

        expectedUsers = Arrays.asList(
                user17, user23, user3, user4, user5, user6, user7, user8, user13,
                user14, user15, user18, user22, user2, user12, user16, user1, user11,
                user21, user19, user20, user10
        );

        userService.lock(5);
    }

    @DataProvider(name = "input_users_authorisation_data")
    public Object[][] createCorrectData() {
        return new Object[][]{
                {"barbara@gmail.com", "12345678Reynolds", user15},
                {"Peacock@mail.ru", "12345678p", user16},
                {"posa@mail.ru", "1111", user5},
                {"abramson@gmail.com", "123abramson", user6},
        };
    }

    @Test(dataProvider = "input_users_authorisation_data")
    public void testAuthorisation(final String email,
                                  final String password,
                                  final User expected) throws ServiceException {
        User actual = userService.authorisation(email, password);
        assertEquals(actual, expected);
    }

    @Test
    public void testRegistration() {
    }

    @Test
    public void testReadAll() throws ServiceException {
        List<User> actual = userService.readAll();
        List<User> expected = expectedUsers;

        assertEquals(actual.size(), expected.size());
        assertTrue(actual.containsAll(expected));
    }

    @Test
    public void testFilterUsers() {
    }

    @Test
    public void testReadFromStartPosition() {
    }

    @Test
    public void testReadById() {
    }

    @Test
    public void testCalcRating() {
    }

    @Test
    public void testLock() throws ServiceException {
        userService.lock(3);
        User user = userService.readById(3);
        assertFalse(user.getIsActive());
    }

    @Test
    public void testUnlock() throws ServiceException {
        userService.unlock(5);
        User user = userService.readById(5);
        assertTrue(user.getIsActive());
    }

    @AfterClass
    public void tearDown() throws ServiceException {
        userService.unlock(3);
    }

}