CREATE DATABASE `hospitality_network` DEFAULT CHARACTER SET utf8;

GRANT SELECT,INSERT,UPDATE,DELETE
ON `hospitality_network`.*
TO hospitality_network_user@localhost
IDENTIFIED BY 'hospitality_network_password';

GRANT SELECT,INSERT,UPDATE,DELETE
ON `hospitality_network`.*
TO hospitality_network_user@'%'
IDENTIFIED BY 'hospitality_network_password';

USE `hospitality_network`;

CREATE TABLE `users` (
  `id` INTEGER AUTO_INCREMENT,
  `surname` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `email` VARCHAR(254) NOT NULL UNIQUE,
  `password` VARCHAR(60) NOT NULL,/**BCrypt psw*/
  /*
   * 0 - администратор (Role.ADMINISTRATOR)
   * 2 - зарегистрированный пользователь (Role.REGISTERED_USER)
   */
  `role` TINYINT NOT NULL DEFAULT 2,
  CONSTRAINT check_role CHECK (`role` IN (0, 2)),
  CONSTRAINT PK_users PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARACTER SET utf8;

CREATE UNIQUE INDEX ind_email ON `users` (
  `email`);

CREATE TABLE `users_info` (
  `id` INTEGER AUTO_INCREMENT,
  `phone` LONG,
  /*
   * 0 - не принимает гостей
   * 1 - принимает гостей
   */
  `is_host` TINYINT DEFAULT 0 CHECK (`is_host` IN (0, 1)),
  `rating` INT DEFAULT 0,
  `profile` TINYTEXT,
  /*
   * 0 - active
   * 1 - blocked
   */
  `is_active` BOOLEAN DEFAULT 1,
  `country_id` INTEGER NOT NULL ,
  `city_id` INTEGER,
  CONSTRAINT PK_users_info PRIMARY KEY (id),
  CONSTRAINT FK_users_info FOREIGN KEY (id)
  REFERENCES users(`id`),
  CONSTRAINT FK_country_id FOREIGN KEY (country_id)
  REFERENCES countries_catalog(`id`),
  CONSTRAINT FK_city_id FOREIGN KEY (city_id)
  REFERENCES cities_catalog(`id`)
) ENGINE=INNODB DEFAULT CHARACTER SET utf8;

CREATE INDEX ind_host ON `users_info` (
  `is_host`);

CREATE TABLE `countries_catalog` (
  `id` INTEGER AUTO_INCREMENT,
  `country` VARCHAR(25) UNIQUE,
  CONSTRAINT PK_countries PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARACTER SET utf8;
CREATE INDEX ind_countries_catalog ON `countries_catalog` (
  `country`);

/**
countries.id  = cities.id (capital)
 */

CREATE TABLE `cities_catalog` (
  `id` INTEGER AUTO_INCREMENT,
  `city` VARCHAR(25) UNIQUE,
  `country_id` INTEGER NOT NULL,
  CONSTRAINT PK_cities PRIMARY KEY (id),
  CONSTRAINT FK_cities_catalog FOREIGN KEY (country_id)
  REFERENCES countries_catalog(`id`)
) ENGINE=INNODB DEFAULT CHARACTER SET utf8;

CREATE INDEX ind_cities_catalog ON `cities_catalog` (
  `city`);

CREATE TABLE `places` (
  `id` INTEGER AUTO_INCREMENT,
  `owner_id` INTEGER NOT NULL ,
  `country_id` INTEGER NOT NULL ,
  `city_id` INTEGER NOT NULL ,
  `beds_number` INTEGER NOT NULL,
  `is_available` TINYINT DEFAULT 0 CHECK (`is_available` IN (0, 1)),
  CONSTRAINT PK_places PRIMARY KEY (id),
  CONSTRAINT FK_owner_id FOREIGN KEY (owner_id)
  REFERENCES users(`id`),
  CONSTRAINT FK_country_id FOREIGN KEY (country_id)
  REFERENCES countries_catalog(`id`),
  CONSTRAINT FK_city_id FOREIGN KEY (city_id)
  REFERENCES cities_catalog(`id`)
) ENGINE=INNODB DEFAULT CHARACTER SET utf8;

CREATE INDEX ind_places ON `places` (
  `country_id`, `city_id`, `is_available`);

CREATE TABLE `avatars` (
  `id` INTEGER AUTO_INCREMENT,
  `user_id` INTEGER NOT NULL,
  `path` VARCHAR(255) NOT NULL,
  CONSTRAINT PK_gallery PRIMARY KEY (id),
  CONSTRAINT FK_gallery_user_id FOREIGN KEY (user_id)
  REFERENCES users(`id`)
) ENGINE=INNODB DEFAULT CHARACTER SET utf8;

CREATE INDEX ind_avatars ON avatars (
  `user_id`);

CREATE TABLE `place_photos` (
  `id` INTEGER AUTO_INCREMENT,
  `place_id` INTEGER NOT NULL,
  `photo` BLOB NOT NULL,
  CONSTRAINT PK_gallery PRIMARY KEY (id),
  CONSTRAINT FK_gallery_place_id FOREIGN KEY (place_id)
  REFERENCES places(`id`)
) ENGINE=INNODB DEFAULT CHARACTER SET utf8;

CREATE INDEX ind_place_photos ON `place_photos` (
  `place_id`);


CREATE TABLE `queries` (
  `id` INTEGER AUTO_INCREMENT,
  `surferName` INTEGER NOT NULL,
  `hostId` INTEGER NOT NULL,
  `dateFrom` DATE NOT NULL,
  `dateTo` DATE NOT NULL,
  `message` TINYTEXT,
  /*
   * 0 - принят (Condition.ACCEPTED)
   * 1 - отклонен (Condition.REJECTED)
   * 2 - на рассмотрении (Condition.PENDING)
   */
  `condition` TINYINT DEFAULT 2 CHECK (`condition` IN (0, 1, 2)),
  CONSTRAINT PK_queries PRIMARY KEY (id),
  CONSTRAINT FK_queries_surfer_id FOREIGN KEY (`surferName`)
  REFERENCES users(`id`),
  CONSTRAINT FK_queries_host_id FOREIGN KEY (hostId)
  REFERENCES places(`id`)
) ENGINE=INNODB DEFAULT CHARACTER SET utf8;

CREATE INDEX ind_queries ON `queries` (
  `condition`, date_to DESC);


CREATE TABLE `reviews` (
  `id` INTEGER AUTO_INCREMENT,
  `user_id` INTEGER,
  `author_id` INTEGER,
  `text` TINYTEXT NOT NULL,
  `rating` TINYINT DEFAULT 0 CHECK (`rating` IN (0, 1, 2, 3, 4, 5)),
  CONSTRAINT PK_reviews PRIMARY KEY (id),
  CONSTRAINT FK_reviews_user_id FOREIGN KEY (`user_id`)
  REFERENCES users(`id`),
  CONSTRAINT FK_reviews_author_id FOREIGN KEY (`author_id`)
  REFERENCES users(`id`)
) ENGINE=INNODB DEFAULT CHARACTER SET utf8;

CREATE INDEX ind_reviews ON `reviews` (
  `user_id`);

INSERT INTO users (
  `id`,
  `surname`,
  `name`,
  `email`,
  `password`,
  `role`
) VALUES (
  1,
  'Ivanov',
  'Ivan',
  'admin@mail.ru',
  '$2a$10$sKcLvv9iRhK3vOlRzg0Ew.WU8DWdo5sDXAC7WfOOKyMszdSkqKD5u', /*BCrypt password "admin"*/
  0
);

INSERT INTO countries_catalog (`country`) VALUES
  ('Afghanistan'), ('Albania'), ('Algeria'), ('Andorra'), ('Angola'), ('Antigua and Barbuda'), ('Argentina'),
  ('Armenia'), ('Australia'), ('Austria'), ('Azerbaijan'), ('The Bahamas'), ('Bahrain'),
  ('Bangladesh'), ('Barbados'), ('Belarus'), ('Belgium'), ('Belize'), ('Benin'), ('Bhutan'),
  ('Bolivia'), ('Bosnia and Herzegovina'), ('Botswana'), ('Brazil'), ('Brunei'), ('Bulgaria'),
  ('Burkina Faso'), ('Burundi'), ('Cabo Verde'), ('Cambodia'), ('Cameroon'), ('Canada'),
  ('Central African Republic'), ('Chad'), ('Chile'), ('China'), ('Colombia'), ('Comoros'), ('Congo'),
  ('Costa Rica'), ('Côte d’Ivoire'), ('Croatia'), ('Cuba'), ('Cyprus'), ('Czech Republic'),
  ('Denmark'), ('Djibouti'), ('Dominica'), ('Dominican Republic'), ('East Timor'), ('Ecuador'),
  ('Egypt'), ('El Salvador'), ('Equatorial Guinea'), ('Eritrea'), ('Estonia'), ('Eswatini'),
  ('Ethiopia'), ('Fiji'), ('Finland'), ('France'), ('Gabon'), ('The Gambia'), ('Georgia'),
  ('Germany'), ('Ghana'), ('Greece'), ('Grenada'), ('Guatemala'), ('Guinea'),
  ('Guinea-Bissau'), ('Guyana'), ('Haiti'), ('Honduras'), ('Hungary'), ('Iceland'),
  ('India'), ('Indonesia'), ('Iran'), ('Iraq'), ('Ireland'), ('Israel'),
  ('Italy'), ('Jamaica'), ('Japan'), ('Jordan'), ('Kazakhstan'), ('Kenya'), ('Kiribati'),
  ('Korea, North'), ('Korea, South'), ('Kosovo'), ('Kuwait'), ('Kyrgyzstan'), ('Laos');

INSERT INTO cities_catalog (`city`) VALUES
  ('Kabul'), ('Tirana'), ('Algiers'), ('Andorra la Vella'), ('Luanda'), ('Saint John'), ('Buenos Aires'),
  ('Yerevan'), ('Canberra'), ('Vienna'), ('Baku'), ('Nassau'), ('Manama'),
  ('Dhaka'), ('Bridgetown'), ('Minsk'), ('Brussels'), ('Belmopan'), ('Porto-Novo'), ('Thimphu'),
  ('La Paz'), ('Sarajevo'), ('Gaborone'), ('Brasilia'), ('Bandar Seri Begawan'), ('Sofia'),
  ('Ouagadougou'), ('Gitega '), ('Praia'), ('Phnom Penh'), ('Yaounde'), ('Ottawa'),
  ('Bangui'), ('N`Djamena'), ('Santiago'), ('Beijing'), ('Bogota'), ('Moroni'), ('Brazzaville'),
  ('San Jose'), ('Yamoussoukro '), ('Zagreb'), ('Havana'), ('Nicosia'), ('Prague'),
  ('Copenhagen'), ('Djibouti'), ('Roseau'), ('Santo Domingo'), ('Dili'), ('Quito'),
  ('Cairo'), ('San Salvador'), ('Malabo'), ('Asmara'), ('Tallinn'), ('Mbabane'),
  ('Addis Ababa'), ('Suva'), ('Helsinki'), ('Paris'), ('Libreville'), ('Banjul'), ('Tbilisi'),
  ('Berlin'), ('Accra'), ('Athens'), ('Saint George`s'), ('Guatemala City'), ('Conakry'),
  ('Bissau'), ('Georgetown'), ('Port-au-Prince'), ('Tegucigalpa'), ('Budapest'), ('Reykjavik'),
  ('New Delhi'), ('Jakarta'), ('Tehran'), ('Baghdad'), ('Dublin'), ('Jerusalem'),
  ('Rome'), ('Kingston'), ('Tokyo'), ('Amman'), ('Astana'), ('Nairobi'), ('Tarawa Atoll'),
  ('Pyongyang'), ('Seoul'), ('Pristina'), ('Kuwait City'), ('Bishkek'), ('Vientiane');



INSERT INTO users (
  `name`,
  `surname`,
  `email`,
  `password`
) VALUES
  ('Max',
   'Polino',
   'polino89@mail.ru',
   '$2a$10$sGaZbTIxvGxRZAGcg0pWyuVHXjzSBEVtrOfV.L/i6FFnMaLWaVp0K' /** BCrypt "polino89" */
  ),
  ('Anna',
   'Fedorova',
   'anechka@mail.ru',
   '$2a$10$LxXY8CV5Y46Df9W0Q7EQsO/HITWdYwbB6iXJe1IJMxV3b2M8em1lW' /** BCrypt "анюта123" */
  ),
  ('Olga',
   'Grinevich',
   'grinevich@gmail.ru',
   '$2a$10$pvW9Yhs5M/BlKrxiz15utOz8LkjyJKh95KFmPTnQBXCZ8BhRsrEae' /** BCrypt "1234" */
  ),
  ('Chopra',
   'Prienco',
   'posa@mail.ru',
   '$2a$10$mLUSqeYxOHZILnk5uDGEpevrJHVGurJ/0oKjdqPo7uX3kfCYzAOyy' /** BCrypt "1111" */
  ),
  ('Adrian',
   'Abramson',
   'abramson@gmail.com',
   '$2a$10$tJCfyn1DPY4BUWeUCkn4duBR028Bwsk.17VoHKiKJkJ8r9VaEftV.' /** BCrypt "123abramson" */
  ),
  ('Alexa',
   'Adderiy',
   'alexa@gmail.com',
   '$2a$10$avyfbZ.JKp75P5Z58cuGe.1q1Z5pW/DuRgtOii8rpo51eh0rIIw3.' /** BCrypt "alexa12345" */
  ),
  ('Alexandra',
   'Hodges',
   'AlexandraHodges@gmail.com',
   '$2a$10$onWkoZbwDijh7mRfVvXo9u6Fu7bhv0/jygrj/gt8KGVP2L9DMOSm.' /** BCrypt "lovesummer987" */
  ),
  ('Andrew',
   'White',
   'whiteWhite@mail.ru',
   '$2a$10$ur7awtHJjs85E2O2fPTfQu3PpcXmdX2l3TrZwAR4HizT1FYqOWOL6' /** BCrypt "андрюха1982" */
  ),
  ('Angel',
   'Wayne',
   'angelangel@gmail.com',
   '$2a$10$lLU9K5S9lbxMOxS7csWXZO4piLTkjJNoGHOZ8xZmkkGqh2BUtaj3W' /** BCrypt "Angel98Angel" */
  ),
  ('Anita',
   'Walter',
   'anita@mail.ru',
   '$2a$10$hnTBogEn0IXpLxFBXF/tA.c0kyZxgjLF5uhWjK1QLpz/fDzixaepe' /** BCrypt "Walter1996" */
  ),
  ('Ava',
   'Thorndike',
   'ava@gmail.com',
   '$2a$10$eH6k4bxqxfCzuvtYnBGYI.kxOFI/3hUDzABSHIkKr159AmLHizDSm' /** BCrypt "Thorndike1996" */
  ),
  ('Benjamin',
   'Shackley',
   'Shackley@mail.ru',
   '$2a$10$VoufydmBCQrkURXSmCGJ9uqtIIc6m88XpYTlX/5a16bfL5gYMoFdi' /** BCrypt "12345678b" */
  ),
  ('Barbara',
   'Reynolds',
   'barbara@gmail.com',
   '$2a$10$MMjuf3yIl7dP8Mtbv2eNQeAG9STLaUsvv3j9bK.xOotLX3NP7goxu' /** BCrypt "12345678Reynolds" */
  ),
  ('Bruce',
   'Peacock',
   'Peacock@mail.ru',
   '$2a$10$Pv9hvzlZjAOnqshTRdn.sulFavbvLUq6Y3Fpc096runRUKIl6JLR6' /** BCrypt "12345678p" */
  ),
  ('Bryan',
   'Otis',
   'OtisBryan@gmail.com',
   '$2a$10$fusz6pBrzu8.bYwukVjKYOz0v3ouNS3KnfjAvTd9DQGVprczebgP2' /** BCrypt "12345678o" */
  ),
  ('Caroline',
   'Nevill',
   'Nevill@gmail.com',
   '$2a$10$FiDv5TNHio86v9e9no5lG.fvCe8QzJLsXZuTv9r6v7f8Qrd9.Ynsq' /** BCrypt "Nevill18" */
  ),
  ('Christian',
   'Miller',
   'MillerChristian@mail.ru',
   '$2a$10$sfb9IM.i3lt8x5IVvpA2weaEMmQOfOd4cvgCMFImmtVOyILqVk2Nq' /** BCrypt "Miller27" */
  ),
  ('Cody',
   'Macey',
   'CodyMacey@mail.ru',
   '$2a$10$U1M8P7IPMaRd6Y9ByDt8x.GLRuWOMe5XnDFAxrAswUwCM7jo.z8HO' /** BCrypt "CodyMacey00" */
  ),
  ('Ethan',
   'Leman',
   'Leman@gmail.com',
   '$2a$10$RI9umFxg6q8sdMF5rxwgjey2mn72PDil7/jAKQLRtum.FQzMAXYZG' /** BCrypt "Lemanlem64" */
  );

INSERT INTO `users_info` (
  `phone`, is_host,
  `rating`, `profile`, `country_id`, `city_id`
) VALUES (
  '+79668182134', 0,
  3, 'I like travelling.', 5, 5
),
  ('+375294444444', 1,
   5, 'I like to receive quests', 7, 7
  ),
  (
    '+79668002134', 0,
    10, 'I like sport.', 10, 10
  ),
  ('+375294444444', 1,
   18, 'I like reading books', 8, 8
  ),
  ('+375294444444', 1,
   54, 'I like reading books', 8, 8
  ),
  ('+375291234444', 1,
   29, '', 6, 6
  ),
  ('+375334414444', 1,
   32, 'I am a vegetarian', 3, 3
  ),
  ('+375294144444', 1,
   45, '', 7, 7
  ),
  ('+375294988744', 1,
   14, '', 5, 5
  );


INSERT INTO queries (`surfer_id`, `host_id`,
                     `date_from`, `date_to`, `condition`, `message`
) VALUES
  (2, 1, '2019-11-21', '2019-11-23', 2, 'I want to visit you'),
  (4, 1, '2019-09-21', '2019-09-23', 1, 'Hi');

INSERT INTO reviews (`user_id`, `author_id`, `text`) VALUES
  (1, 2, 'Good guy'),
  (3, 4, 'I am happy to know this person');
