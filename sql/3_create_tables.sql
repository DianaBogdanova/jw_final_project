USE `hospitality_network`;

CREATE TABLE `countries_catalog` (
	`id` INTEGER AUTO_INCREMENT,
	`country` VARCHAR(25) UNIQUE,
	CONSTRAINT PK_countries PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARACTER SET utf8;
CREATE INDEX ind_countries_catalog ON `countries_catalog` (
	`country`);

/**
countries.id  = cities.id (capital)
 */

CREATE TABLE `cities_catalog` (
	`id` INTEGER AUTO_INCREMENT,
	`city` VARCHAR(25) UNIQUE,
	`country_id` INTEGER NOT NULL,
	CONSTRAINT PK_cities PRIMARY KEY (id),
	CONSTRAINT FK_cities_catalog FOREIGN KEY (country_id)
	REFERENCES countries_catalog(`id`)
) ENGINE=INNODB DEFAULT CHARACTER SET utf8;

CREATE INDEX ind_cities_catalog ON `cities_catalog` (
	`city`);


CREATE TABLE `users` (
	`id` INTEGER AUTO_INCREMENT,
	`surname` VARCHAR(45) NOT NULL,
	`name` VARCHAR(45) NOT NULL,
	`email` VARCHAR(254) NOT NULL UNIQUE,
	`password` VARCHAR(60) NOT NULL,/**BCrypt psw*/
	/*
	 * 0 - администратор (Role.ADMINISTRATOR)
	 * 1 - зарегистрированный пользователь (Role.REGISTERED_USER)
	 */
	`role` TINYINT NOT NULL DEFAULT 1,
	CONSTRAINT check_role CHECK (`role` IN (0, 2)),
	CONSTRAINT PK_users PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARACTER SET utf8;

CREATE UNIQUE INDEX ind_email ON `users` (
`email`);

CREATE TABLE `users_info` (
	`id` INTEGER AUTO_INCREMENT,
	`phone` LONG,
	/*
	 * 0 - не принимает гостей
	 * 1 - принимает гостей
	 */
	`is_host` TINYINT DEFAULT 0 CHECK (`is_host` IN (0, 1)),
	`rating` INT DEFAULT 0,
  `profile` TINYTEXT,
	/*
	 * 0 - active
	 * 1 - blocked
	 */
	`is_active` BOOLEAN DEFAULT 1,
	`country_id` INTEGER,
	`city_id` INTEGER,
  CONSTRAINT PK_users_info PRIMARY KEY (id),
	CONSTRAINT FK_users_info FOREIGN KEY (id)
	REFERENCES users(`id`),
	CONSTRAINT FK_country_id FOREIGN KEY (country_id)
	REFERENCES countries_catalog(`id`),
	CONSTRAINT FK_city_id FOREIGN KEY (city_id)
	REFERENCES cities_catalog(`id`)
) ENGINE=INNODB DEFAULT CHARACTER SET utf8;

CREATE INDEX ind_host ON `users_info` (
`is_host`);


CREATE TABLE `avatars` (
	`id` INTEGER AUTO_INCREMENT,
	`user_id` INTEGER NOT NULL,
	`path` VARCHAR(255) NOT NULL,
	CONSTRAINT PK_gallery PRIMARY KEY (id),
	CONSTRAINT FK_gallery_user_id FOREIGN KEY (user_id)
	REFERENCES users(`id`)
) ENGINE=INNODB DEFAULT CHARACTER SET utf8;

CREATE INDEX ind_avatars ON avatars (
`user_id`);


CREATE TABLE `queries` (
	`id` INTEGER AUTO_INCREMENT,
	`surfer_id` INTEGER NOT NULL,
	`host_id` INTEGER NOT NULL,
	`date_from` DATE NOT NULL,
	`date_to` DATE NOT NULL,
	`message` TINYTEXT,
	/*
	 * 0 - принят (Condition.ACCEPTED)
	 * 1 - отклонен (Condition.REJECTED)
	 * 2 - на рассмотрении (Condition.PENDING)
	 */
	`condition` TINYINT DEFAULT 2 CHECK (`condition` IN (0, 1, 2)),
	CONSTRAINT PK_queries PRIMARY KEY (id),
	CONSTRAINT FK_queries_surfer_id FOREIGN KEY (`surferName`)
	REFERENCES users(`id`),
	CONSTRAINT FK_queries_host_id FOREIGN KEY (hostId)
	REFERENCES users(`id`)
) ENGINE=INNODB DEFAULT CHARACTER SET utf8;

CREATE INDEX ind_queries ON `queries` (
`condition`, date_to DESC);


CREATE TABLE `reviews` (
	`id` INTEGER AUTO_INCREMENT,
	`user_id` INTEGER,
	`author_id` INTEGER,
	`text` TEXT NOT NULL,
	`rating` TINYINT DEFAULT 0 CHECK (`rating` IN (0, 1, 2, 3, 4, 5)),
	CONSTRAINT PK_reviews PRIMARY KEY (id),
	CONSTRAINT FK_reviews_user_id FOREIGN KEY (`user_id`)
	REFERENCES users(`id`),
	CONSTRAINT FK_reviews_author_id FOREIGN KEY (`author_id`)
	REFERENCES users(`id`)
) ENGINE=INNODB DEFAULT CHARACTER SET utf8;

CREATE INDEX ind_reviews ON `reviews` (
`user_id`);