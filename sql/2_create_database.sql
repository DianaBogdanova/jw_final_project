CREATE DATABASE `hospitality_network` DEFAULT CHARACTER SET utf8;

GRANT SELECT,INSERT,UPDATE,DELETE
ON `hospitality_network`.*
TO hospitality_network_user@localhost
IDENTIFIED BY 'hospitality_network_password';

GRANT SELECT,INSERT,UPDATE,DELETE
ON `hospitality_network`.*
TO hospitality_network_user@'%'
IDENTIFIED BY 'hospitality_network_password';