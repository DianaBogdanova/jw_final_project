USE `hospitality_network`;

INSERT INTO users (
  `name`,
  `surname`,
  `email`,
  `password`
) VALUES
  ('Max',
   'Polino',
   'polino89@mail.ru',
   '$2a$10$sGaZbTIxvGxRZAGcg0pWyuVHXjzSBEVtrOfV.L/i6FFnMaLWaVp0K' /** BCrypt "polino89" */
  ),
  ('Anna',
   'Fedorova',
   'anechka@mail.ru',
   '$2a$10$LxXY8CV5Y46Df9W0Q7EQsO/HITWdYwbB6iXJe1IJMxV3b2M8em1lW' /** BCrypt "анюта123" */
  ),
  ('Olga',
   'Grinevich',
   'grinevich@gmail.ru',
   '$2a$10$pvW9Yhs5M/BlKrxiz15utOz8LkjyJKh95KFmPTnQBXCZ8BhRsrEae' /** BCrypt "1234" */
  ),
  ('Chopra',
   'Prienco',
   'posa@mail.ru',
   '$2a$10$mLUSqeYxOHZILnk5uDGEpevrJHVGurJ/0oKjdqPo7uX3kfCYzAOyy' /** BCrypt "1111" */
  ),
  ('Adrian',
   'Abramson',
   'abramson@gmail.com',
   '$2a$10$tJCfyn1DPY4BUWeUCkn4duBR028Bwsk.17VoHKiKJkJ8r9VaEftV.' /** BCrypt "123abramson" */
  ),
  ('Alexa',
   'Adderiy',
   'alexa@gmail.com',
   '$2a$10$avyfbZ.JKp75P5Z58cuGe.1q1Z5pW/DuRgtOii8rpo51eh0rIIw3.' /** BCrypt "alexa12345" */
  ),
  ('Alexandra',
     'Hodges',
     'AlexandraHodges@gmail.com',
     '$2a$10$onWkoZbwDijh7mRfVvXo9u6Fu7bhv0/jygrj/gt8KGVP2L9DMOSm.' /** BCrypt "lovesummer987" */
  ),
  ('Andrew',
     'White',
     'whiteWhite@mail.ru',
     '$2a$10$ur7awtHJjs85E2O2fPTfQu3PpcXmdX2l3TrZwAR4HizT1FYqOWOL6' /** BCrypt "андрюха1982" */
  ),
  ('Angel',
     'Wayne',
     'angelangel@gmail.com',
     '$2a$10$lLU9K5S9lbxMOxS7csWXZO4piLTkjJNoGHOZ8xZmkkGqh2BUtaj3W' /** BCrypt "Angel98Angel" */
  ),
  ('Anita',
     'Walter',
     'anita@mail.ru',
     '$2a$10$hnTBogEn0IXpLxFBXF/tA.c0kyZxgjLF5uhWjK1QLpz/fDzixaepe' /** BCrypt "Walter1996" */
  ),
  ('Ava',
     'Thorndike',
     'ava@gmail.com',
     '$2a$10$eH6k4bxqxfCzuvtYnBGYI.kxOFI/3hUDzABSHIkKr159AmLHizDSm' /** BCrypt "Thorndike1996" */
  ),
  ('Benjamin',
     'Shackley',
     'Shackley@mail.ru',
     '$2a$10$VoufydmBCQrkURXSmCGJ9uqtIIc6m88XpYTlX/5a16bfL5gYMoFdi' /** BCrypt "12345678b" */
  ),
  ('Barbara',
     'Reynolds',
     'barbara@gmail.com',
     '$2a$10$MMjuf3yIl7dP8Mtbv2eNQeAG9STLaUsvv3j9bK.xOotLX3NP7goxu' /** BCrypt "12345678Reynolds" */
  ),
  ('Bruce',
     'Peacock',
     'Peacock@mail.ru',
     '$2a$10$Pv9hvzlZjAOnqshTRdn.sulFavbvLUq6Y3Fpc096runRUKIl6JLR6' /** BCrypt "12345678p" */
  ),
  ('Bryan',
     'Otis',
     'OtisBryan@gmail.com',
     '$2a$10$fusz6pBrzu8.bYwukVjKYOz0v3ouNS3KnfjAvTd9DQGVprczebgP2' /** BCrypt "12345678o" */
  ),
  ('Caroline',
     'Nevill',
     'Nevill@gmail.com',
     '$2a$10$FiDv5TNHio86v9e9no5lG.fvCe8QzJLsXZuTv9r6v7f8Qrd9.Ynsq' /** BCrypt "Nevill18" */
  ),
  ('Christian',
   'Miller',
   'MillerChristian@mail.ru',
   '$2a$10$sfb9IM.i3lt8x5IVvpA2weaEMmQOfOd4cvgCMFImmtVOyILqVk2Nq' /** BCrypt "Miller27" */
  ),
  ('Cody',
    'Macey',
    'CodyMacey@mail.ru',
    '$2a$10$U1M8P7IPMaRd6Y9ByDt8x.GLRuWOMe5XnDFAxrAswUwCM7jo.z8HO' /** BCrypt "CodyMacey00" */
  ),
  ('Ethan',
    'Leman',
    'Leman@gmail.com',
    '$2a$10$RI9umFxg6q8sdMF5rxwgjey2mn72PDil7/jAKQLRtum.FQzMAXYZG' /** BCrypt "Lemanlem64" */
  ),
  ('Anna',
   'Bobko',
   'Bobko@gmail.com',
   '$2a$10$lLU9K5S9lbxMOxS7csWXZO4piLTkjJNoGHOZ8xZmkkGqh2BUtaj3W' /** BCrypt "Angel98Angel" */
  ),
  ('Diana',
  'Bogdanova',
  'di@mail.ru',
  '$2a$10$mLUSqeYxOHZILnk5uDGEpevrJHVGurJ/0oKjdqPo7uX3kfCYzAOyy');

INSERT INTO `users_info` (`id`) VALUE (1); /**admin*/

SET FOREIGN_KEY_CHECKS=0;

INSERT INTO `users_info` (
 `id`, `phone`, is_host,
`rating`, `profile`, `country_id`, `city_id`
) VALUES (
2,  '+79668182134', 0,
9, 'I like travelling.', 112, 112
),
(3, '+375294444444', 1,
10, 'I like to receive quests', 16, 16
),
(
4, '+79668002134', 0,
10, 'I like sport.', 112, 112
),
(5, '+375291244444', 1,
10, 'I am pleased to support communication after the trip.', 16, 16
),
(6, '+375294234544', 1,
 10, 'I like reading books', 16, 16
),
(7, '+375298124223', 1,
10, 'I can provide you with one floor in my big house.', 16, 16
),
(8, '+375336144444', 0,
10, 'I am a vegetarian', 16, 16
),
(9, '+375292164454', 0,
-10, 'I play the piano at night. If you like the sounds of the piano at night - welcome', 16, 16
),
(10, '+375295121744', 0,
-1, 'I am a sports, erudite person without bad habits', 16, 16
),
(
  11, '+79668180034', 0,
  9, 'I can teach you to dance tango and play Chinese chess', 112, 112
),
(12, '+79618182123', 1,
 10, 'I will be glad to invite you to my house if you are not going to steal my clothes.', 112, 112
),
(
  13, '+79668128934', 0,
  10, 'I love to eat and cook. I can feed your family', 112, 112
),
(14, '+79667182131', 1,
 10, 'Men please do not disturb me. Women are welcome', 112, 112
),
(15, '+79666782139', 1,
 5, 'Do you like to receive guests? Then invite me and I will convince you otherwise.', 112, 112
),
(16, '+34694493904', 1,
 15, 'I have visited 37 countries', 65, 65
),
(17, '+34692493993', 0,
 10, 'I love to dance, sing in the shower and tell jokes. I am a vegetarian', 65, 65
),
(18, '+34696493903', 1,
 -5, 'I am pleased to invite you to my house. I will not object to children and animals', 65, 65
),
(19, '+34695493901', 0,
 -5, 'I just want to spend the night. I do not need to advise anything and introduce me to your dog.', 65, 65
),
(20, '0033142955001', 1,
 -1, 'I like new acquaintances, I can live in any conditions, for me it is only important to meet interesting people.', 61, 61
),
(21, '0033642155001', 1,
 10, 'I am sociable, cheerful, I like active rest. Traveling with my cat', 61, 61
),
(22, '+375298387178', 1,
 15, 'I like chocolate', 61, 61
);

SET FOREIGN_KEY_CHECKS=1;

INSERT INTO queries (`surfer_id`, `host_id`,
`date_from`, `date_to`, `condition`, `message`
) VALUES
(2, 20, '2019-12-09', '2019-12-15', 2, 'I want to visit you'),
(3, 19, '2019-09-21', '2019-09-23', 1, 'Hi. I will be glad if you agree to accept me.'),
(4, 18, '2019-11-03', '2019-11-10', 2, 'I love silence and purity'),
(5, 22, '2019-10-21', '2019-10-23', 0, 'I promise to keep clean'),
(6, 22, '2019-09-04', '2019-09-05', 2, 'I travel with my cat'),
(7, 22, '2019-08-01', '2019-08-03', 1, 'I am ready to cook breakfast for you'),
(8, 22, '2019-07-11', '2019-07-12', 2, 'I can teach you to dance tango'),
(9, 17, '2019-06-21', '2019-06-23', 1, 'I am allergic to cats. I hope there are no cats in your house.'),
(10, 12, '2019-05-06', '2019-05-09', 2, 'I like singing in the shower'),
(11, 11, '2019-04-20', '2019-04-22', 1, 'I will be glad to visit you with my friend'),
(12, 11, '2019-03-16', '2019-03-17', 2, 'I promise you will have a lot of fun with me.'),
(13, 10, '2019-02-09', '2019-02-12', 2, 'I want to visit you'),
(14, 9, '2019-11-23', '2019-11-26', 1, 'Hi. I will be glad if you agree to accept me.'),
(15, 9, '2019-04-16', '2019-04-18', 2, 'I love silence and purity'),
(16, 8, '2019-07-05', '2019-07-06', 1, 'I promise to keep clean'),
(17, 7, '2019-08-15', '2019-08-17', 2, 'I travel with my cat'),
(18, 7, '2019-09-21', '2019-09-23', 1, 'I am ready to cook breakfast for you'),
(19, 6, '2019-01-15', '2019-11-19', 2, 'I can teach you to dance tango'),
(22, 5, '2019-05-12', '2019-05-14', 1, 'I am allergic to cats. I hope there are no cats in your house.'),
(22, 4, '2019-03-10', '2019-03-11', 2, 'I like singing in the shower'),
(22, 3, '2019-09-25', '2019-09-27', 1, 'I will be glad to visit you with my friend'),
(22, 2, '2019-10-21', '2019-10-23', 2, 'I promise you will have a lot of fun with me.');

INSERT INTO reviews (`user_id`, `author_id`, `text`, `rating`) VALUES
(2, 20, 'The person is cheerful, erudite and kind. But not very careful', 4),
(2, 15, 'We had a great time at Max place. The communication
was super easy and quick. The neighborhood is very conveniently located and
although it’s a lively neighborhood the apartment is very quiet!
Would absolutely recommend to stay there!', 5),
(3, 12, 'I highly recommend Anna\'s house. She is such an amazing host
and her home is perfect. Everything was very clean and she had basic food
(i.e. milk, eggs, bread) waiting for us when we arriwed. She is more than willing
to make you stay as comfortable and conveniet as possible.', 5),
(3, 4, 'It was my pleasure hosting Anna. Anna has respected my house rules,
communication ran smoothly, and most importantly, was super friendly!
I highly recommend Anna', 5),
(4, 22, 'Olga was a great guest because took great care of my space,
left it clean and tidy, and was a pleasure to speak with. I’d be
glad to host Olga again anytime.', 5),
(4, 17, 'I loved having Olga because was great with
corresponding the purpose of the trip and treated my place with
respect. I and any other host, would be lucky to have Olga
again!', 5),
(5, 20, 'Chopra was an amazing guest.
Communication was as smooth as can be, the place was left in great condition,
and the house rules were respected. I hope to host Chopra again
in the future!', 5),
(5, 19, 'I have enjoyed hosting Chopra. I think Chopra is such a kind person.
Our experience was smooth from the beginning to the end. I hope to have
the opportunity to host Chopra again!', 5),
(6, 5, 'I recommend Adrian to other hosts. Our experience with
Adrian has been great. Communication with Adrian was good and the place was in order.', 5),
(6, 2, 'When we first stumbled upon this flat, it seemed almost too good to be true.
There must be a catch! But everything was as perfect as it seemed online. Adrian
is the most thoughtful, gracious host.
He provided us with all the information we needed and showed us around. The flat
is just perfect! Kitchen is compact and contains everything one needs.
The bed is comfortable, bathroom and terrace are excellent. Location is incredible.
Adrian had excellent restaurant recommendations and was of great help.
He went way beyond to make our long-weekend comfortable and memorable.
Thanks Adrian for a wonderful weekend!', 5),
(7, 12, 'Very tidy and lovely apartment equipped with everything
you need. A good bed and nice bathroom. Alexa is a great host and
there when you need her, Very nice and wants to share all she know about
the area. We had a great stay!', 5),
(7, 14, 'I would recommend Alexa to anyone renting out rooms! She was
the perfect guest! The good qualities you would expect from a guest in
your own home. She was polite, considerate, sociable, helpful and quiet.
It felt good, safe and comfortable to have her/him living here. As a person
she was wonderful to get to know. I would recommend her to anyone
renting out rooms! You are always warmly welcome back! I wish you all the
best.', 5),
(8, 18, 'Alexandras place is at a perfect location, very easy to get around
with public transportation and walking distance to awesome bars and restaurants.
Her place is great, very clean and has everything you need. Alexandra was also
very helpful since the beginning, available at all times whenever I had any
questions. Totally recommend it.', 5),
(8, 16, 'Alexandra came to visit with her partner and they are wonderful
guests! I would highly recommend them to future hosts, they left the
studio super clean. Thanks!', 5),
(9, 9, 'The condition of the rooms were very bad. Bed sheets, linens were dirty.
Toilet was horrible. Ambience was very bad.', -5),
(9, 10, 'Very rude person.', -5),
(10, 11, 'Hot water constantly running out. - No ventilation in bathroom
(No window or extractor fan) leaving the bathroom misty after taking a shower
and leaving it smelly after using the loo. - Virtually no cooking utensils,
making a basic task such as hard boiling an egg extremely difficult. No sponge
to clean any dishes. - Beds were extremely uncomfortable, may as well of slept
on their sun loungers. - Pillows were solid hurting your neck when you slept.', -5),
(10, 7, 'Angel stayed with us and were the perfect guests. Great to talk to,
interesting people. We would highly recommend them to all other hosts.
They took care of our home as if it were their own.', 4),
  (11, 16, 'The person is cheerful, erudite and kind. But not very careful', 4),
  (11, 9, 'We had a great time at Anita\'s place. The communication
was super easy and quick. The neighborhood is very conveniently located and
although it’s a lively neighborhood the apartment is very quiet!
Would absolutely recommend to stay there!', 5),
  (12, 17, 'I highly recommend Ava\'s house. She is such an amazing host
and her home is perfect. Everything was very clean and she had basic food
(i.e. milk, eggs, bread) waiting for us when we arriwed. She is more than willing
to make you stay as comfortable and conveniet as possible.', 5),
  (12, 7, 'It was my pleasure hosting Ava. Ava has respected my house rules,
communication ran smoothly, and most importantly, was super friendly!
I highly recommend Ava', 5),
  (13, 8, 'Benjamin was a great guest because took great care of my space,
left it clean and tidy, and was a pleasure to speak with. I’d be
glad to host Benjamin again anytime.', 5),
  (13, 9, 'I loved having Benjamin because was great with
corresponding the purpose of the trip and treated my place with
respect. I and any other host, would be lucky to have Benjamin
again!', 5),
  (14, 6, 'Barbara was an amazing guest.
Communication was as smooth as can be, the place was left in great condition,
and the house rules were respected. I hope to host Barbara again
in the future!', 5),
  (14, 5, 'I have enjoyed hosting Barbara. I think Barbara is such a kind person.
Our experience was smooth from the beginning to the end. I hope to have
the opportunity to host Barbara again!', 5),
  (15, 4, 'I recommend Adrian to other hosts. Our experience with
Adrian has been great. Communication with Adrian was good and the place was in order.', 5),
  (16, 3, 'When we first stumbled upon this flat, it seemed almost too good to be true.
There must be a catch! But everything was as perfect as it seemed online. Bryan
is the most thoughtful, gracious host.
He provided us with all the information we needed and showed us around. The flat
is just perfect! Kitchen is compact and contains everything one needs.
The bed is comfortable, bathroom and terrace are excellent. Location is incredible.
Adrian had excellent restaurant recommendations and was of great help.
He went way beyond to make our long-weekend comfortable and memorable.
Thanks Bryan for a wonderful weekend!', 5),
  (16, 22, 'Very tidy and lovely apartment equipped with everything
you need. A good bed and nice bathroom. Bryan is a great host and
there when you need him, Very nice and wants to share all he know about
the area. We had a great stay!', 5),
  (16, 21, 'I would recommend Bryan to anyone renting out rooms! She was
the perfect guest! The good qualities you would expect from a guest in
your own home. She was polite, considerate, sociable, helpful and quiet.
It felt good, safe and comfortable to have her/him living here. As a person
she was wonderful to get to know. I would recommend her to anyone
renting out rooms! You are always warmly welcome back! I wish you all the
best.', 5),
  (17, 19, 'Caroline\'s place is at a perfect location, very easy to get around
with public transportation and walking distance to awesome bars and restaurants.
Her place is great, very clean and has everything you need. Caroline was also
very helpful since the beginning, available at all times whenever I had any
questions. Totally recommend it.', 5),
  (17, 21, 'Caroline came to visit with her partner and they are wonderful
guests! I would highly recommend them to future hosts, they left the
studio super clean. Thanks!', 5),
  (18, 22, 'The condition of the rooms were very bad. Bed sheets, linens were dirty.
Toilet was horrible. Ambience was very bad.', -5),
  (19, 18, 'Very rude person.', -5),
  (20, 5, 'Hot water constantly running out. - No ventilation in bathroom
(No window or extractor fan) leaving the bathroom misty after taking a shower
and leaving it smelly after using the loo. - Virtually no cooking utensils,
making a basic task such as hard boiling an egg extremely difficult. No sponge
to clean any dishes. - Beds were extremely uncomfortable, may as well of slept
on their sun loungers. - Pillows were solid hurting your neck when you slept.', -5),
  (20, 7, 'Ethan stayed with us and were the perfect guests. Great to talk to,
interesting people. We would highly recommend them to all other hosts.
They took care of our home as if it were their own.', 4),
(21, 14, 'Anna was an outstanding guest. She was excellent communicator,
friendly and left the apartment in immaculate condition. I would welcome
her back anytime.', 5),
(21, 16, 'I am happy to know this person', 5),
(22, 20, 'This world is in good hands if this lovely young person get to be
‘in charge’! Lovely Diana was simply wonderful person & we were fortunate
to host this guest. Cannot recommend highly enough!! Communication, respectful
of us and our property =100%', 5),
(22, 18, 'I highly recommend Diana\'s house. She is such an amazing host
and her home is perfect. Everything was very clean and she had basic food
(i.e. milk, eggs, bread) waiting for us when we arriwed. She is more than willing
to make you stay as comfortable and conveniet as possible.', 5),
(22, 2, 'Diana was absolutely perfect guest to host!! The house was left
in pristine condition, and she was super friendly and considerate. I
would recommend her as the perfect guest to host in your property!', 5);

INSERT INTO `avatars`(`user_id`, `path`) VALUES
  (1, ''),
  (2, '../../img/avatar/userm1.png'),
  (3, '../../img/avatar/userw1.jpg'),
  (4, '../../img/avatar/userw2.jpg'),
  (5, '../../img/avatar/userw3.jpg'),
  (6, '../../img/avatar/userm2.jpg'),
  (7, '../../img/avatar/userw4.jpeg'),
  (8, '../../img/avatar/userw5.jpg'),
  (9, '../../img/avatar/userm3.jpg'),
  (10, '../../img/avatar/userw6.jpg'),
  (11, '../../img/avatar/userw7.jpg'),
  (12, '../../img/avatar/userm4.jpg'),
  (13, '../../img/avatar/userm5.jpg'),
  (14, '../../img/avatar/userw8.jpg'),
  (15, '../../img/avatar/userm6.jpg'),
  (16, '../../img/avatar/userm7.jpg'),
  (17, '../../img/avatar/userw9.jpg'),
  (18, '../../img/avatar/userm7.jpg'),
  (19, '../../img/avatar/userm8.jpg'),
  (20, '../../img/avatar/userm9.jpg'),
  (21, '../../img/avatar/userw10.jpg'),
  (22, '../../img/avatar/userw12.jpg');