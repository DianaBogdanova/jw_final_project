USE `hospitality_network`;

INSERT INTO users (
`id`,
`surname`,
`name`,
`email`,
`password`,
`role`
) VALUES (
 1,
'Ivanov',
'Ivan',
'admin@mail.ru',
'$2a$10$sKcLvv9iRhK3vOlRzg0Ew.WU8DWdo5sDXAC7WfOOKyMszdSkqKD5u', /*BCrypt password "admin"*/
0
);

INSERT INTO countries_catalog (`country`) VALUES
  ('Afghanistan'), ('Albania'), ('Algeria'), ('Andorra'), ('Angola'), ('Antigua and Barbuda'), ('Argentina'),
  ('Armenia'), ('Australia'), ('Austria'), ('Azerbaijan'), ('The Bahamas'), ('Bahrain'),
  ('Bangladesh'), ('Barbados'), ('Belarus'), ('Belgium'), ('Belize'), ('Benin'), ('Bhutan'),
  ('Bolivia'), ('Bosnia and Herzegovina'), ('Botswana'), ('Brazil'), ('Brunei'), ('Bulgaria'),
  ('Burkina Faso'), ('Burundi'), ('Cabo Verde'), ('Cambodia'), ('Cameroon'), ('Canada'),
  ('Central African Republic'), ('Chad'), ('Chile'), ('China'), ('Colombia'), ('Comoros'), ('Congo'),
  ('Costa Rica'), ('Côte d’Ivoire'), ('Croatia'), ('Cuba'), ('Cyprus'), ('Czech Republic'),
  ('Denmark'), ('Djibouti'), ('Dominica'), ('Dominican Republic'), ('East Timor'), ('Ecuador'),
  ('Egypt'), ('El Salvador'), ('Equatorial Guinea'), ('Eritrea'), ('Estonia'), ('Eswatini'),
  ('Ethiopia'), ('Fiji'), ('Finland'), ('France'), ('Gabon'), ('The Gambia'), ('Georgia'),
  ('Germany'), ('Ghana'), ('Greece'), ('Grenada'), ('Guatemala'), ('Guinea'),
  ('Guinea-Bissau'), ('Guyana'), ('Haiti'), ('Honduras'), ('Hungary'), ('Iceland'),
  ('India'), ('Indonesia'), ('Iran'), ('Iraq'), ('Ireland'), ('Israel'),
  ('Italy'), ('Jamaica'), ('Japan'), ('Jordan'), ('Kazakhstan'), ('Kenya'), ('Kiribati'),
  ('Korea, North'), ('Korea, South'), ('Kosovo'), ('Kuwait'), ('Kyrgyzstan'), ('Laos'),
  ('Latvia'), ('Lebanon'), ('Liechtenstein'), ('Maldives'), ('Mexico'), ('Moldova'), ('Monaco'),
  ('Morocco'), ('Netherlands'), ('New Zealand'), ('Norway'), ('Pakistan'), ('Papua New Guinea'),
  ('Poland'), ('Portugal'), ('Qatar'), ('Russia'), ('San Marino'), ('Serbia'),
  ('Seychelles'), ('Singapore'), ('Slovakia'), ('Slovenia'), ('Spain'), ('Sri Lanka'),
  ('Swaziland'), ('Sweden'), ('Switzerland'), ('Syria'), ('Thailand'), ('Turkey'), ('Turkmenistan'),
  ('Ukraine'), ('United Arab Emirates'), ('United Kingdom'), ('United States of America'), ('Uzbekistan'), ('Vatican '),
  ('Vietnam'), ('Yemen'), ('Zambia'), ('Zimbabwe');

ALTER TABLE cities_catalog AUTO_INCREMENT = 1;

INSERT INTO cities_catalog (`city`, `country_id`) VALUES
  ('Kabul', 1), ('Tirana', 2), ('Algiers', 3), ('Andorra la Vella', 4), ('Luanda', 5),
  ('Saint John', 6), ('Buenos Aires', 7),
  ('Yerevan', 8), ('Canberra', 9), ('Vienna', 10), ('Baku', 11), ('Nassau', 12), ('Manama', 13),
  ('Dhaka', 14), ('Bridgetown', 15), ('Minsk', 16), ('Brussels', 17), ('Belmopan', 18),
  ('Porto-Novo', 19), ('Thimphu', 20),
  ('La Paz', 21), ('Sarajevo', 22), ('Gaborone', 23), ('Brasilia', 24), ('Bandar Seri Begawan', 25),
  ('Sofia', 26),
  ('Ouagadougou', 27), ('Gitega', 28), ('Praia', 29), ('Phnom Penh', 30), ('Yaounde', 31), ('Ottawa', 32),
  ('Bangui', 33), ('N`Djamena', 34), ('Santiago', 35), ('Beijing', 36), ('Bogota', 37),
  ('Moroni', 38), ('Brazzaville', 39),
  ('San Jose', 40), ('Yamoussoukro', 41), ('Zagreb', 42), ('Havana', 43), ('Nicosia', 44), ('Prague', 45),
  ('Copenhagen', 46), ('Djibouti', 47), ('Roseau', 48), ('Santo Domingo', 49), ('Dili', 50), ('Quito', 51),
  ('Cairo', 52), ('San Salvador', 53), ('Malabo', 54), ('Asmara', 55), ('Tallinn', 56), ('Mbaban', 57),
  ('Addis Ababa', 58), ('Suva', 59), ('Helsinki', 60), ('Paris', 61), ('Libreville', 62),
  ('Banjul', 63), ('Tbilisi', 64),
  ('Berlin', 65), ('Accra', 66), ('Athens', 67), ('Saint George`s', 68), ('Guatemala City', 69), ('Conakry', 70),
  ('Bissau', 71), ('Georgetown', 72), ('Port-au-Prince', 73), ('Tegucigalpa', 74),
  ('Budapest', 75), ('Reykjavik', 76),
  ('New Delhi', 77), ('Jakarta', 78), ('Tehran', 79), ('Baghdad', 80), ('Dublin', 81), ('Jerusalem', 82),
  ('Rome', 83), ('Kingston', 84), ('Tokyo', 85), ('Amman', 86), ('Astana', 87), ('Nairobi', 88),
  ('Tarawa Atoll', 89),
  ('Pyongyang', 90), ('Seoul', 91), ('Pristina', 92), ('Kuwait City', 93), ('Bishkek', 94), ('Vientiane', 95),
  ('Riga', 96), ('Beirut', 97), ('Vaduz', 98), ('Male', 99), ('Mexico City', 100), ('Chisinau', 101), ('Monaco', 102),
  ('Rabat', 103), ('Amsterdam', 104), ('Wellington', 105), ('Oslo', 106), ('Islamabad', 107), ('Port Moresby', 108),
  ('Warsaw', 109), ('Lisbon', 110), ('Doha', 111), ('Moscow', 112), ('San Marino', 113), ('Belgrade', 114),
  ('Victoria', 115), ('Singapore', 116), ('Bratislava', 117), ('Ljubljana', 118), ('Madrid', 119), ('Colombo', 120),
  ('Mbabane', 121), ('Stockholm', 122), ('Bern', 123), ('Damascus', 124), ('Bangkok', 125), ('Ankara', 126), ('Ashgabat', 127),
  ('Kyiv', 128), ('Abu Dhabi', 129), ('London', 130), ('Washington', 131), ('Tashkent', 132), ('Vatican', 133),
  ('Hanoi', 134), ('Sanaa', 135), ('Lusaka', 136), ('Harare', 137);