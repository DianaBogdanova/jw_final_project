<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<footer class="page-footer font-small unique-color-dark" style="background-image: url(../../../img/header.jpg); background-size: cover">

    <div style="background-color: #6f42c1">
        <div class="container">
            <div class="row py-4 d-flex align-items-center">
                <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
                    <h6 class="mb-0">Get connected with us on social networks!</h6>
                </div>
                <div class="col-md-6 col-lg-7 text-center text-md-right">

                    <a href="https://www.facebook.com/profile.php?id=100004250680724&ref=bookmarks"><i class="fa fa-facebook mr-4"></i></a>
                    <a href="https://twitter.com/travel_with_me7"><i class="fa fa-twitter mr-4"></i></a>
                    <a href="https://vk.com/away.php?to=https%3A%2F%2Finstagram.com%2Fdi_laevskay_%3Figshid%3D9g7z6c2v2bnh&cc_key="><i class="fa fa-instagram mr-4"></i></a>
                    <a href="https://vk.com/id177344356"><i class="fa fa-vk mr-4"></i></a>

                </div>
            </div>
        </div>
    </div>

    <div class="container text-center text-md-left mt-5">
        <div class="row mt-3">
            <div class="col-6">
                <h4 class="text-uppercase font-weight-bold" style="color: black">HospEx</h4>
                <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p>We connect members to a global
                    community of travelers. Using HospEx you can find a place to stay or share
                    your home with travelers.</p>

            </div>

            <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">

                <h6 class="text-uppercase font-weight-bold">Contact</h6>
                <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p>
                    <i class="fa fa-home mr-2"></i> Minsk</p>
                <p>
                    <i class="fa fa-envelope-o mr-2"></i> dianabogdanova.98@mail.ru</p>
                <p>
                    <i class="fa fa-phone mr-2"></i> + 375 29 838 71 78</p>
            </div>
        </div>
    </div>
    <div class="footer-copyright text-center py-3">© 2019 Copyright: Diana Bogdanova</div>
</footer>
