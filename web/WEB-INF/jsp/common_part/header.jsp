<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="ctg" uri="customtags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="language" value="${not empty language ? language : pageContext.request.locale}" scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources.locale" var="loc"/>

<fmt:message bundle="${loc}" key="myAccount" var="myAccount"/>
<fmt:message bundle="${loc}" key="logOut" var="logOut"/>
<fmt:message bundle="${loc}" key="lang" var="lang"/>
<fmt:message bundle="${loc}" key="registration" var="registration"/>
<fmt:message bundle="${loc}" key="signIn" var="signIn"/>
<fmt:message bundle="${loc}" key="ru" var="ru"/>
<fmt:message bundle="${loc}" key="en" var="en"/>
<fmt:message bundle="${loc}" key="be" var="be"/>
<fmt:message bundle="${loc}" key="signOut" var="signOut"/>
<!DOCTYPE html>
<html lang="${language}">
<head>
    <title>HospEx</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../css/account.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <style>
        h4 {
            font-family: "Monotype Corsiva", Cursive;
            font-weight: bold;
            font-stretch: expanded;
            font-size: 20pt;
            color: honeydew;
        }

    </style>
</head>
<nav class="navbar navbar-expand-sm navbar-dark" style="background-image: url(../../../img/header.jpg);
        background-size: cover">
  <div class="col-2">
                    <a class="navbar-brand">
                        <h4 style="font-family: 'Monotype Corsiva'; font-weight: bold; font-stretch: expanded; font-size: 20pt; color: honeydew;">
                            HospEx
                        </h4>
                    </a>
                    <div class="dropdown">
                        <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown">
                            ${lang}
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="/language.html?language=ru">${ru}</a>
                            <a class="dropdown-item" href="/language.html?language=be">${be}</a>
                            <a class="dropdown-item" href="/language.html?language=en">${en}</a>
                        </div>
                    </div>
                </div>
    <div class="col-8">
        <span class="navbar-text">
        <ctg:greeting userName="${currentUser.name} ${currentUser.surname}"/>
        </span>
    </div>

                <div class="col-2">
                    <div class="dropdown">
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            ${currentUser.email}
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="/account.html">${myAccount}</a>
                            <a class="dropdown-item" href="/logout.html">${logOut}</a>
                        </div>
                    </div>
                </div>
</nav>
</html>