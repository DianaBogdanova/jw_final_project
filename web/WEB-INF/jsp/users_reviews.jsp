<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

<div class="container">
    <div class="row">
    <div class="col-6">
        <c:choose>
            <c:when test="${not empty otherReviews}">
            </c:when>
            <c:otherwise>
                <h3>No reviews</h3>
            </c:otherwise>
        </c:choose>
    </div>
        <div class="col-4">
            <a class="float-right btn btn-info ml-2" href="/review/new.html?userId=${otherUser.id}">Add review</a>
        </div>
    </div>
    <c:forEach var="review" items="${otherReviews}">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-10">
                        <p>
                            <c:url value="/user/account.html?otherUser=${user.id}" var="otherUserURL"/>
                            <a class="float-left" href="${otherUserURL}"><strong>${review.authorName}</strong></a>
                            <c:if test="${review.rating > 1}">
                                <c:forEach begin="1" end="${review.rating}" varStatus="loop">
                                    <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                                </c:forEach>
                            </c:if>
                        </p>
                        <div class="clearfix"></div>
                        <p>${review.text}</p>
                    </div>
                </div>
            </div>
        </div>
    </c:forEach>
</div>
