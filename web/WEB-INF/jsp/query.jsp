<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="common_part/header.jsp"/>

<body>
<div class="container">
    <h2>Your request to ${addresseeName}</h2>
    <form action="/apply.html" class="needs-validation" novalidate method="post" id="form">
        <div class="form-group">
            <label for="arrival">Arrival date:</label>
            <input type="text" class="form-control" id="arrival" placeholder="YYYY-MM-DD" name="date_from" required pattern="([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))">
            <div class="valid-feedback">Valid.</div>
            <div class="invalid-feedback">
                The date format must match the pattern YYYY-MM-DD</div>
        </div>
        <div class="form-group">
            <label for="leaving">Departure date:</label>
            <input type="text" class="form-control" id="leaving" placeholder="YYYY-MM-DD" name="date_to" required pattern="([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))">
            <div class="valid-feedback">Valid.</div>
            <div class="invalid-feedback"> The date format must match the pattern YYYY-MM-DD</div>
        </div>
        <div class="form-group">
            <label for="message">Message:</label>
            <input type="text" class="form-control" id="message" placeholder="Enter text here" name="message">
            <div class="valid-feedback">Valid.</div>
        </div>
        <input type="hidden" id="addressee" name="addressee" value="${param["addressee"]}">
        <button type="submit" class="btn btn-primary">Send</button>
    </form>
    <br/>
    <br/>
    <br/>

    <c:if test="${message!=null}">
        <div class="alert alert-success">
            <strong>Success!</strong> ${message}
            <br/>
            <a class="profile-edit-btn" href="/search.html">Back</a>
        </div>
    </c:if>
    <c:if test="${error!=null}">
        <div class="alert alert-danger">
            <strong>Error!</strong> ${error}
            <br/>
        </div>
    </c:if>
</div>

<script>
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            var forms = document.getElementsByClassName('needs-validation');
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();

</script>

</body>
<%@ include file="common_part/footer.jsp" %>
