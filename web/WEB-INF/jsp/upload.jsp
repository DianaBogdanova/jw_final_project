<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources.locale" var="loc"/>
<jsp:include page="common_part/header.jsp"/>

<body>
<div class="container" align="center">
<form action="/avatar/upload.html" method="post" enctype="multipart/form-data">
    <input class="btn btn-secondary" name="photo"
           type="file" value="Choose file"
           accept=".jpg, .jpeg, .png"/>
    <input class="btn btn-primary" type="submit" name="edit" value="OK"/>
</form>
</div>

<c:if test="${message!=null}">
    <div class="alert alert-success">
        <strong>Success!</strong> ${message}
        <br/>
        <a class="profile-edit-btn" href="/account.html">My account</a>
    </div>
</c:if>
<c:if test="${error!=null}">
    <div class="alert alert-danger">
        <strong>Error!</strong> ${error}
        <br/>
    </div>
</c:if>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
</body>
<%@ include file="common_part/footer.jsp" %>
</html>
