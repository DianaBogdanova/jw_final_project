<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Title</title>
</head>
<body>

<form action="/place/change.html" method="post" enctype="multipart/form-data" multiple="true">

    <select name="country" title="Select country">
        <option selected="selected">${currentCountry}</option>
        <c:forEach var="country" items="${countries}">
            <option value="${country}">${country}</option>
        </c:forEach>
    </select>
    Select city:
    <select name="city">
        <option selected="selected">${currentCity}</option>
        <c:forEach var="city" items="${cities}">
            <option value="${city}">${city}</option>
        </c:forEach>
    </select>

    The maximum number of guests:
    <input type="text" name="beds_number" value="${bedsNumber}"/>
    <br/>
    Add photos
    <input type="file" name="photo" multiple accept="image/*,image/jpg"/>
    <input type="checkbox" name="is_available">
    <input type="submit" name="edit" value="OK"/>
</form>
<br/>

<h3>${message}</h3>
<h3>${error}</h3>
</body>
</html>
