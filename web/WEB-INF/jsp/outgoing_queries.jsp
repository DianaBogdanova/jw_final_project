<fmt:message bundle="${loc}" key="cancelBtn" var="cancelBtn"/>

<div class="container">
    <c:forEach var="outQuery" items="${outQueries}">
        <c:choose>
            <c:when test="${outQuery.condition==0}">
                <div class="card text-white bg-success mb-3 text-center">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2">
                                <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                                <br/>
                                <br/>
                                <p>${outQuery.message}</p>
                            </div>
                            <div class="col-md-10">
                                <p>
                                    <a class="float-left text-dark" href="https://maniruzzaman-akash.blogspot.com/p/contact.html"><strong>${outQuery.hostName}</strong></a>
                                </p>
                                <div class="clearfix"></div>
                                <strong>${arrivalDate}:</strong> ${outQuery.dateFrom}
                                <br/>
                                <strong>${leavingDate}:</strong> ${outQuery.dateTo}
                                <br/>
                                <br/>
                                <br/>
                                <p>
                                <p style="color:darkgreen; background-color:#dddddd">${requestAccepted}</p>
                                <a class="float-right btn btn-danger ml-2" href="/cancel.html?outQuery=${outQuery.id}">${cancelBtn}</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </c:when>
            <c:when test="${outQuery.condition==1}">
                <div class="card text-white bg-danger mb-3 text-center">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2">
                                <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                                <br/>
                                <br/>
                                <p>${outQuery.message}</p>
                            </div>
                            <div class="col-md-10">
                                <p>
                                    <a class="float-left text-dark" href="https://maniruzzaman-akash.blogspot.com/p/contact.html"><strong>${outQuery.hostName}</strong></a>
                                </p>
                                <div class="clearfix"></div>
                                <strong>${arrivalDate}:</strong> ${outQuery.dateFrom}
                                <br/>
                                <strong>${leavingDate}:</strong> ${outQuery.dateTo}
                                <br/>
                                <p>
                                <p style="color:red; background-color:#dddddd">${requestDenied}</p>
                                <a class="float-right btn btn-success ml-2" href="/cancel.html?outQuery=${outQuery.id}">${cancelBtn}</a>

                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </c:when>
            <c:when test="${outQuery.condition==2}">
                <div class="card text-white bg-info mb-3 text-center">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2">
                                <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                                <br/>
                                <br/>
                                <p>${outQuery.message}</p>
                            </div>
                            <div class="col-md-10">
                                <p>
                                    <a class="float-left text-dark" href="https://maniruzzaman-akash.blogspot.com/p/contact.html"><strong>${outQuery.hostName}</strong></a>
                                </p>
                                <div class="clearfix"></div>
                                <strong>${arrivalDate}:</strong> ${outQuery.dateFrom}
                                <br/>
                                <strong>${leavingDate}:</strong> ${outQuery.dateTo}
                                <br/>
                                <p>
                                <p style="color:#117a8b; background-color:#dddddd">${requestProcessed}</p>
                                <a class="float-right btn btn-success ml-2" href="/cancel.html?outQuery=${outQuery.id}">${cancelBtn}</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </c:when>
        </c:choose>
    </c:forEach>
</div>