<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="language" value="${not empty language ? language : pageContext.request.locale}" scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources.locale" var="loc"/>

<fmt:message bundle="${loc}" key="lang" var="lang"/>
<fmt:message bundle="${loc}" key="ru" var="ru"/>
<fmt:message bundle="${loc}" key="en" var="en"/>
<fmt:message bundle="${loc}" key="be" var="be"/>

<fmt:message bundle="${loc}" key="registrationLabel" var="registrationLabel"/>
<fmt:message bundle="${loc}" key="nameLabel" var="nameLabel"/>
<fmt:message bundle="${loc}" key="surnameLabel" var="surnameLabel"/>
<fmt:message bundle="${loc}" key="emailAddress" var="emailAddress"/>
<fmt:message bundle="${loc}" key="passwordField" var="passwordField"/>
<fmt:message bundle="${loc}" key="signUpBtn" var="signUpBtn"/>
<fmt:message bundle="${loc}" key="cancelBtn" var="cancelBtn"/>
<fmt:message bundle="${loc}" key="validMessage" var="validMessage"/>
<fmt:message bundle="${loc}" key="invalidName" var="invalidName"/>
<fmt:message bundle="${loc}" key="invalidSurname" var="invalidSurname"/>
<fmt:message bundle="${loc}" key="invalidEmail" var="invalidEmail"/>
<fmt:message bundle="${loc}" key="invalidPassword" var="invalidPassword"/>

<!DOCTYPE html>
<html lang="${language}">
<head>
    <title>HospEx</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/account.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <style>
        h4 {
            font-family: "Monotype Corsiva", Cursive;
            font-weight: bold;
            font-stretch: expanded;
            font-size: 20pt;
            color: honeydew;
        }
        body {
            background-image: url(../../img/header.jpg);
            background-size: cover
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-sm navbar-dark" style="background-image: url(../../img/header.jpg);
        background-size: cover">
    <div class="col-2">
        <a class="navbar-brand">
            <h4 style="font-family: 'Monotype Corsiva'; font-weight: bold; font-stretch: expanded; font-size: 20pt; color: honeydew;">
                HospEx
            </h4>
        </a>
        <div class="dropdown">
            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown">
                ${lang}
            </button>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="/language.html?language=ru">${ru}</a>
                <a class="dropdown-item" href="/language.html?language=be">${be}</a>
                <a class="dropdown-item" href="/language.html?language=en">${en}</a>
            </div>
        </div>
    </div>
</nav>
<div class="container">
    <h2>${registrationLabel}</h2>
    <form action="/signUp.html" class="needs-validation" novalidate method="post">
        <div class="form-group">
            <label for="name">${nameLabel}:</label>
            <input type="text" class="form-control" id="name" placeholder="${nameLabel}" name="name" required pattern="^[A-Za-z ,.'-]+$">
            <div class="valid-feedback">${validMessage}.</div>
            <div class="invalid-feedback">
                ${invalidName}</div>
        </div>
        <div class="form-group">
            <label for="surname">${surnameLabel}:</label>
            <input type="text" class="form-control" id="surname" placeholder="${surnameLabel}" name="surname" required pattern="^[A-Za-z ,.'-]+$">
            <div class="valid-feedback">${validMessage}.</div>
            <div class="invalid-feedback">${invalidSurname}</div>
        </div>

        <div class="form-group">
            <label for="email">${emailAddress}:</label>
            <input type="text" class="form-control" id="email" placeholder="${emailAddress}" name="email" required pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-z0-9.-]+\.[a-z]{2,}$">
            <div class="valid-feedback">${validMessage}.</div>
            <div class="invalid-feedback">${invalidEmail}.</div>
        </div>
        <div class="form-group">
            <label for="pwd">${passwordField}:</label>
            <input type="password" class="form-control" id="pwd" placeholder="${passwordField}" name="password" required pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d@$!%*#?&]{8,}$">
            <div class="valid-feedback">${validMessage}.</div>
            <div class="invalid-feedback">${invalidPassword}.</div>
        </div>

        <button type="submit" class="btn btn-primary">${signUpBtn}</button>
        <a class="btn btn-secondary" href="/main.html">${cancelBtn}</a>
    </form>

    <c:if test="${message!=null}">
        <div class="alert alert-success">
            <strong>Success!</strong> ${message}
            <br/>
            <a class="profile-edit-btn" href="/main.html">Main page</a>
        </div>
    </c:if>
    <c:if test="${error!=null}">
        <div class="alert alert-danger">
            <strong>Error!</strong> ${error}
            <br/>
        </div>
    </c:if>
</div>

<script>
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            var forms = document.getElementsByClassName('needs-validation');
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();

</script>

</body>
</html>