<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources.locale" var="loc"/>

<fmt:message bundle="${loc}" key="mainPage" var="mainPage"/>
<fmt:message bundle="${loc}" key="notFound" var="notFound"/>
<!DOCTYPE html>
<html>
<head>
    <title>HospEx</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/error.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
<section>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
        <div class="col-8">
        <a class="navbar-brand">
            <h4 style="font-family: 'Monotype Corsiva'; font-weight: bold; font-stretch: expanded; font-size: 20pt; color: honeydew;">
                HospEx
            </h4>
        </a>
        </div>
        <div class="col-4">
        <a class="float-right btn btn-primary ml-2" href="/main.html">${mainPage}</a>
        </div>
    </nav>
</section>
<section id="not-found">
    <div class="circles">
        <p>404<br>
            <small>${notFound}</small>
        </p>

        <span class="circle big"></span>
        <span class="circle med"></span>
        <span class="circle small"></span>
    </div>
</section>
</body>
</html>
