<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources.locale" var="loc"/>

<fmt:message bundle="${loc}" key="saveBtn" var="saveBtn"/>
<fmt:message bundle="${loc}" key="ratingLabel" var="ratingLabel"/>
<fmt:message bundle="${loc}" key="textLabel" var="textLabel"/>
<fmt:message bundle="${loc}" key="reviewPlaceholder" var="reviewPlaceholder"/>

<jsp:include page="common_part/header.jsp"/>

<body>
<div class="container">
    <c:url value="/review/save.html" var="reviewURL"/>
    <form action="${reviewURL}" method="post">
        <div class="form-group">
            <label for="text">${textLabel}:</label>
            <br/>
            <input type="text" id="text" name="textReview"
                   placeholder="${reviewPlaceholder}">
        </div>
        <div class="form-group">
            <label for="rating">${ratingLabel}:</label>
            <br/>
            <input type="number" step="1" min="-5" max="5" value="0" id="rating" name="rating"/>
        </div>
        <input type="hidden" name="id" id="id" value="${param["userId"]}">
        <br/>
        <button type="submit" class="btn btn-primary">${saveBtn}</button>
    </form>
</div>
<br/>
<br/>

</body>
<%@ include file="common_part/footer.jsp" %>
