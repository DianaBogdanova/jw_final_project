<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources.locale" var="loc"/>

<fmt:message bundle="${loc}" key="rejectBtn" var="rejectBtn"/>
<fmt:message bundle="${loc}" key="acceptBtn" var="acceptBtn"/>
<fmt:message bundle="${loc}" key="requestAccepted" var="requestAccepted"/>
<fmt:message bundle="${loc}" key="requestDenied" var="requestDenied"/>
<fmt:message bundle="${loc}" key="requestProcessed" var="requestProcessed"/>
<fmt:message bundle="${loc}" key="acceptBtn" var="acceptBtn"/>
<fmt:message bundle="${loc}" key="arrivalDate" var="arrivalDate"/>
<fmt:message bundle="${loc}" key="leavingDate" var="leavingDate"/>

<div class="container">
    <c:forEach var="query" items="${inQueries}">
        <c:choose>
            <c:when test="${query.condition==0}">
                <div class="card text-white bg-success mb-3 text-center">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2">
                                <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                                <br/>
                                <br/>
                                <p>${query.message}</p>
                            </div>
                            <div class="col-md-10">
                                <p>
                                    <a class="float-left text-dark" href="https://maniruzzaman-akash.blogspot.com/p/contact.html"><strong>${query.surferName}</strong></a>
                                </p>
                                <div class="clearfix"></div>
                                <strong>${arrivalDate}:</strong> ${query.dateFrom}
                                <br/>
                                <strong>${leavingDate}:</strong> ${query.dateTo}
                                <br/>
                                <br/>
                                <br/>
                                <p>
                                <p style="color:darkgreen; background-color:#dddddd">${requestAccepted}</p>
                                <form action="/reject.html">
                                    <input type="hidden" name="queryId" value="${query.id}">
                                    <input class="float-right btn btn-danger ml-2" type="submit" value="${rejectBtn}">
                                </form>
                                </p>
                            </div>

                        </div>
                    </div>
                </div>
            </c:when>
            <c:when test="${query.condition==1}">
                <div class="card text-white bg-danger mb-3 text-center">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2">
                                <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                                <br/>
                                <br/>
                                <p>${query.message}</p>
                            </div>
                            <div class="col-md-10">
                                <p>
                                    <a class="float-left text-dark" href="https://maniruzzaman-akash.blogspot.com/p/contact.html"><strong>${query.surferName}</strong></a>
                                </p>
                                <div class="clearfix"></div>
                                <strong>Arrival date:</strong> ${query.dateFrom}
                                <br/>
                                <strong>Leaving date:</strong> ${query.dateTo}

                                <br/>
                                <p>
                                <p style="color:red; background-color:#dddddd">${requestDenied}</p>
                                <form action="/accept.html">
                                    <input type="hidden" name="queryId" value="${query.id}">
                                    <input class="float-right btn btn-success ml-2" type="submit" value="${acceptBtn}">
                                </form>

                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </c:when>
            <c:when test="${query.condition==2}">
                <div class="card text-white bg-info mb-3 text-center">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2">
                                <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                                <br/>
                                <br/>
                                <p>${query.message}</p>
                            </div>
                            <div class="col-md-10">
                                <p>
                                    <a class="float-left text-dark" href="https://maniruzzaman-akash.blogspot.com/p/contact.html"><strong>${query.surferName}</strong></a>
                                </p>
                                <div class="clearfix"></div>
                                <strong>Arrival date:</strong> ${query.dateFrom}
                                <br/>
                                <strong>Leaving date:</strong> ${query.dateTo}

                                <br/>
                                <p>
                                <p style="color:#117a8b; background-color:#dddddd">${requestProcessed}</p>
                                <form action="/reject.html">
                                    <input type="hidden" name="query" value="${query.id}">
                                    <input class="float-right btn btn-danger ml-2" type="submit" value="${rejectBtn}">
                                </form>
                                <form action="/accept.html">
                                    <input type="hidden" name="queryId" value="${query.id}">
                                    <input class="float-right btn btn-success ml-2" type="submit" value="${acceptBtn}">
                                </form>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </c:when>
        </c:choose>
    </c:forEach>
</div>