<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="language" value="${not empty language ? language : pageContext.request.locale}" scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources.locale" var="loc"/>

<fmt:message bundle="${loc}" key="lang" var="lang"/>
<fmt:message bundle="${loc}" key="ru" var="ru"/>
<fmt:message bundle="${loc}" key="en" var="en"/>
<fmt:message bundle="${loc}" key="be" var="be"/>

<fmt:message bundle="${loc}" key="authorisationLabel" var="authorisationLabel"/>
<fmt:message bundle="${loc}" key="authorisationText" var="authorisationText"/>
<fmt:message bundle="${loc}" key="emailAddress" var="emailAddress"/>
<fmt:message bundle="${loc}" key="passwordField" var="passwordField"/>
<fmt:message bundle="${loc}" key="signInBtn" var="signInBtn"/>
<fmt:message bundle="${loc}" key="cancelBtn" var="cancelBtn"/>
<fmt:message bundle="${loc}" key="validMessage" var="validMessage"/>
<fmt:message bundle="${loc}" key="invalidFeedbackA" var="invalidFeedbackA"/>



<!DOCTYPE html>
<html lang="${language}">
<head>
    <title>HospEx</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/account.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <style>
        h4 {
            font-family: "Monotype Corsiva", Cursive;
            font-weight: bold;
            font-stretch: expanded;
            font-size: 20pt;
            color: honeydew;
        }
        body {
            background-image: url(../../img/header.jpg);
            background-size: cover
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-sm navbar-dark" style="background-image: url(../../img/header.jpg);
        background-size: cover">
    <div class="col-2">
        <a class="navbar-brand">
            <h4 style="font-family: 'Monotype Corsiva'; font-weight: bold; font-stretch: expanded; font-size: 20pt; color: honeydew;">
                HospEx
            </h4>
        </a>
        <div class="dropdown">
            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown">
                ${lang}
            </button>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="/language.html?language=ru">${ru}</a>
                <a class="dropdown-item" href="/language.html?language=be">${be}</a>
                <a class="dropdown-item" href="/language.html?language=en">${en}</a>
            </div>
        </div>
    </div>
</nav>
<div class="container">
    <h2>${authorisationLabel}</h2>
    <p>${authorisationText}</p>
    <form action="/authorisation.html" class="needs-validation" novalidate method="post">
        <div class="form-group">
            <label for="email">${emailAddress}:</label>
            <input type="text" class="form-control" id="email" placeholder="${emailAddress}" name="email" required>
            <div class="valid-feedback">${validMessage}.</div>
            <div class="invalid-feedback">${invalidFeedbackA}.</div>
        </div>
        <div class="form-group">
            <label for="pwd">${passwordField}:</label>
            <input type="password" class="form-control" id="pwd" placeholder="${passwordField}" name="password" required>
            <div class="valid-feedback">${validMessage}.</div>
            <div class="invalid-feedback">${invalidFeedbackA}.</div>
        </div>

        <button type="submit" class="btn btn-primary">${signInBtn}</button>
        <a class="btn btn-secondary" href="/main.html">${cancelBtn}</a>
    </form>

    <c:if test="${error!=null}">
        <div class="alert alert-danger">
            <strong>Error!</strong> ${error}
        </div>
    </c:if>
</div>

<script>
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            var forms = document.getElementsByClassName('needs-validation');
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();

</script>
</body>
</html>