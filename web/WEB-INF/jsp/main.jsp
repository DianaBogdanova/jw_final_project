<%@ page contentType="text/html;charset=UTF-8" language="java" import="by.training.hospitality_network.entity.User"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>hospex</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <style>
        h1 {
            font-family: "Monotype Corsiva", Cursive;
            font-weight: bold;
            font-stretch: expanded;
            background: rgba(0, 0, 0, 0.5);
            font-size: 40pt;
            color: honeydew;
        }
        h2 {
            font-family: "Monotype Corsiva", Cursive;
            font-weight: bold;
            font-stretch: expanded;
            background: rgba(255, 255, 255, 0.5);
            font-size: 30pt;
            color: black;
        }
        h4 {
            font-family: "Monotype Corsiva", Cursive;
            font-weight: bold;
            font-stretch: expanded;
            font-size: 20pt;
            color: honeydew;
        }
        .carousel-inner img {
            width: 100%;
            height: 100%;
        }
    </style>
</head>
<body>

<div class="jumbotron text-center"
     style="background-image: url(../../img/header.jpg); background-size: cover">
    <h1>Hospitality exchange</h1>
    <h2>Work, Travel, Save, Repeat</h2>
</div>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <a class="navbar-brand">
        <h4>
            HospEx
        </h4>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="/registration.html"> Sign up </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/authorisation.html"> Sign in </a>
            </li>
        </ul>
    </div>
</nav>

<div class="container" style="margin-top:30px">
    <div class="row">
        <div class="col-sm-4">
            <br/>
            <h2>Life is either a daring adventure or nothing at all.</h2>
            <br/>
            <br/>

            <h3>You have friends all over the world, you just haven't met them yet.</h3>
            <br/>
            <br/>
            <p>Hospitality Exchange is a service that connects members
                to a global community of travelers.
                Use HospEx to find a place to stay or share your home with travelers.</p>
            <hr class="d-sm-none">
        </div>
    <div class="col-8">
        <div id="demo" class="carousel slide" data-ride="carousel">

            <ul class="carousel-indicators">
                <li data-target="#demo" data-slide-to="0" class="active"></li>
                <li data-target="#demo" data-slide-to="1"></li>
                <li data-target="#demo" data-slide-to="2"></li>
                <li data-target="#demo" data-slide-to="3"></li>
                <li data-target="#demo" data-slide-to="4"></li>
                <li data-target="#demo" data-slide-to="5"></li>
            </ul>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="img/main_carousel_7.jpeg">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="img/main_carousel_3.jpeg" alt="SAVE">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="img/main_carousel_4.jpeg" alt="REPEAT">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="img/main_carousel_5.jpeg" alt="REPEAT">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="img/main_carousel_6.jpeg" alt="REPEAT">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="img/main_carousel_2.jpeg" alt="REPEAT">
                </div>
            </div>

            <a class="carousel-control-prev" href="#demo" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#demo" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a>
        </div>
        </div>
    </div>
    </div>

<footer class="page-footer font-small unique-color-dark">

    <div style="background-color: #23272b">
        <div class="container">
            <div class="row py-4 d-flex align-items-center">
                <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
                    <h6 class="mb-0" style="color: #dddddd">Get connected with us on social networks!</h6>
                </div>
                <div class="col-md-6 col-lg-7 text-center text-md-right">

                    <a href="https://www.facebook.com/profile.php?id=100004250680724&ref=bookmarks"><i class="fa fa-facebook mr-4"></i></a>
                    <a href="https://twitter.com/travel_with_me7"><i class="fa fa-twitter mr-4"></i></a>
                    <a href="https://vk.com/away.php?to=https%3A%2F%2Finstagram.com%2Fdi_laevskay_%3Figshid%3D9g7z6c2v2bnh&cc_key="><i class="fa fa-instagram mr-4"></i></a>
                    <a href="https://vk.com/id177344356"><i class="fa fa-vk mr-4"></i></a>

                </div>
            </div>
        </div>
    </div>

    <div class="container text-center text-md-left mt-5">
        <div class="row mt-3">
            <div class="col-6">
                <h4 class="text-uppercase font-weight-bold" style="color: black">HospEx</h4>
                <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p>
                    Stop worrying about the potholes in the road and enjoy the journey.
                </p>
            </div>

            <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">

                <h6 class="text-uppercase font-weight-bold">Contact</h6>
                <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p>
                    <i class="fa fa-home mr-1"></i> Minsk</p>
                <p>
                    <i class="fa fa-envelope-o mr-1"></i> dianabogdanova.98@mail.ru</p>
                <p>
                    <i class="fa fa-phone mr-1"></i> + 375 29 838 71 78</p>
            </div>
        </div>
    </div>
    <div class="footer-copyright text-center py-3">© 2019 Copyright: Diana Bogdanova</div>
</footer>
</body>

</html>
