<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources.locale" var="loc"/>

<fmt:message bundle="${loc}" key="myAccount" var="myAccount"/>
<fmt:message bundle="${loc}" key="myQueries" var="myQueries"/>
<fmt:message bundle="${loc}" key="usersList" var="usersList"/>

<br/>
<ul class="nav nav-pills flex-column">
    <li class="nav-item">
        <a class="nav-link active" href="#">${myAccount}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/queries.html">${myQueries}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/search.html">${usersList}</a>
    </li>
</ul>
