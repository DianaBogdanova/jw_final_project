
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources.locale" var="loc"/>

<fmt:message bundle="${loc}" key="noReviews" var="noReviews"/>
<div class="container">
    <c:choose>
        <c:when test="${empty reviews}">
            <h2 class="text-center">${noReviews}</h2>
        </c:when>
        <c:otherwise>
    <c:forEach var="review" items="${reviews}">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-2">
                    <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                </div>
                <div class="col-md-10">
                    <p>
                        <a class="float-left" href="/user/account.html?otherUser=${user.id}"><strong>${review.authorName}</strong></a>
                        <c:forEach begin="1" end="${review.rating}" varStatus="loop">
                            <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                        </c:forEach>
                    </p>
                    <div class="clearfix"></div>
                    <p>${review.text}</p>
                </div>
            </div>
        </div>
    </div>
    </c:forEach>
        </c:otherwise>
    </c:choose>

    <c:if test="${pagesNumber>1}">
        <ul class="pagination justify-content-center">
            <c:if test="${currentPage>1}">
                <c:url value="/search.html?currentPage=${currentPage-1}&pagesNumber=${pagesNumber}"
                       var="previousLinkURL"/>
                <li class="page-item"><a class="page-link"
                                         href="${previousLinkURL}"><c:out
                        value="${previous}"/></a>
                </li>
            </c:if>

            <c:if test="${currentPage==1}">
                <li class="page-item disabled"><a class="page-link " href="#"><c:out value="${previous}"/></a></li>
            </c:if>

            <c:forEach begin="${1}" end="${pagesNumber}" var="i">
                <c:choose>
                    <c:when test="${currentPage==i}">
                        <c:url value="/account.html?currentPage=${i}&pagesNumber=${pagesNumber}"
                               var="loadPageURL"/>
                        <li class="page-item active"><a class="page-link"
                                                        href="${loadPageURL}">${i}</a>
                        </li>
                    </c:when>
                    <c:when test="${currentPage!=i}">
                        <c:url value="/account.html?currentPage=${i}&pagesNumber=${pagesNumber}"
                               var="loadPageURL"/>
                        <li class="page-item"><a class="page-link"
                                                 href="${loadPageURL}">${i}</a>
                        </li>
                    </c:when>
                </c:choose>
            </c:forEach>

            <c:if test="${currentPage+1<=pagesNumber}">
                <c:url value="/account.html?currentPage=${currentPage+1}&pagesNumber=${pagesNumber}"
                       var="nextLinkURL"/>
                <li class="page-item"><a class="page-link"
                                         href="${nextLinkURL}"><c:out
                        value="${next}"/></a>
                </li>
            </c:if>

            <c:if test="${currentPage==pagesNumber}">
                <li class="page-item disabled"><a class="page-link " href="#"><c:out value="${next}"/></a></li>
            </c:if>
        </ul>
    </c:if>
</div>