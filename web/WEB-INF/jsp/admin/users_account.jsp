<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources.locale" var="loc"/>

<fmt:message bundle="${loc}" key="userIsHost" var="userIsHost"/>
<fmt:message bundle="${loc}" key="userIsNotHost" var="userIsNotHost"/>
<fmt:message bundle="${loc}" key="userRating" var="userRating"/>


<jsp:include page="../common_part/header.jsp"/>

<body>

<div class="container emp-profile">

    <c:if test="${not empty message or not empty error}">
        <div class="toast" data-autohide="false">
            <div class="toast-header">
                <strong class="mr-auto text-primary">HospEx</strong>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast">&times;</button>
            </div>
            <div class="toast-body">
                    ${message}
                    ${error}
            </div>
        </div>
    </c:if>
    <form method="post">
        <div class="row">
            <div class="col-md-3">
                <c:choose>
                    <c:when test="${not empty otherUser.avatarPath}">
                        <img src="${otherUser.avatarPath}" class="img" width="250" height="200"/>
                    </c:when>
                    <c:otherwise>
                        <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="col-md-6">
                <div class="profile-head">
                    <h5>
                        ${otherUser.name} ${otherUser.surname}
                    </h5>
                    <c:choose>
                        <c:when test="${otherUser.isHost==true}">
                            <h6>
                                ${userIsHost}
                            </h6>
                        </c:when>
                        <c:otherwise>
                            <h6>
                                ${userIsNotHost}
                            </h6>
                        </c:otherwise>
                    </c:choose>
                    <p class="proile-rating">${userRating}: <span>${otherUser.rating}</span></p>
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Reviews</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2">
                <c:choose>
                    <c:when test="${otherUser.isActive}">
                        <a href="/user/lock.html?userId=${otherUser.id}&currentPage=account">lock</a>
                    </c:when>
                    <c:otherwise>
                        <a href="/user/unlock.html?userId=${otherUser.id}&currentPage=account">unlock</a>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">

            </div>
            <div class="col-md-8">
                <div class="tab-content profile-tab" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Email</label>
                            </div>
                            <div class="col-md-6">
                                <p>${otherUser.email}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Phone</label>
                            </div>
                            <div class="col-md-6">
                                <p>${otherUser.phone}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>About me:</label>
                            </div>
                            <div class="col-md-6">
                                <p>${otherUser.profile}</p>
                            </div>
                        </div>
                    </div>
                    <form action="/review/new.html" method="post" id="newReview">
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <%@ include file="users_reviews.jsp" %>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    $(document).ready(function(){
        $('.toast').toast('show');
    });

</script>
</body>
<%@ include file="../common_part/footer.jsp" %>
