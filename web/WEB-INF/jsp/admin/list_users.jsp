<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources.locale" var="loc"/>

<fmt:message bundle="${loc}" key="userIsHost" var="userIsHost"/>
<fmt:message bundle="${loc}" key="userIsNotHost" var="userIsNotHost"/>
<fmt:message bundle="${loc}" key="userRating" var="userRating"/>
<fmt:message bundle="${loc}" key="next" var="next"/>
<fmt:message bundle="${loc}" key="previous" var="previous"/>
<fmt:message bundle="${loc}" key="usersListAdmTitle" var="usersListAdmTitle"/>
<fmt:message bundle="${loc}" key="lockBtn" var="lockBtn"/>
<fmt:message bundle="${loc}" key="unlockBtn" var="unlockBtn"/>


<jsp:include page="../common_part/header.jsp"/>

<body>
<div class="container mt-3">
    <form action="/users/filter.html" method="post">
        <div class="form-group col-md-4">
            <select name="country" title="Select country" class="form-control">
                <c:forEach var="country" items="${countries}">
                    <option value="${country}">${country}</option>
                </c:forEach>
            </select>
        </div>
        <div class="form-group col-md-3">
            <label for="is_host">Only hosts </label>
            <input type="checkbox" id="is_host" name="is_host">
        </div>
        <button type="submit" class="btn btn-primary">Search</button>
    </form>
    <c:if test="${not empty message or not empty error}">
        <div class="toast" data-autohide="false">
            <div class="toast-header">
                <strong class="mr-auto text-primary">HospEx</strong>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast">&times;</button>
            </div>
            <div class="toast-body">
                    ${message}
                    ${error}
            </div>
        </div>
    </c:if>
    <div class="row">
        <div class="col-md-9">
            <h2>${usersListAdmTitle}</h2>
        </div>
    </div>
    <input class="form-control" id="myInput" type="text" placeholder="Search..">
    <br>
    <span class="pull-right"></span>
    <ul class="list-group" id="myList">
        <c:forEach var="user" items="${users}">
            <c:if test="${user.id != currentUser.id}">
                <c:choose>
                    <c:when test="${user.isActive}">
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-4">
                                    <c:choose>
                                        <c:when test="${not empty user.avatarPath}">
                                            <img src="${user.avatarPath}" class="rounded-circle" width="200" height="150"/>
                                        </c:when>
                                        <c:otherwise>
                                            <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                                <div class="col-6">
                                    <c:url value="/user/account.html?otherUser=${user.id}"
                                           var="otherAccountURL"/>
                                    <a href="${otherAccountURL}">${user.name} ${user.surname}</a>
                                    <br/>
                                    <c:choose>
                                        <c:when test="${user.isHost==true}">
                                            <h6 style="color: #10d510">
                                                    ${userIsHost}
                                            </h6>
                                        </c:when>
                                        <c:otherwise>
                                            <h6 style="color: red">
                                                    ${userIsNotHost}
                                            </h6>
                                        </c:otherwise>
                                    </c:choose>
                                    <br/>
                                        ${userRating} ${user.rating}
                                </div>
                            <div class="col-2">
                                <c:url value="/user/lock.html?userId=${user.id}&currentPage=list" var="lockURL"/>
                                <a class="float-right btn btn-info ml-2" href="${lockURL}">${lockBtn}</a>
                            </div>
                                <span class="pull-right">
                                </span>
                            </div>
                        </li>
                    </c:when>
                    <c:when test="${not user.isActive}">
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-4">
                                    <c:choose>
                                        <c:when test="${not empty user.avatarPath}">
                                            <img src="${user.avatarPath}" class="rounded-circle" width="200" height="150"/>
                                        </c:when>
                                        <c:otherwise>
                                            <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                                <div class="col-6">
                                    <c:url value="/user/account.html?otherUser=${user.id}"
                                           var="otherAccountURL"/>
                                    <a href="${otherAccountURL}">${user.name} ${user.surname}</a>
                                    <br/>
                                    <c:choose>
                                        <c:when test="${user.isHost==true}">
                                            <h6 style="color: #10d510">
                                                    ${userIsHost}
                                            </h6>
                                        </c:when>
                                        <c:otherwise>
                                            <h6 style="color: red">
                                                    ${userIsNotHost}
                                            </h6>
                                        </c:otherwise>
                                    </c:choose>
                                    <br/>
                                        ${userRating} ${user.rating}
                                </div>
                                <div class="col-2">
                                    <c:url value="/user/unlock.html?userId=${user.id}&currentPage=list" var="unlockURL"/>
                                    <a class="float-right btn btn-info ml-2" href="${unlockURL}">${unlockBtn}</a>
                                </div>
                                <span class="pull-right">
                                </span>
                            </div>
                        </li>
                    </c:when>
                </c:choose>
            </c:if>
        </c:forEach>
    </ul>
    <c:if test="${pagesNumber>1}">
        <ul class="pagination justify-content-center">
            <c:if test="${currentPage>1}">
                <c:url value="/search.html?currentPage=${currentPage-1}&pagesNumber=${pagesNumber}"
                       var="previousLinkURL"/>
                <li class="page-item"><a class="page-link"
                                         href="${previousLinkURL}"><c:out
                        value="${previous}"/></a>
                </li>
            </c:if>

            <c:if test="${currentPage==1}">
                <li class="page-item disabled"><a class="page-link " href="#"><c:out value="${previous}"/></a></li>
            </c:if>

            <c:forEach begin="${1}" end="${pagesNumber}" var="i">
                <c:choose>
                    <c:when test="${currentPage==i}">
                        <c:url value="/search.html?currentPage=${i}&pagesNumber=${pagesNumber}"
                               var="loadPageURL"/>
                        <li class="page-item active"><a class="page-link"
                                                        href="${loadPageURL}">${i}</a>
                        </li>
                    </c:when>
                    <c:when test="${currentPage!=i}">
                        <c:url value="/search.html?currentPage=${i}&pagesNumber=${pagesNumber}"
                               var="loadPageURL"/>
                        <li class="page-item"><a class="page-link"
                                                 href="${loadPageURL}">${i}</a>
                        </li>
                    </c:when>
                </c:choose>
            </c:forEach>

            <c:if test="${currentPage+1<=pagesNumber}">
                <c:url value="/search.html?currentPage=${currentPage+1}&pagesNumber=${pagesNumber}"
                       var="nextLinkURL"/>
                <li class="page-item"><a class="page-link"
                                         href="${nextLinkURL}"><c:out
                        value="${next}"/></a>
                </li>
            </c:if>

            <c:if test="${currentPage==pagesNumber}">
                <li class="page-item disabled"><a class="page-link " href="#"><c:out value="${next}"/></a></li>
            </c:if>
        </ul>
    </c:if>
</div>

<script>
    $(document).ready(function(){
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myList li").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
        $('.toast').toast('show');
    });
</script>
</body>
<%@ include file="../common_part/footer.jsp" %>
