<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources.locale" var="loc"/>

<fmt:message bundle="${loc}" key="saveBtn" var="saveBtn"/>
<fmt:message bundle="${loc}" key="resetBtn" var="resetBtn"/>

<jsp:include page="common_part/header.jsp"/>
<body>
    <div class="container">
        <h2>Edit account</h2>
        <div class="row">
            <c:url value="/account/save.html" var="saveURL"/>
            <form action="${saveURL}" method="post">
                <div class="form-group">
                    <label for="phone">Phone:</label>
                    <br/>
                    <input type="text" id="phone" name="phone"
                           value="${!empty currentUser.phone?currentUser.phone:""}"
                           placeholder="Enter your phone number"
                           pattern="[0-9]{1,15}">
                    <div class="invalid-feedback">Please check this field.</div>
                </div>
                <div class="form-group">
                    <label for="profile">Profile:</label>
                    <input type="text" class="form-control" id="profile" placeholder="Enter name" name="profile" value="${currentUser.profile}">
                </div>

                <select name="country" title="Select country">
                    <option selected="selected">${currentUser.country}</option>
                    <c:forEach var="country" items="${countries}">
                        <option value="${country}">${country}</option>
                    </c:forEach>
                </select>
                Select city:
                <select name="city">
                    <option selected="selected">${currentUser.city}</option>
                    <c:forEach var="city" items="${cities}">
                        <option value="${city}">${city}</option>
                    </c:forEach>
                </select>
                <div class="form-group">
                    <label for="is_host">I receive guests </label>
                    <input type="checkbox" id="is_host" name="is_host">
                </div>
                <button type="submit" class="btn btn-primary">${saveBtn}</button>
                <c:url value="/account/edit.html" var="editURL"/>
                <a class="btn btn-secondary" href="${editURL}">${resetBtn}</a>
            </form>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
        </div>
        <br/>

        <c:if test="${message!=null}">
        <div class="alert alert-success">
            <strong>Success!</strong> ${message}
        </div>
        </c:if>
        <c:if test="${error!=null}">
        <div class="alert alert-danger">
            <strong>Error!</strong> ${error}
            <br/>
        </div>
        </c:if>
    </div>
</body>
<%@ include file="common_part/footer.jsp" %>
