<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <link rel="stylesheet" href="../../css/account.css">

    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
    <fmt:setLocale value="${language}"/>
    <fmt:setBundle basename="resources.locale" var="loc"/>

    <fmt:message bundle="${loc}" key="userIsNotHost" var="userIsNotHost"/>
    <fmt:message bundle="${loc}" key="userIsHost" var="userIsHost"/>
    <fmt:message bundle="${loc}" key="editProfile" var="editProfile"/>
    <fmt:message bundle="${loc}" key="userRating" var="userRating"/>
    <fmt:message bundle="${loc}" key="about" var="about"/>
    <fmt:message bundle="${loc}" key="emailAddress" var="emailAddress"/>
    <fmt:message bundle="${loc}" key="phoneNumber" var="phoneNumber"/>
    <fmt:message bundle="${loc}" key="aboutMe" var="aboutMe"/>
    <fmt:message bundle="${loc}" key="myReviews" var="myReviews"/>
    <fmt:message bundle="${loc}" key="changePhoto" var="changePhoto"/>
    <jsp:include page="common_part/header.jsp"/>

<body>
<div class="container emp-profile" style="margin-top: 10px">
    <form method="post">
        <div class="row" >
            <div class="col-md-3">
                <c:choose>
                    <c:when test="${not empty currentUser.avatarPath}">
                        <img src="${currentUser.avatarPath}" class="img" width="230" height="200"/>
                    </c:when>
                    <c:otherwise>
                        <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                    </c:otherwise>
                </c:choose>
                <a class="btn btn-lg btn-secondary btn-block" href="/upload.html">${changePhoto}</a>
            </div>
            <div class="col-md-6">
                <div class="profile-head">
                    <h5>
                        ${currentUser.name} ${currentUser.surname}
                    </h5>
                    <c:choose>
                        <c:when test="${currentUser.isHost==true}">
                            <h6>
                                    ${userIsHost}
                            </h6>
                        </c:when>
                        <c:otherwise>
                            <h6>
                                    ${userIsNotHost}
                            </h6>
                        </c:otherwise>
                    </c:choose>
                    <p class="proile-rating">${userRating} <span>${currentUser.rating}</span></p>
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">${about}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">${myReviews}</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2">
                <a class="profile-edit-btn" href="/account/edit.html">${editProfile}</a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <%@ include file="sidebar.jsp" %>
            </div>
            <div class="col-md-8">
                <div class="tab-content profile-tab" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row">
                            <div class="col-md-6">
                                <label>${emailAddress}</label>
                            </div>
                            <div class="col-md-6">
                                <p>${currentUser.email}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>${phoneNumber}</label>
                            </div>
                            <div class="col-md-6">
                                <p>${currentUser.phone}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>${aboutMe}:</label>
                            </div>
                            <div class="col-md-6">
                                <p>${currentUser.profile}</p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <%@ include file="my_reviews.jsp" %>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
</body>
<%@ include file="common_part/footer.jsp" %>
</html>
