<link rel="stylesheet" href="../../css/queries.css">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="common_part/header.jsp"/>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources.locale" var="loc"/>

<fmt:message bundle="${loc}" key="outgoingQueries" var="outgoingQueries"/>
<fmt:message bundle="${loc}" key="incomingQueries" var="incomingQueries"/>

<!DOCTYPE html>
<html>
<body>
<div class="row">
    <div class="col-md-9">

    <ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">${incomingQueries}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">${outgoingQueries}</a>
    </li>
</ul>
    </div>
</div>
<div class="tab-content profile-tab" id="myTabContent">
    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
        <%@ include file="incoming_queries.jsp" %>
    </div>
    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
        <%@ include file="outgoing_queries.jsp" %>
    </div>
</div>
</body>
</html>
